import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider_architecture/provider_architecture.dart';
import 'package:rwandashop/core/model/authentication/reset_password_update.dart';
import 'package:rwandashop/core/services/auth_service.dart';
import 'package:rwandashop/ui/screens/register/registerViewModel.dart';
import 'package:rwandashop/ui/widget/generalButton.dart';
import 'package:rwandashop/utils/app_localizations.dart';
import 'package:rwandashop/utils/locator.dart';
import 'package:rwandashop/utils/router/navigationService.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class EnterPasswordFG extends StatefulWidget {
  @override
  EnterPasswordState createState() => new EnterPasswordState();
}

class EnterPasswordState extends State<EnterPasswordFG> {
  TextEditingController passwordController = TextEditingController();
  final Authentication _authentication = locator<Authentication>();

  final NavigationService _navigationService = locator<NavigationService>();
  bool passwordVisible;

  @override
  void initState() {
    super.initState();
    passwordVisible = true;
  }

  @override
  Widget build(BuildContext context) {
    AppLocalizations tr = AppLocalizations.of(context);
    return ViewModelProvider<RegisterViewModel>.withConsumer(
        viewModelBuilder: () => RegisterViewModel(),
        builder: (context, model, child) {
          return Scaffold(
            body: SingleChildScrollView(
              child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    SizedBox(
                      height: 30.h,
                    ),
                    Container(
                      padding: EdgeInsets.all(20),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          InkWell(
                            onTap: () {
                              _navigationService.pop();
                            },
                            child: Icon(Icons.arrow_back_ios),
                          ),
                          Text(''),
                        ],
                      ),
                    ),
                    SizedBox(
                      height: 40.h,
                    ),
                    Container(
                      child: Text(
                        tr.text('changePasswordFP.enterNewPassword'),
                        style: TextStyle(
                            fontSize: 26.ssp, fontWeight: FontWeight.bold),
                      ),
                    ),
                    SizedBox(
                      height: 10.h,
                    ),
                    Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Container(
                            padding: EdgeInsets.only(left: 20, right: 20),
                            child: Text(
                              tr.text('changePasswordFP.newPassword'),
                              style: TextStyle(
                                  fontSize: 14.ssp,
                                  color: Color(0xff7C7C7C),
                                  fontWeight: FontWeight.w400),
                            ),
                          ),
                          Text('')
                        ]),
                    Container(
                      padding: EdgeInsets.only(left: 20, right: 20),
                      child: TextFormField(
                        controller: passwordController,
                        // maxLength: 11,
                        decoration: InputDecoration(
                          suffixIcon: IconButton(
                            icon: Icon(
                              // Based on passwordVisible state choose the icon
                              passwordVisible
                                  ? Icons.visibility
                                  : Icons.visibility_off,
                              color: Colors.black,
                            ),
                            onPressed: () {
                              // Update the state i.e. toogle the state of passwordVisible variable
                              setState(() {
                                passwordVisible = !passwordVisible;
                              });
                            },
                          ),
//                    errorText: residentialValid
//                        ? null
//                        : 'Kindly update this field',
                          hintText: '******',
                          hintStyle: TextStyle(
                            color: Colors.black45,
                          ),
                          labelStyle: TextStyle(color: Colors.blue),
                          // border: new OutlineInputBorder(
                          //   borderRadius: new BorderRadius.circular(5.0),
                          //   borderSide: new BorderSide(),
                          // ),
                        ),
                        obscureText: passwordVisible,
                        keyboardType: TextInputType.visiblePassword,
                        style: TextStyle(color: Colors.black),
                        cursorColor: Colors.black,
                      ),
                    ),
                    SizedBox(
                      height: 20.h,
                    ),
                    Container(
                        padding: EdgeInsets.all(20.w),
                        child: GeneralButton(
                          buttonText: tr.text('changePasswordFP.update'),
                          onPressed: () {
                            model.changePassword(ResetPasswordUpdate(
                              phoneNumber: _authentication.phoneNumber,
                              password: passwordController.text,
                            ));
                          },
                        )),
                  ]),
            ),
          );
        });
  }
}
