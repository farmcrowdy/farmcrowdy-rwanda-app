import 'package:flutter/cupertino.dart';
import 'package:flutterwave/flutterwave.dart';
import 'package:get/get.dart';
import 'package:rwandashop/core/model/error_model.dart';
import 'package:rwandashop/core/model/product/cart.dart';
import 'package:rwandashop/core/model/product/edit_cart.dart';
import 'package:rwandashop/core/model/product/pay.dart';
import 'package:rwandashop/core/model/product/payment_approval.dart';
import 'package:rwandashop/core/model/product/single_package.dart';
import 'package:rwandashop/core/model/success_model.dart';
import 'package:rwandashop/core/services/auth_service.dart';
import 'package:rwandashop/core/services/products_service.dart';
import 'package:rwandashop/utils/app_localizations.dart';
import 'package:rwandashop/utils/baseModel.dart';
import 'package:rwandashop/utils/colors.dart';
import 'package:rwandashop/utils/helpers.dart';
import 'package:rwandashop/utils/locator.dart';
import 'package:rwandashop/utils/router/navigationService.dart';
import 'package:rwandashop/utils/router/routeNames.dart';
import 'package:url_launcher/url_launcher.dart';

import 'package:rwandashop/core/model/product/products.dart' as product;

class CartViewModel extends BaseModel {
  final Product _product = locator<Product>();
  final NavigationService _navigationService = locator<NavigationService>();
  final Authentication _authentication = locator<Authentication>();
  Cart cart;
  bool loading;
  String _contact= '080000000000';
  String get contact => _contact;

  Future<List<Orders>> getCart() async {
    //setBusy(true);
    var result = await _product.getCart();
    if (result is ErrorModel) {
      // showToast('Login failed');
      print(result.error);
      notifyListeners();
      throw Exception('Failed to load internet');
      //return ErrorModel(result.error);
    }
    print('HDHDHL:::::::::::::::::::::::');
    print(result);
    return result;
  }


  addAndRemoveFromSavedPackages(String userId,String packageId, BuildContext context) async {
    AppLocalizations tr = AppLocalizations.of(context);
    setBusy(true);
    var result = await _product.addAndRemoveFromSavedPackage(userId,packageId);

    if (result is ErrorModel) {
      setBusy(false);
      showErrorToast(result.error);
      print(result.error);
      notifyListeners();
      return ErrorModel(result.error);
    }
    if (result is SuccessModel) {
      setBusy(false);
      Get.snackbar(
        tr.text('shop.productSaved'),
        result.data['message'],
        backgroundColor: AppColors.green,
      );
      // getCartSummary();

      //_product.getCartSummary();
      // Get.back();
      //setBusy(false);
      // _navigationService.navigateTo(BottomNavigationRoute);

      notifyListeners();
      //_navigationService.pop();
      // return SuccessModel(result.data);
    }
  }

  Future<List<product.Data>> getSavedPackages() async {
    //setBusy(true);
    var result = await _product.getSavedPackagess();
    if (result is ErrorModel) {
      // showToast('Login failed');
      print(result.error);
      notifyListeners();
      throw Exception('Failed to load internet');
      //return ErrorModel(result.error);
    }
    print('HDHDHL:::::::::::::::::::::::');
    print(result);
    return result;
  }

  callStore() async {
    var  url = "tel:$contact";
    if (await canLaunch(url)) {
      print(url);
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }

  getSinglePackage(String id) async {
    //  loading = true;
    var result = await _product.getSinglePackages(id);
    if (result is ErrorModel) {
      //  loading = false;
      showErrorToast(result.error);
      print(result.error);
      notifyListeners();
      return ErrorModel(result.error);
    }
    if (result is SuccessModel) {
      print('SUMMARY:::::::::::');
      print(result.data);
      SinglePackage packageData = SinglePackage.fromJson(result.data);
      //loading = false;
      //cart = result.data;
      _contact = packageData.data.contact;
      print(_contact);
      notifyListeners();
      return packageData.data.contact;
    }
  }

  getCartSummary() async {
    //  loading = true;
    var result = await _product.getCartSummary();
    if (result is ErrorModel) {
      //  loading = false;
      showErrorToast(result.error);
      print(result.error);
      notifyListeners();
      return ErrorModel(result.error);
    }
    if (result is SuccessModel) {
      print('SUMMARY:::::::::::');
      print(result.data);
      //loading = false;
      //cart = result.data;
      notifyListeners();
      // return categories;
    }
  }

  approvePay(PaymentApproval paymentApproval) async {
    setBusy(true);
    var result = await _product.approvePayment(paymentApproval.toJson());
    if (result is ErrorModel) {
      setBusy(false);
      showErrorToast(result.error);
      print(result.error);
      notifyListeners();
      return ErrorModel(result.error);
    }
    if (result is SuccessModel) {
      setBusy(false);
      print(result.data);
      return SuccessModel(result.data);
    }
  }

  deleteCart(String id, BuildContext context) async {
    AppLocalizations tr = AppLocalizations.of(context);
    setBusy(true);
    var result = await _product.deleteCart(id);
    if (result is ErrorModel) {
      setBusy(false);
      showErrorToast(result.error);
      print(result.error);
      notifyListeners();
      return ErrorModel(result.error);
    }
    if (result is SuccessModel) {
      setBusy(false);
      Get.snackbar(tr.text('cart.deleteRequest'), result.data,
          backgroundColor: AppColors.green, colorText: AppColors.white);
      getCartSummary();
      //_product.getCartSummary();

      //cart = result.data;
      notifyListeners();
      // return categories;
    }
  }

  editCart(String id, BuildContext context, EditCart editCart) async {
    AppLocalizations tr = AppLocalizations.of(context);
    setBusy(true);
    var result = await _product.editCart(id, editCart.toJson());
    if (result is ErrorModel) {
      setBusy(false);
      showErrorToast(result.error);
      print(result.error);
      notifyListeners();
      return ErrorModel(result.error);
    }
    if (result is SuccessModel) {
      setBusy(false);
      Get.snackbar(tr.text(result.data), result.data,
          backgroundColor: AppColors.green, colorText: AppColors.white);
      getCartSummary();
      //_product.getCartSummary();

      //cart = result.data;
      notifyListeners();
      // return categories;
    }
  }

  handlePayment(BuildContext context, String amount) async {
    final flutterwave = Flutterwave.forUIPayment(
        amount: amount,
        // currency:  FlutterwaveCurrency.NGN,
        currency: FlutterwaveCurrency.RWF,
        context: context,
        //publicKey: 'FLWPUBK_TEST-2e678466fc7c5580b27da61e7efdd4f2-X',
        publicKey: 'FLWPUBK-c18b6e711faa6d3a1fa20da642f08388-X',
        //encryptionKey: 'FLWSECK_TEST883478744ef0',
        encryptionKey: '466027c1a8e40dde2565f6ae',
        email: 'rwandamarket@gmail.com',
        fullName: _authentication.currentUser.phoneNumber,
        txRef: DateTime.now().toIso8601String(),
        narration: "Rwanda Marketplace",
        isDebugMode: false,
        phoneNumber: _authentication.currentUser.phoneNumber,
        //acceptAccountPayment: true,
        acceptCardPayment: true,
        //acceptUSSDPayment: true,
        acceptRwandaMoneyPayment: true);
    final response = await flutterwave.initializeForUiPayments();
    if (response != null) {
      print(response.message);
      print(response.data.id);
      print(response.data.txRef);
      print(response.data.amount);
      print(response.data.status);
      print(response.data.accountId);
      print(response.data.appFee);
      print(response.data.authModel);
      print(response.data.authUrl);
      print(response.data.card);
      print(response.data.chargedAmount);
      print(response.data.chargeType);
      print(response.data.createdAt);
      print(response.data.customer.name);
      approvePay(PaymentApproval(
        phonenumber: response.data.customer.phoneNumber,
        currency: response.data.currency,
        amount: response.data.amount,
        name: _authentication.currentUser.phoneNumber,
        status: response.data.status,
        reference: response.data.txRef,
      ));
      getCartSummary();
      showToast(response.data.status);
      // _navigationService.navigateReplacementTo(BottomNavigationRoute);
      notifyListeners();
    } else {
      print(response.data);
      showToast('Not successful');
      getCartSummary();
      notifyListeners();
    }
  }
}
