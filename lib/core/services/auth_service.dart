import 'dart:convert';
import 'dart:io';
import 'package:rwandashop/core/model/authentication/auth_model.dart';
import 'package:rwandashop/core/model/authentication/final_reg_response.dart';
import 'package:rwandashop/core/model/authentication/login_response.dart'
    as Login;
import 'package:rwandashop/core/model/authentication/register_phone_no_response.dart';
import 'package:rwandashop/core/model/authentication/resend_otp_response.dart';
import 'package:rwandashop/core/model/authentication/reset_password_request_response.dart';
import 'package:rwandashop/core/model/authentication/reset_password_update_response.dart';
import 'package:rwandashop/core/model/error_model.dart';
import 'package:rwandashop/core/model/success_model.dart';
import 'package:rwandashop/core/services/index.dart';
import 'package:rwandashop/core/services/products_service.dart';
import 'package:rwandashop/utils/helpers.dart';
import 'package:rwandashop/utils/http/paths.dart';
import 'package:rwandashop/utils/locator.dart';
import 'package:rwandashop/utils/router/navigationService.dart';
import 'package:rwandashop/utils/router/routeNames.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http_parser/http_parser.dart';
import 'package:mime/mime.dart';
import 'package:http/http.dart' as htp;

class Authentication {
  final Product _product = locator<Product>();
  final NavigationService _navigationService = locator<NavigationService>();
  Login.User _currentUser;
  Login.User get currentUser => _currentUser;
  bool _loggedIn;
  bool get loggedIn => _loggedIn;

  String _phoneNumber;
  String get phoneNumber => _phoneNumber;

  AuthModel _token;
  AuthModel get token => _token;

  saveRegPhone(String phone) async {
    _phoneNumber = phone;
  }

  login(Map<dynamic, dynamic> payload) async {
    try {
      final result = await http.post(Paths.LOGIN, payload);
      if (result is ErrorModel) {
        print("ERROR");
        print(result.error);
        return ErrorModel(result.error);
      }
      _product.getCartSummary();
      print("RESULT");
      print(result.data);

      final AuthModel auth =
          AuthModel.fromJson(result.data['data']['access_token']);
      print("TOKEN AGBA::::::::: ${auth.token}");
      _token = auth;
      Login.User user = Login.User.fromJson(result.data['data']['user']);
      showToast(result.data['message']);
      _currentUser = user;
      SharedPreferences prefs;
      prefs = await SharedPreferences.getInstance();
      await prefs.setString('token', auth.token);
      await prefs.setString('userId', user.phoneNumber);
      var s = json.encode(_currentUser);
      await prefs.setString('profile', s);
      print('Sector' + user.sectorName);
      // return SuccessModel({'auth': auth, 'user': user});
      return SuccessModel(user);
    } catch (e) {
      print(e.toString());
      return ErrorModel('Login failed, try again.');
    }
  }

  alreadyLoggedIn() async {
    _product.alreadyLoggedInCart();
    SharedPreferences prefs;
    prefs = await SharedPreferences.getInstance();
    var d = prefs.getString('profile');
    Login.User user = Login.User.fromJson(json.decode(d));
    _currentUser = user;
    print(_currentUser);
    var t = prefs.getString('token');
    final AuthModel auth = AuthModel.fromJson(t);
    print("TOKEN AGBA::::::::: ${auth.token}");
    _token = auth;
    print('refresh');
    _navigationService.navigateReplacementTo(BottomNavigationRoute);
    // return SuccessModel(user);
  }

  registerPhone(Map<dynamic, dynamic> payload) async {

    try {
      final result = await http.post(Paths.REGISTER_PHONE, payload);
      if (result is ErrorModel) {
        print("ERROR");
        print(result.error);
        return ErrorModel(result.error);
      }

      print("RESULT");
      print(result.data);

      //  final AuthModel auth = AuthModel.fromJson(result.data['data']['token']);
      //print("TOKEN AGBA::::::::: ${auth.token}");
      //_token = auth;

      RegisterPhoneNoResponse registerPhoneNoResponse =
          RegisterPhoneNoResponse.fromJson(result.data);
      print(registerPhoneNoResponse.data);
      showToast(registerPhoneNoResponse.data.toString());
      // _currentUser = data;
      // return SuccessModel({'auth': auth, 'user': user});
      return SuccessModel(registerPhoneNoResponse);
    } catch (e) {
      print(e.toString());
      return ErrorModel('$e');
    }
  }

  verifyOtp(Map<dynamic, dynamic> payload) async {
    try {
      final result = await http.post(Paths.OTP_VERIFY, payload);
      if (result is ErrorModel) {
        print("ERROR");
        print(result.error);

        return ErrorModel(result.error);
      }

      print("RESULT");
      print(result.data);

      // VerifyOtpResponse verifyOtpResponse =
      //     VerifyOtpResponse.fromJson(result.data);

      return SuccessModel(result.data);
    } catch (e) {
      print(e.toString());
      return ErrorModel('$e');
    }
  }

  resendOtp(Map<dynamic, dynamic> payload) async {
    try {
      final result = await http.post(Paths.OTP_RESEND, payload);
      if (result is ErrorModel) {
        print("ERROR");
        print(result.error);

        return ErrorModel(result.error);
      }

      print("RESULT");
      print(result.data);

      ResendOtpResponse resendOtpResponse =
          ResendOtpResponse.fromJson(result.data);
      print(resendOtpResponse.message);
      return SuccessModel(resendOtpResponse.message);
    } catch (e) {
      print(e.toString());
      return ErrorModel('$e');
    }
  }

  registrationFinal(Map<dynamic, dynamic> payload) async {
    try {
      final result = await http.post(Paths.REGISTER_FINAL, payload);
      if (result is ErrorModel) {
        print("ERROR");
        print('errror: ${result.error}');
        return ErrorModel(result.error);
      }

      print("RESULT");
      print(result.data);

      FinalRegResponse finalRegResponse = FinalRegResponse.fromJson(result.data);
      return SuccessModel(finalRegResponse);
    } catch (e) {
      print(e.toString());
      return ErrorModel('$e');
    }
  }

  getZone() async {
    try {
      final result = await http.get(Paths.ZONE);
      if (result is ErrorModel) {
        print("ERROR");
        print(result.error);
        return ErrorModel(result.error);
      }

      print("RESULT");
      print(result.data);
      return SuccessModel(result.data);
    } catch (e) {
      print(e.toString());
      return ErrorModel('$e');
    }
  }

  getCity(String id) async {
    try {
      final result = await http.get(Paths.ZONE_CITY + id);
      if (result is ErrorModel) {
        print("ERROR");
        print(result.error);

        return ErrorModel(result.error);
      }

      print("RESULT");
      print(result.data);
      return SuccessModel(result.data);
    } catch (e) {
      print(e.toString());
      return ErrorModel('$e');
    }
  }

  getDistrict(String id) async {
    try {
      final result = await http.get(Paths.CITY_DISTRICT + id);
      if (result is ErrorModel) {
        print("ERROR");
        print(result.error);

        return ErrorModel(result.error);
      }

      print("RESULT");
      print(result.data);
      return SuccessModel(result.data);
    } catch (e) {
      print(e.toString());
      return ErrorModel('$e');
    }
  }

  resetRequest(Map<dynamic, dynamic> payload) async {
    try {
      final result = await http.post(Paths.RESET_PASSWORD, payload);
      if (result is ErrorModel) {
        print("ERROR");
        print(result.error);

        return ErrorModel(result.error);
      }

      print("RESULT");
      print(result.data);

      ResetPasswordRequestResponse resetPasswordRequestResponse =
          ResetPasswordRequestResponse.fromJson(result.data);
      print(resetPasswordRequestResponse.data);
      return SuccessModel(resetPasswordRequestResponse);
    } catch (e) {
      print(e.toString());
      return ErrorModel('$e');
    }
  }

  resetOTP(Map<dynamic, dynamic> payload) async {
    try {
      final result = await http.post(Paths.RESET_OTP_VERIFY, payload);
      if (result is ErrorModel) {
        print("ERROR");
        print(result.error);

        return ErrorModel(result.error);
      }

      print("RESULT");
      print(result.data);

      return SuccessModel(result.data);
    } catch (e) {
      print(e.toString());
      return ErrorModel('$e');
    }
  }

  changePassword(Map<dynamic, dynamic> payload) async {
    try {
      final result = await http.post(Paths.RESET_UPDATE, payload);
      if (result is ErrorModel) {
        print("ERROR");
        print(result.error);

        return ErrorModel(result.error);
      }

      print("RESULT");
      print(result.data);

      ResetPasswordUpdateResponse resetPasswordUpdateResponse =
          ResetPasswordUpdateResponse.fromJson(result.data);
      print(resetPasswordUpdateResponse.data);
      return SuccessModel(resetPasswordUpdateResponse);
    } catch (e) {
      print(e.toString());
      return ErrorModel('$e');
    }
  }

  updatePassword(Map<dynamic, dynamic> payload) async {
    try {
      final result = await http.post(Paths.UPDATE_PROFILE, payload);
      if (result is ErrorModel) {
        print("ERROR");
        print(result.error);

        return ErrorModel(result.error);
      }

      print("RESULT");
      print(result.data);
      //
      // VerifyOtpResponse verifyOtpResponse =
      // VerifyOtpResponse.fromJson(result.data);
      // print(verifyOtpResponse.message);
      return SuccessModel(result.data);
    } catch (e) {
      print(e.toString());
      return ErrorModel('$e');
    }
  }

  updateEmail(Map<dynamic, dynamic> payload) async {
    try {
      final result = await http.post(Paths.UPDATE_EMAIL, payload);
      if (result is ErrorModel) {
        print("ERROR");
        print(result.error);

        return ErrorModel(result.error);
      }
      Login.User user = Login.User.fromJson(result.data['data']);
      //showToast(result.data['message']);
      _currentUser = user;
      SharedPreferences prefs;
      prefs = await SharedPreferences.getInstance();
      await prefs.setString('userId', user.phoneNumber);
      var s = json.encode(_currentUser);
      await prefs.setString('profile', s);
      print("RESULT");
      print(result.data);
      //
      // VerifyOtpResponse verifyOtpResponse =
      // VerifyOtpResponse.fromJson(result.data);
      // print(verifyOtpResponse.message);
      return SuccessModel(result.data);
    } catch (e) {
      print(e.toString());
      return ErrorModel('$e');
    }
  }

  updateAddress(Map<dynamic, dynamic> payload) async {
    try {
      final result = await http.post(Paths.UPDATE_ZONE, payload);
      if (result is ErrorModel) {
        print("ERROR");
        print(result.error);

        return ErrorModel(result.error);
      }

      print("RESULT");
      print(result.data);
      Login.User user = Login.User.fromJson(result.data['data']);
      //showToast(result.data['message']);
      _currentUser = user;
      SharedPreferences prefs;
      prefs = await SharedPreferences.getInstance();
      await prefs.setString('userId', user.phoneNumber);
      var s = json.encode(_currentUser);
      await prefs.setString('profile', s);
      return SuccessModel(result.data);
    } catch (e) {
      print(e.toString());
      return ErrorModel('$e');
    }
  }

  updateName(Map<dynamic, dynamic> payload) async {
    try {
      final result = await http.post(Paths.UPDATE_NAME, payload);
      if (result is ErrorModel) {
        print("ERROR");
        print(result.error);

        return ErrorModel(result.error);
      }

      print("RESULT");
      print(result.data);
      Login.User user = Login.User.fromJson(result.data['data']);
      //showToast(result.data['message']);
      _currentUser = user;
      SharedPreferences prefs;
      prefs = await SharedPreferences.getInstance();
      await prefs.setString('userId', user.phoneNumber);
      var s = json.encode(_currentUser);
      await prefs.setString('profile', s);

      return SuccessModel(result.data);
    } catch (e) {
      print(e.toString());
      return ErrorModel('$e');
    }
  }

  uploadImage(File image, String token) async {
    var header = {
      'Content-Type': 'application/json; charset=UTF-8',
      "Authorization": "Bearer $token"
    };

    // setState(() {
    //  // isUploading = true;
    //   profilePr.show();
    // });
    print(image.toString());
    // Find the mime type of the selected file by looking at the header bytes of the file
    final mimeTypeData =
        lookupMimeType(image.path, headerBytes: [0xFF, 0xD8]).split('/');
    // Intilize the multipart request
    final imageUploadRequest =
        htp.MultipartRequest('POST', Uri.parse(baseUrl + 'user/profile/image'));
    imageUploadRequest.headers.addAll(header);
    // Attach the file in the request
    final file = await htp.MultipartFile.fromPath('avatar', image.path,
        contentType: MediaType(mimeTypeData[0], mimeTypeData[1]));
    // Explicitly pass the extension of the image with request body
    // Since image_picker has some bugs due which it mixes up
    // image extension with file name like this filenamejpge
    // Which creates some problem at the server side to manage
    // or verify the file extension
    imageUploadRequest.fields['ext'] = mimeTypeData[1];
    imageUploadRequest.files.add(file);

    try {
      final streamedResponse = await imageUploadRequest.send();
      final response = await htp.Response.fromStream(streamedResponse);
      if (response.statusCode != 200) {
        print(response.body);
        return null;
      }
      final Map<String, dynamic> responseData = json.decode(response.body);
      //_resetState();
      print(responseData);
      Login.User user = Login.User.fromJson(responseData['data']);
      //showToast(result.data['message']);
      _currentUser = user;
      SharedPreferences prefs;
      prefs = await SharedPreferences.getInstance();
      await prefs.setString('userId', user.phoneNumber);
      var s = json.encode(_currentUser);
      await prefs.setString('profile', s);

      return responseData;
      //  return responseData;
    } catch (e) {
      print(e);
      // _resetState();
      return null;
    }
  }
}
