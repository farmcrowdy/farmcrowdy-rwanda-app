import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider_architecture/provider_architecture.dart';
import 'package:rwandashop/core/model/authentication/change_password.dart';
import 'package:rwandashop/core/services/analytics_service.dart';
import 'package:rwandashop/core/services/auth_service.dart';
import 'package:rwandashop/ui/screens/account/account_view_model.dart';
import 'package:rwandashop/ui/widget/generalButton.dart';
import 'package:rwandashop/utils/app_localizations.dart';
import 'package:rwandashop/utils/helpers.dart';
import 'package:rwandashop/utils/locator.dart';


class ChangePasswordPage extends StatefulWidget {
  @override
  _Pickup createState() => _Pickup();
}

class _Pickup extends State<ChangePasswordPage> {
  TextEditingController oldPasswordController = TextEditingController();
  TextEditingController newPasswordController = TextEditingController();
  bool passwordVisible;
  bool passwordVisible2;
  final Authentication _authentication = locator<Authentication>();
  final Analytics _analytics = locator<Analytics>();


  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    passwordVisible = true;
    passwordVisible2 = true;
  }

  @override
  Widget build(BuildContext context) {
    AppLocalizations tr = AppLocalizations.of(context);
    return ViewModelProvider<AccountViewModel>.withConsumer(
      onModelReady: (model){
        _analytics.setCurrentScreen('Change password', 'Change password');
      },
        viewModelBuilder: () => AccountViewModel(),
        builder: (context, model, child) {
          return
            Scaffold(
              body:
              SingleChildScrollView(
                child:

                Column(

                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[

                    Container(
                      padding: EdgeInsets.only(top: 60),
                      child:
                      Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                      Container(
                      padding: EdgeInsets.only(left: 10),
                  child:
                            Row(
                                children: <Widget>[

                                  GestureDetector(
                                    onTap: () {
                                      Navigator.pop(context);
                                    },
                                    child:
                               Icon(
                                 Icons.arrow_back_ios
                               ),
                                  ),
                                  SizedBox(width: 20,),
                                  Text(
                                    tr.text('changePassword.security'),
                                    style: TextStyle(
                                        color: Colors.black,
                                        fontWeight: FontWeight.bold,
                                        fontSize: 20),
                                  ),
                                ])),
                            Divider(),
                            Column(

                              children: <Widget>[
                                Container(
                                  padding: EdgeInsets.all(20),
                                  child:
                                  Column(
                                    crossAxisAlignment: CrossAxisAlignment
                                        .start,
                                    children: <Widget>[
                                      Text(
                                        tr.text('changePassword.currentPassword'),
                                        style: TextStyle(
                                            color: Colors.black45,
                                            fontWeight: FontWeight.bold,
                                            fontSize: 12),),
                                      TextFormField(
                                        //controller: emailController,
                                        decoration: InputDecoration(
                                          suffixIcon: IconButton(
                                            icon: Icon(
                                              // Based on passwordVisible state choose the icon
                                              passwordVisible
                                                  ? Icons.visibility
                                                  : Icons.visibility_off,
                                              color: Colors.black,
                                            ),
                                            onPressed: () {
                                              // Update the state i.e. toogle the state of passwordVisible variable
                                              setState(() {
                                                passwordVisible =
                                                !passwordVisible;
                                              });
                                            },
                                          ),
//                    errorText: residentialValid
//                        ? null
//                        : 'Kindly update this field',
                                          hintText:
                                          tr.text('changePassword.enterPassword'),
                                          hintStyle: TextStyle(
                                            color: Colors.black45,
                                          ),
                                          labelStyle: TextStyle(
                                              color: Colors.blue),
                                          // border: new OutlineInputBorder(
                                          //   borderRadius: new BorderRadius.circular(5.0),
                                          //   borderSide: new BorderSide(),
                                          // ),
                                        ),
                                        obscureText: passwordVisible,
                                        keyboardType: TextInputType
                                            .visiblePassword,
                                        style: TextStyle(color: Colors.black),
                                        cursorColor: Colors.black,
                                        controller: oldPasswordController,
                                      ),
                                    ],
                                  ),
                                ),
                                Container(
                                  padding: EdgeInsets.all(20),
                                  child:
                                  Column(
                                    crossAxisAlignment: CrossAxisAlignment
                                        .start,
                                    children: <Widget>[
                                      Text(
                                        tr.text('changePassword.newPassword'),
                                        style: TextStyle(
                                            color: Colors.black45,
                                            fontWeight: FontWeight.bold,
                                            fontSize: 12),),
                                      TextFormField(
                                        //controller: emailController,
                                        decoration: InputDecoration(
                                          suffixIcon: IconButton(
                                            icon: Icon(
                                              // Based on passwordVisible state choose the icon
                                              passwordVisible2
                                                  ? Icons.visibility
                                                  : Icons.visibility_off,
                                              color: Colors.black,
                                            ),
                                            onPressed: () {
                                              // Update the state i.e. toogle the state of passwordVisible variable
                                              setState(() {
                                                passwordVisible2 =
                                                !passwordVisible2;
                                              });
                                            },
                                          ),
//                    errorText: residentialValid
//                        ? null
//                        : 'Kindly update this field',
                                          hintText:
                                          '********',
                                          hintStyle: TextStyle(
                                            color: Colors.black45,
                                          ),
                                          labelStyle: TextStyle(
                                              color: Colors.blue),
                                          // border: new OutlineInputBorder(
                                          //   borderRadius: new BorderRadius.circular(5.0),
                                          //   borderSide: new BorderSide(),
                                          //),
                                        ),
                                        keyboardType: TextInputType
                                            .visiblePassword,
                                        style: TextStyle(color: Colors.black),
                                        cursorColor: Colors.black,
                                        obscureText: passwordVisible2,
                                        controller: newPasswordController,
                                      ),
                                    ],
                                  ),
                                ),

                                Container(
                                    margin: EdgeInsets.all(20),
                                    child:
                                    GeneralButton(
                                      onPressed: (){
                                        if(oldPasswordController.text.isNotEmpty && newPasswordController.text.isNotEmpty) {
                                          model.changePassword(ChangePassword(
                                              phoneNumber: _authentication
                                                  .phoneNumber,
                                              password: oldPasswordController
                                                  .text,
                                              newPassword: newPasswordController
                                                  .text
                                          ));
                                        }else{
                                          showToast(tr.text(
                                              'changeAddress.fieldCompulsory'));
                                        }
                                      },
                                      buttonText:
                                      tr.text('changeAddress.saveChanges'),
                                    )
                                )
                              ],
                            )
                          ]
                      ),
                    ),
                  ],
                ),
              ),
            );
        }
    );
  }
}