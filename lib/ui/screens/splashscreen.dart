import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:rwandashop/core/services/auth_service.dart';
import 'package:rwandashop/core/services/language_services.dart';
import 'package:rwandashop/utils/locator.dart';
import 'package:rwandashop/utils/router/navigationService.dart';
import 'package:rwandashop/utils/router/routeNames.dart';
import 'package:shared_preferences/shared_preferences.dart';

// This is the slash-screen first screen to display after launch
class AnimatedSplashScreen extends StatefulWidget {
  @override
  SplashScreenState createState() => new SplashScreenState();
}

class SplashScreenState extends State<AnimatedSplashScreen>
    with SingleTickerProviderStateMixin {
  var _visible = true;
  final NavigationService _navigationService = locator<NavigationService>();
  final Authentication _authentication = locator<Authentication>();
  final LanguageService languageService = locator<LanguageService>();
  AnimationController animationController;
  Animation<double> animation;

  startTime() async {
    var _duration = new Duration(seconds: 4);
    return new Timer(_duration, navigationPage);
  }

  void navigationPage() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();

    var id = prefs.getString('profile');

    print(id);
    if (id != null) {
      _authentication.alreadyLoggedIn();
    }
    // _navigationService.navigateReplacementTo(BottomNavigationRoute);
    // print(languageService.hasPrefferedLanguage);
    // _navigationService.navigateTo(WelcomeRoute);
    print('currentUser ${_authentication.currentUser.toString()}');
    _authentication.currentUser == null
        ? languageService.hasPrefferedLanguage
            ? _navigationService.navigateReplacementTo(WelcomeRoute) //BottomNavigationRoute
            : _navigationService.navigateReplacementTo(LanguageScreenRoute)
        : _navigationService.navigateReplacementTo(BottomNavigationRoute);
  }

  @override
  void initState() {
    super.initState();
    // animationController = new AnimationController(
    //     vsync: this, duration: new Duration(seconds: 2));
    // animation =
    //     new CurvedAnimation(parent: animationController, curve: Curves.easeOut);
    // animation.addListener(() => this.setState(() {}));
    // animationController.forward();
    setState(() {
      _visible = !_visible;
    });
    startTime();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        fit: StackFit.expand,
        children: <Widget>[
          new Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                new Image.asset(
                  'assets/images/logo_new.png',
                  width: 200.w,
                  height: 200.h,
                  // width: animation.value * 250,
                  // height: animation.value * 250,
                ),
              ]),
        ],
      ),
    );
  }
}
