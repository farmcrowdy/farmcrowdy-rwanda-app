import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:intl_phone_number_input/intl_phone_number_input.dart';

import 'package:provider_architecture/provider_architecture.dart';
import 'package:rwandashop/core/model/authentication/login.dart';
import 'package:rwandashop/core/services/analytics_service.dart';
import 'package:rwandashop/ui/screens/login/loginViewModel.dart';
import 'package:rwandashop/ui/widget/generalButton.dart';
import 'package:rwandashop/utils/app_localizations.dart';
import 'package:rwandashop/utils/colors.dart';
import 'package:rwandashop/utils/locator.dart';
import 'package:rwandashop/utils/router/navigationService.dart';
import 'package:rwandashop/utils/router/routeNames.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class LoginPage extends StatefulWidget {
  @override
  LoginState createState() => new LoginState();
}

class LoginState extends State<LoginPage> {
  TextEditingController numberController = TextEditingController();
  TextEditingController passwordController = TextEditingController();
  final Analytics _analytics = locator<Analytics>();
  final NavigationService _navigationService = locator<NavigationService>();
  bool passwordVisible;
  String gender = 'Select Zone';

  String initialCountry = 'RW';
  PhoneNumber number = PhoneNumber(isoCode: 'RW');

  void getPhoneNumber(String phoneNumber) async {
    PhoneNumber number =
    await PhoneNumber.getRegionInfoFromPhoneNumber(phoneNumber, 'RW');

    setState(() {
      this.number = number;
    });
  }

  @override
  void initState() {
    super.initState();
    passwordVisible = true;
  }


  @override
  Widget build(BuildContext context) {
    AppLocalizations tr = AppLocalizations.of(context);
    return ViewModelProvider<LoginViewModel>.withConsumer(
        onModelReady: (model) {
          _analytics.setCurrentScreen('Login page', 'Login');
        },
        viewModelBuilder: () => LoginViewModel(),
        builder: (context, model, child) {
          return Scaffold(
            body: SingleChildScrollView(
              child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    SizedBox(
                      height: 40.h,
                    ),
                    // Container(
                    //   padding: EdgeInsets.all(20.w),
                    //   child: Row(
                    //     mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    //     children: [
                    //       InkWell(
                    //         onTap: () {
                    //           _navigationService.pop();
                    //         },
                    //         child: Icon(Icons.arrow_back_ios),
                    //       ),
                    //       Text(''),
                    //     ],
                    //   ),
                    // ),
                    SizedBox(
                      height: 40.h,
                    ),
                    Container(
                      height: 50.w,
                      width: 50.w,
                      child: Image.asset(
                        'assets/images/icon_new.png',
                        fit: BoxFit.contain,
                      ),
                    ),
                    SizedBox(
                      height: 40.h,
                    ),
                    Container(
                      padding: EdgeInsets.only(left: 20.w, right: 20.w),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Container(
                            child: Text(
                              tr.text('login.login'),
                              style: TextStyle(
                                  fontSize: 26.ssp,
                                  fontWeight: FontWeight.bold),
                            ),
                          ),
                          Text('')
                        ],
                      ),
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Container(
                      padding: EdgeInsets.only(left: 20.w, right: 20.w),
                      child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Flexible(
                                child: Container(
                              child: Text(
                                tr.text('login.enterPassword'),
                                style: TextStyle(
                                    fontSize: 15.ssp,
                                    color: Color(0xff7C7C7C),
                                    fontWeight: FontWeight.w400),
                              ),
                            )),
                          ]),
                    ),
                    SizedBox(height: 40.h),
                    Container(
                      padding: EdgeInsets.only(left: 20.w, right: 20.w),
                      child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Text(
                              tr.text('login.yourPhoneNumber'),
                              style: TextStyle(
                                  fontSize: 16.ssp,
                                  color: Color(0xff7C7C7C),
                                  fontWeight: FontWeight.bold),
                            ),
                            Text('')
                          ]),
                    ),
                    SizedBox(height: 10.h),
                    Container(
                      padding: EdgeInsets.only(left: 20.w, right: 20.w),
                      child: InternationalPhoneNumberInput(
                        onInputChanged: (PhoneNumber number) {
                          print(number.phoneNumber);
                        },
                        onInputValidated: (bool value) {
                          print(value);
                        },
                        countries: ['RW'],
                        selectorConfig: SelectorConfig(
                          selectorType: PhoneInputSelectorType.BOTTOM_SHEET,
                        ),
                        ignoreBlank: false,
                        autoValidateMode: AutovalidateMode.disabled,
                        textStyle: TextStyle(fontWeight: FontWeight.w700),
                        selectorTextStyle: TextStyle(color: Colors.black),
                        initialValue: number,
                        textFieldController: numberController,
                        formatInput: false,
                        keyboardType:
                        TextInputType.numberWithOptions(signed: true, decimal: true),
                        onSaved: (PhoneNumber number) {
                          print('On Saved: $number');
                        },
                      ),

//                       TextFormField(
//                         controller: numberController,
//                         // maxLength: 11,
//                         decoration: InputDecoration(
// //                    errorText: residentialValid
// //                        ? null
// //                        : 'Kindly update this field',
//                           hintText: '08100000000',
//                           hintStyle: TextStyle(
//                             color: Colors.black45,
//                           ),
//                           labelStyle: TextStyle(color: Colors.blue),
//                           // border: new OutlineInputBorder(
//                           //   borderRadius: new BorderRadius.circular(5.0),
//                           //   borderSide: new BorderSide(),
//                           // ),
//                         ),
//                         keyboardType: TextInputType.phone,
//                         style: TextStyle(color: Colors.black),
//                         cursorColor: Colors.black,
//                       ),
                    ),
                    SizedBox(height: 20.h),
                    Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Container(
                            padding: EdgeInsets.only(left: 20.w, right: 20.w),
                            child: Text(
                              tr.text('login.password'),
                              style: TextStyle(
                                  fontSize: 16.ssp,
                                  color: Color(0xff7C7C7C),
                                  fontWeight: FontWeight.bold),
                            ),
                          ),
                          Text('')
                        ]),
                    Container(
                      padding: EdgeInsets.only(left: 20.w, right: 20.w),
                      child: TextFormField(
                        controller: passwordController,
                        // maxLength: 11,
                        decoration: InputDecoration(
                          suffixIcon: IconButton(
                            icon: Icon(
                              // Based on passwordVisible state choose the icon
                              passwordVisible
                                  ? Icons.visibility
                                  : Icons.visibility_off,
                              color: Colors.black,
                            ),
                            onPressed: () {
                              // Update the state i.e. toogle the state of passwordVisible variable
                              setState(() {
                                passwordVisible = !passwordVisible;
                              });
                            },
                          ),
//                    errorText: residentialValid
//                        ? null
//                        : 'Kindly update this field',
                          hintText: '******',
                          hintStyle: TextStyle(
                            color: Colors.black45,
                          ),
                          labelStyle: TextStyle(color: Colors.blue),
                          // border: new OutlineInputBorder(
                          //   borderRadius: new BorderRadius.circular(5.0),
                          //   borderSide: new BorderSide(),
                          // ),
                        ),
                        obscureText: passwordVisible,
                        keyboardType: TextInputType.visiblePassword,
                        style: TextStyle(color: Colors.black),
                        cursorColor: Colors.black,
                      ),
                    ),
                    SizedBox(
                      height: 30.h,
                    ),
                    Container(
                      padding: EdgeInsets.only(left: 20.w, right: 20.w),
                      child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Text(''),
                            InkWell(
                              onTap: () {
                                _navigationService
                                    .navigateTo(ForgotPasswordRoute);
                              },
                              child: Text(
                                tr.text('login.forgotPassword'),
                                style: TextStyle(
                                    fontSize: 14.ssp,
                                    fontWeight: FontWeight.w400),
                              ),
                            )
                          ]),
                    ),
                    Container(
                        padding: EdgeInsets.all(20.w),
                        child: GeneralButton(
                          buttonText: tr.text('login.log'),
                          onPressed: () {
                            model.signIn(Login(
                                phoneNumber: numberController.text,
                                password: passwordController.text,
                                requestType: 'login'));
                          },
                        )),
                    SizedBox(
                      height: 30.h,
                    ),
                    Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Text(
                            tr.text('login.noAccount'),
                            style: TextStyle(
                                fontSize: 14.ssp, fontWeight: FontWeight.w400),
                          ),
                          InkWell(
                            onTap: () {
                              _navigationService.navigateTo(RegisterRoute);
                            },
                            child: Text(
                              tr.text('login.signUp'),
                              style: TextStyle(
                                  fontSize: 14.ssp,
                                  color: AppColors.themeGreen,
                                  fontWeight: FontWeight.w400),
                            ),
                          ),
                        ]),
                  ]),
            ),
          );
        });
  }
}
