import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:rwandashop/core/services/auth_service.dart';
import 'package:rwandashop/utils/locator.dart';

class Analytics {
  final Authentication _authentication = locator<Authentication>();
  String _message = '';
  void setMessage(String message) {
    _message = message;
    print(_message);
  }

  Future<void> setCurrentScreen(String screenName, String screenDetails) async {
    FirebaseAnalytics analytics = FirebaseAnalytics();
    await analytics.setCurrentScreen(
      screenName: screenName,
      screenClassOverride: screenDetails,
    );
    setMessage('setCurrentScreen succeeded');
  }

  Future<void> sendAnalyticsFrontPage() async {
    FirebaseAnalytics analytics = FirebaseAnalytics();
    await analytics.logEvent(
      name: 'FrontPage_launch',
      parameters: <String, dynamic>{
        'name': _authentication.currentUser.name,
        'phoneNo': _authentication.currentUser.phoneNumber,
        'email': _authentication.currentUser.email,
        'time': DateTime.now().toString(),
      },
    );
    setMessage('LogHomePage succeeded');
  }

  Future<void> setUserId() async {
    FirebaseAnalytics analytics = FirebaseAnalytics();
    await analytics.setUserId(_authentication.currentUser.phoneNumber);
    setMessage('setUserId succeeded');
  }

  Future<void> sendAnalyticsCategorySelected(String category) async {
    FirebaseAnalytics analytics = FirebaseAnalytics();

    await analytics.logEvent(
      name: 'Category Selected',
      parameters: <String, dynamic>{
        'category': '$category',
        'time': DateTime.now().toString(),
        'phoneNo': _authentication.currentUser.phoneNumber,
        'email': _authentication.currentUser.email,
      },
    );
    setMessage('logCategory succeeded');
  }

  Future<void> sendAnalyticsItemSelected(String item) async {
    FirebaseAnalytics analytics = FirebaseAnalytics();

    await analytics.logEvent(
      name: 'Item Selected',
      parameters: <String, dynamic>{
        'category': '$item',
        'time': DateTime.now().toString(),
        'phoneNo': _authentication.currentUser.phoneNumber,
        'email': _authentication.currentUser.email,
      },
    );
    setMessage('logCategory succeeded');
  }

  Future<void> addToCart(
      int price, int id, String name, String categories, int quantity) async {
    FirebaseAnalytics analytics = FirebaseAnalytics();
    await analytics.logAddPaymentInfo();
    await analytics.logAddToCart(
      currency: 'NGN',
      value: price.toDouble(),
      itemId: '$id',
      itemName: name,
      itemCategory: categories,
      quantity: quantity,
      price: price.toDouble(),
      origin: 'Rwanda Shop',
      startDate: DateTime.now().toString(),
    );
    print('Cart Logged');
  }

  // Future<void> sendAnalyticsEcommerce(double amount, String referenceNo) async {
  //   FirebaseAnalytics analytics = FirebaseAnalytics();
  //
  //   await analytics.logEcommercePurchase(
  //     currency: 'NGN',
  //     value: amount,
  //     transactionId: referenceNo,
  //     startDate: DateTime.now().toString(),
  //   );
  //   print('logPackage Selected succeeded');
  //
  // }
}
