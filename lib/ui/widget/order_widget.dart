import 'package:flutter/material.dart';
import 'package:rwandashop/core/model/product/orders.dart';
import 'package:intl/intl.dart';
import 'package:rwandashop/ui/screens/order/order_details.dart';
import 'package:rwandashop/utils/app_localizations.dart';



// This is the widget for order history and ongoing orders
class OrderWidget extends StatelessWidget {
  final Data orderModel;
  final Function onTap;

  OrderWidget({this.orderModel, this.onTap});
  final oCcy = new NumberFormat("#,##0.00", "en_US");

  @override
  Widget build(BuildContext context) {
    AppLocalizations tr = AppLocalizations.of(context);
    return Column(children: [
      InkWell(
        onTap: () {
          Navigator.push(
              context,
              MaterialPageRoute(
                  builder: (BuildContext context) =>
                      OrderDetails(orderDetails: orderModel)));
        },
        child: Container(
            padding: EdgeInsets.all(22),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Text(
                      tr.text('orderDetails.orderId') +
                          " "+
                          orderModel.id.toString(),
                      style: TextStyle(
                          color: Colors.black,
                          fontWeight: FontWeight.bold,
                          fontSize: 16),
                    ),
                    Text(
                      orderModel.amount != null
                          ? 'RF ${oCcy.format(orderModel.amount)}'.toString()
                          : 5000.toString(),
                      style: TextStyle(
                          color: Colors.black,
                          fontWeight: FontWeight.w500,
                          fontSize: 16),
                    ),
                  ],
                ),
                SizedBox(
                  height: 20,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    // Flexible(
                    //   child: Text(
                    //     orderModel.location,
                    //     style: TextStyle(
                    //         color:
                    //         Colors.black,
                    //         fontSize: 12),
                    //   ),
                    // ),
                    Container(
                        height: 26,
                        child: FlatButton(
                            child: Text(
                              orderModel.statusName,
                              style: TextStyle(color: Color(0xff7D650E)),
                            ),
                            color: Color(0xFFB08B00).withOpacity(0.45),
                            shape: RoundedRectangleBorder(
                              borderRadius: new BorderRadius.circular(18.0),
                              //side: BorderSide(color: Colors.red)
                            ),
                            onPressed: () {
                              //   Navigator.of(context).pushNamed('/signup');
                            })),
                  ],
                ),
                SizedBox(
                  height: 10,
                ),
                Text(
                  orderModel.dateTime,
                  style: TextStyle(
                      color: Colors.black45,
                      fontWeight: FontWeight.w500,
                      fontSize: 12),
                ),
              ],
            )),
      ),
      Divider(
        thickness: 1,
        endIndent: 20,
        indent: 20,
      ),
    ]);
  }
}
