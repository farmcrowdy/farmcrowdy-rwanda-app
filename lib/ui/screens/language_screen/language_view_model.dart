import 'dart:ui';

import 'package:rwandashop/core/services/auth_service.dart';
import 'package:rwandashop/main.dart';
import 'package:rwandashop/ui/screens/welcomePage.dart';
import 'package:rwandashop/utils/baseModel.dart';
import 'package:rwandashop/utils/helpers.dart';
import 'package:rwandashop/utils/locator.dart';
import 'package:rwandashop/utils/router/navigationService.dart';
import 'package:rwandashop/utils/router/routeNames.dart';
import 'package:shared_preferences/shared_preferences.dart';

//Created by Daniel Makinde
//This the screen to select the preferred language

class LanguageScreenViewModel extends BaseModel {
  final NavigationService _navigationService = locator<NavigationService>();
  final Authentication _authentication = locator<Authentication>();
  Locale _currentLanguage;
  bool _hasPrefferedLanguage;

  Locale get currentLanguage => _currentLanguage;

  bool get hasPrefferedLanguage => _hasPrefferedLanguage;
  String selectedDropdownLanguage;
  bool appStart;

//List of the languages available

  Future<String> get selectedDropdownLanguageString async {
    var prefs = await SharedPreferences.getInstance();
    String languageCode = prefs.getString('language_code');
    return (null != languageCode) ? languageCode : 'en';
  }

  List<Map> myJsonLanguage = [
    {
      "id": "1",
      "image": "assets/images/enflag.jpg",
      "name": "English",
      "code": "en"
    },
    {
      "id": "2",
      "image": "assets/images/rwanda_flag.png",
      "name": "Kinyarwanda",
      "code": "ky"
    },
  ];

  //This saves the language selected
  changeLanguage(context, Locale newLanguage) async {
    print('new changed language is $newLanguage');
    if (_currentLanguage == newLanguage) return;

    var prefs = await SharedPreferences.getInstance();
    _currentLanguage = newLanguage;
    _hasPrefferedLanguage = true;
    await prefs.setString('language_code', newLanguage.languageCode);
    MyApp.setLocale(context, newLanguage);
    notifyListeners();
  }

  //This navigates to the right screen
  Future<dynamic> setPreferredLanguage(context) async {
    if (selectedDropdownLanguage == null) {
      showErrorToast('Please select a language to continue');
      return;
    }

    // TODO: One should go to the walk through the other home page
    changeLanguage(context, Locale(selectedDropdownLanguage));
    notifyListeners();

    (_authentication.currentUser == null)
        ? _navigationService.navigateReplacementTo(WelcomeRoute)
        : appStart
            ? _navigationService.navigateReplacementTo(BottomNavigationRoute)
            : _navigationService.pop();

    // appStart
    //     ? _navigationService.navigateReplacementTo(BottomNavigationRoute)
    //     : _navigationService.pop(): _navigationService.navigateReplacementTo(BottomNavigationRoute) ;
  }

  //This saves the language selected to locale
  setPreferredLanguageChanged(context) async {
    if (selectedDropdownLanguage == null) {
      showErrorToast('Please select a language to continue');
      return;
    }
    changeLanguage(context, Locale(selectedDropdownLanguage));
    await languageService.setLocale();
    notifyListeners();
  }

  //This navigates to the login screen
  Future<dynamic> setPreferredLanguageLogin(context) async {
    if (selectedDropdownLanguage == null) {
      showErrorToast('Please select a language to continue');
      return;
    }

    changeLanguage(context, Locale(selectedDropdownLanguage));
    notifyListeners();

    // TODO: this should go to login page
    _navigationService.navigateReplacementTo(WelcomeRoute);

    // appStart
    //     ? _navigationService.navigateReplacementTo(WelcomeRoute)
    //     : _navigationService.pop();
  }
}
