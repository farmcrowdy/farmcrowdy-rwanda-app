import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:rwandashop/core/model/product/orders.dart';
import 'package:provider_architecture/provider_architecture.dart';
import 'package:rwandashop/core/services/analytics_service.dart';
import 'package:rwandashop/core/services/auth_service.dart';
import 'package:rwandashop/ui/screens/login/second_login_view.dart';
import 'package:rwandashop/ui/screens/order/order_view_model.dart';
import 'package:rwandashop/ui/widget/generalButton.dart';
import 'package:rwandashop/ui/widget/order_widget.dart';
import 'package:rwandashop/utils/app_localizations.dart';
import 'package:rwandashop/utils/colors.dart';
import 'package:rwandashop/utils/locator.dart';
import 'package:rwandashop/utils/router/navigationService.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:rwandashop/utils/router/routeNames.dart';
import 'package:rwandashop/utils/screensize.dart';
import 'package:get/get.dart';
import 'package:shimmer/shimmer.dart';

class Order extends StatefulWidget {
  @override
  OrderState createState() => new OrderState();
}

class OrderState extends State<Order> {
  TextEditingController numberController = TextEditingController();
  final Authentication _authentication = locator<Authentication>();
  final NavigationService _navigationService = locator<NavigationService>();
  final Analytics _analytics = locator<Analytics>();
  bool passwordVisible;
  String gender = 'Select Zone';
  var content = "Ongoing";
  @override
  void initState() {
    super.initState();
    passwordVisible = true;
  }

  //
  @override
  Widget build(BuildContext context) {
    AppLocalizations tr = AppLocalizations.of(context);
    return ViewModelProvider<OrderViewModel>.withConsumer(
        onModelReady: (model) {
          _analytics.setCurrentScreen(
              'Orders page', 'List of all orders made by the user');
        },
        viewModelBuilder: () => OrderViewModel(),
        builder: (context, model, child) {
          if (_authentication.currentUser.isNull) {
            return Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Container(
                      padding: EdgeInsets.only(top: 60, left: 20, right: 20),
                      child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Row(
                              children: <Widget>[
                                Text(
                                  tr.text('order.order'),
                                  style: TextStyle(
                                      color: Colors.black,
                                      fontWeight: FontWeight.bold,
                                      fontSize: 20),
                                ),
                              ],
                            ),
                            Divider(
                              thickness: 1,
                            ),
                            Center(
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: <Widget>[
                                  SizedBox(
                                    height: 150,
                                  ),
                                  Text(
                                    tr.text('order.signOrLog'),
                                    style: TextStyle(
                                      fontWeight: FontWeight.bold,
                                      fontSize: 20,
                                    ),
                                  ),
                                  Text(
                                    tr.text('order.toViewOrder'),
                                    style: TextStyle(
                                      fontWeight: FontWeight.bold,
                                      fontSize: 20,
                                    ),
                                  ),
                                  SizedBox(
                                    height: 20,
                                  ),
                                  Container(
                                    padding: EdgeInsets.all(20),
                                    child: GeneralButton(
                                      onPressed: () {
                                        _navigationService
                                            .navigateTo(WelcomeRoute);
                                      },
                                      buttonText: tr.text('order.getStarted'),
                                    ),
                                  ),
                                  InkWell(
                                    onTap: () {
                                      Navigator.push(
                                        context,
                                        new MaterialPageRoute(
                                            builder: (context) =>
                                                new SecondLoginPage(
                                                    pageIndex: 1)),
                                      );
                                    },
                                    child: Text(
                                      tr.text('order.logIn'),
                                      style: TextStyle(
                                        fontWeight: FontWeight.bold,
                                        color: AppColors.green,
                                        fontSize: 15,
                                      ),
                                    ),
                                  )
                                ],
                              ),
                            )
                          ]))
                ]);
          } else
            return Scaffold(
              body: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    SizedBox(
                      height: 60.h,
                    ),
                    Container(
                      padding: EdgeInsets.only(left: 20, right: 20),
                      child:
                          // InkWell(
                          //   onTap: (){
                          //     _navigationService.pop();
                          //   },
                          //   child:
                          //   Icon(
                          //
                          //     Icons.arrow_back_ios, color: Colors.black,
                          //   ),),
                          Container(
                        child: Text(
                          tr.text('order.order'),
                          style: TextStyle(
                              fontSize: 18.ssp, fontWeight: FontWeight.bold),
                        ),
                      ),
                    ),
                    Container(
                      height: 50,
                      child: Flex(
                        direction: Axis.vertical,
                        children: [
                          Expanded(
                            child: DefaultTabController(
                              length: 2,
                              child: Column(
                                children: <Widget>[
                                  Container(
                                    constraints:
                                        BoxConstraints.expand(height: 50),
                                    child: TabBar(
                                        onTap: (index) {
                                          switch (index) {
                                            case 0:
                                              setState(() {
                                                content = "Ongoing";
                                              });

                                              break;
                                            case 1:
                                              setState(() {
                                                content = "history";
                                              });
                                              break;
                                            default:
                                              content = "Other";
                                              break;
                                          }
                                          print(
                                              "You are clicking the $content");
                                        },
                                        indicatorColor: Colors.blue,
                                        unselectedLabelColor: Colors.black,
                                        labelColor: AppColors.green,
                                        tabs: [
                                          Tab(
                                            text: tr.text('order.onGoing'),
                                          ),
                                          Tab(
                                            text: tr.text('order.history'),
                                          ),
                                        ]),
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                    SizedBox(
                      height: 10.h,
                    ),
                    content == 'Ongoing'
                        ? Expanded(
                            child: FutureBuilder<List<Data>>(
                                future: model.getOrderOnGoing(),
                                builder: (context, snapshot) {
                                  if (!snapshot.hasData) {
                                    return Container(
                                      padding: EdgeInsets.all(20.0),
                                      child: Shimmer.fromColors(
                                          direction: ShimmerDirection.ltr,
                                          period: Duration(seconds: 2),
                                          child: ListView(
                                            children: [0, 1, 2, 3, 4, 5]
                                                .map((_) => Padding(
                                                      padding:
                                                          const EdgeInsets.all(
                                                              8.0),
                                                      child: Row(
                                                        crossAxisAlignment:
                                                            CrossAxisAlignment
                                                                .start,
                                                        children: [
                                                          Container(
                                                            width: Responsive
                                                                    .width(
                                                                        context) -
                                                                20,
                                                            height: 150,
                                                            color: Colors.white,
                                                          ),
                                                          Padding(
                                                            padding:
                                                                const EdgeInsets
                                                                        .symmetric(
                                                                    horizontal:
                                                                        8.0),
                                                          ),
                                                          Expanded(
                                                            child: Column(
                                                              crossAxisAlignment:
                                                                  CrossAxisAlignment
                                                                      .start,
                                                              children: [
                                                                Container(
                                                                  width: double
                                                                      .infinity,
                                                                  height: 8.0,
                                                                  color: Colors
                                                                      .white,
                                                                ),
                                                                Padding(
                                                                  padding: const EdgeInsets
                                                                          .symmetric(
                                                                      vertical:
                                                                          2.0),
                                                                ),
                                                                Container(
                                                                  width: double
                                                                      .infinity,
                                                                  height: 8.0,
                                                                  color: Colors
                                                                      .white,
                                                                ),
                                                                Padding(
                                                                  padding: const EdgeInsets
                                                                          .symmetric(
                                                                      vertical:
                                                                          2.0),
                                                                ),
                                                                Container(
                                                                  width: 40.0,
                                                                  height: 8.0,
                                                                  color: Colors
                                                                      .white,
                                                                ),
                                                              ],
                                                            ),
                                                          )
                                                        ],
                                                      ),
                                                    ))
                                                .toList(),
                                          ),
                                          baseColor: AppColors.grey,
                                          highlightColor: Colors.white),
                                    );
                                  } else if (snapshot.data.isNotEmpty) {
                                    return ListView(
                                        // scrollDirection: Axis.horizontal,
                                        shrinkWrap: true,
                                        children: snapshot.data
                                            .map((feed) =>
                                                OrderWidget(orderModel: feed))
                                            .toList());
                                  } else if (snapshot.hasError) {
                                    return Center(
                                        child: Column(
                                      children: <Widget>[
                                        SizedBox(
                                          height: 100,
                                        ),
                                        Text(
                                          tr.text('order.error'),
                                          style: TextStyle(
                                            fontWeight: FontWeight.bold,
                                            fontSize: 20,
                                          ),
                                        ),
                                        SizedBox(
                                          height: 10,
                                        ),
                                        Text(tr.text('order.notConnected')),
                                        SizedBox(
                                          height: 100,
                                        ),
                                      ],
                                    ));
                                  } else {
                                    return Center(
                                        child: Column(
                                      children: <Widget>[
                                        SizedBox(
                                          height: 100,
                                        ),
                                        Text(
                                          tr.text('order.noOrder'),
                                          style: TextStyle(
                                            fontWeight: FontWeight.bold,
                                            fontSize: 20,
                                          ),
                                        ),
                                        SizedBox(
                                          height: 10,
                                        ),
                                        Column(
                                          children: <Widget>[
                                            Text(tr
                                                .text('order.allNotification')),
                                            Text(tr.text('order.foundHere')),
                                          ],
                                        ),
                                        SizedBox(
                                          height: 100,
                                        ),
                                      ],
                                    ));
                                  }
                                }),
                          )
                        : Expanded(
                            child: FutureBuilder<List<Data>>(
                                future: model.getOrderHistory(),
                                builder: (context, snapshot) {
                                  if (!snapshot.hasData) {
                                    return Container(
                                      padding: EdgeInsets.all(20.0),
                                      child: Shimmer.fromColors(
                                          direction: ShimmerDirection.ltr,
                                          period: Duration(seconds: 2),
                                          child: ListView(
                                            children: [0, 1, 2, 3, 4, 5]
                                                .map((_) => Padding(
                                                      padding:
                                                          const EdgeInsets.all(
                                                              8.0),
                                                      child: Row(
                                                        crossAxisAlignment:
                                                            CrossAxisAlignment
                                                                .start,
                                                        children: [
                                                          Container(
                                                            width: Responsive
                                                                    .width(
                                                                        context) -
                                                                20,
                                                            height: 150,
                                                            color: Colors.white,
                                                          ),
                                                          Padding(
                                                            padding:
                                                                const EdgeInsets
                                                                        .symmetric(
                                                                    horizontal:
                                                                        8.0),
                                                          ),
                                                          Expanded(
                                                            child: Column(
                                                              crossAxisAlignment:
                                                                  CrossAxisAlignment
                                                                      .start,
                                                              children: [
                                                                Container(
                                                                  width: double
                                                                      .infinity,
                                                                  height: 8.0,
                                                                  color: Colors
                                                                      .white,
                                                                ),
                                                                Padding(
                                                                  padding: const EdgeInsets
                                                                          .symmetric(
                                                                      vertical:
                                                                          2.0),
                                                                ),
                                                                Container(
                                                                  width: double
                                                                      .infinity,
                                                                  height: 8.0,
                                                                  color: Colors
                                                                      .white,
                                                                ),
                                                                Padding(
                                                                  padding: const EdgeInsets
                                                                          .symmetric(
                                                                      vertical:
                                                                          2.0),
                                                                ),
                                                                Container(
                                                                  width: 40.0,
                                                                  height: 8.0,
                                                                  color: Colors
                                                                      .white,
                                                                ),
                                                              ],
                                                            ),
                                                          )
                                                        ],
                                                      ),
                                                    ))
                                                .toList(),
                                          ),
                                          baseColor: AppColors.grey,
                                          highlightColor: Colors.white),
                                    );
                                  } else if (snapshot.data.isNotEmpty) {
                                    return ListView(
                                        // scrollDirection: Axis.horizontal,
                                        shrinkWrap: true,
                                        children: snapshot.data
                                            .map((feed) =>
                                                OrderWidget(orderModel: feed))
                                            .toList());
                                  } else if (snapshot.hasError) {
                                    return Center(
                                        child: Column(
                                      children: <Widget>[
                                        SizedBox(
                                          height: 100,
                                        ),
                                        Text(
                                          tr.text('order.error'),
                                          style: TextStyle(
                                            fontWeight: FontWeight.bold,
                                            fontSize: 20,
                                          ),
                                        ),
                                        SizedBox(
                                          height: 10,
                                        ),
                                        Text(tr.text('order.notConnected')),
                                        SizedBox(
                                          height: 100,
                                        ),
                                      ],
                                    ));
                                  } else {
                                    return Center(
                                        child: Column(
                                      children: <Widget>[
                                        SizedBox(
                                          height: 100,
                                        ),
                                        Text(
                                          tr.text('order.noOrder'),
                                          style: TextStyle(
                                            fontWeight: FontWeight.bold,
                                            fontSize: 20,
                                          ),
                                        ),
                                        SizedBox(
                                          height: 10,
                                        ),
                                        Column(
                                          children: <Widget>[
                                            Text(tr
                                                .text('order.allNotification')),
                                            Text(tr.text('order.foundHere')),
                                          ],
                                        ),
                                        SizedBox(
                                          height: 100,
                                        ),
                                      ],
                                    ));
                                  }
                                }),
                          ),
                  ]),
            );
        });
  }
}
