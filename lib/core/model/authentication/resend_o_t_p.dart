/// request_type : ""
/// phone_number : ""

class ResendOTP {
  String _requestType;
  String _phoneNumber;

  String get requestType => _requestType;
  String get phoneNumber => _phoneNumber;

  ResendOTP({
      String requestType, 
      String phoneNumber}){
    _requestType = requestType;
    _phoneNumber = phoneNumber;
}

  ResendOTP.fromJson(dynamic json) {
    _requestType = json["request_type"];
    _phoneNumber = json["phone_number"];
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map["request_type"] = _requestType;
    map["phone_number"] = _phoneNumber;
    return map;
  }

}