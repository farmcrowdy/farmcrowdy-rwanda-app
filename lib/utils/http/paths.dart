class Paths {

  //LIST OF API ENDPOINTS


  //Authentication
  static const String REGISTER_PHONE = 'register';
  static const String REGISTER_FINAL = 'password/set_zone';
  static const String LOGIN = 'login';
  static const String OTP_RESEND = 'otp/resend';
  static const String OTP_VERIFY = 'otp/verify';
  static const String RESET_PASSWORD = 'password/reset/request';
  static const String RESET_OTP_VERIFY = 'password/reset/otp/verify';
  static const String RESET_UPDATE = 'password/reset/update';
  static const String ZONE = 'zone/all';
  static const String ZONE_CITY = 'city/by_zone/';
  static const String CITY_DISTRICT = 'sector/by_city/';
  static const String CHANGE_PASSWORD = 'password/update';
  static const String UPDATE_PROFILE = 'user/profile/update';
  static const String UPDATE_DP = 'user/profile/update';
  static const String UPDATE_EMAIL = 'user/profile/update/phone_email';
  static const String UPDATE_ZONE = 'user/profile/update/zone_address';
  static const String UPDATE_NAME = 'user/profile/update/name';


  //Products
  static const String CATEGORIES = 'category/all';
  static const String PACKAGES = 'package/type/';
  static const String PACKAGES_BY_CATEGORY_AND_DISTRICT = 'package/search/params';
  static const String SINGLE_PACKAGE = 'package/';
  static const String ADD_CART = 'order/create';
  static const String ADD_SAVED_PACKAGE = 'package/save/package';
  static const String DELETE_CART = 'order/delete/';
  static const String EDIT_CART = 'order/edit/';
  static const String CART_ALL = 'basket/all';
  static const String SAVED_PACKAGES = 'package/saved/userpackage';
  static const String CART_SUMMARY = 'basket/summary';
  static const String ORDER_ONGOING = 'transaction/user/ongoing';
  static const String ORDER_HISTORY = 'transaction/user/history';
  static const String SINGLE_ORDER = 'transaction/';
  static const String SEARCH = 'search/name/';
  static const String MERCHANT = 'merchant/all';
  static const String MERCHANT_BY_DISTRICT = 'merchant/location/';
  static const String MERCHANT_PACKAGE = 'merchant/';
  static const String PRODUCT_BY_CATEGORY = 'package/type/';
  static const String MERCHANT_SEARCH = 'merchant/search/';
  static const String MERCHANT_SEARCH_DISTRICT = 'merchant/location/';
  static const String PAYMENT = 'payment';
}
