/// phone_number : "phoneNo.toString()"
/// password : "oldPasswordController.text"
/// new_password : "newPasswordController.text"

class ChangePassword {
  String _phoneNumber;
  String _password;
  String _newPassword;

  String get phoneNumber => _phoneNumber;
  String get password => _password;
  String get newPassword => _newPassword;

  ChangePassword({
      String phoneNumber, 
      String password, 
      String newPassword}){
    _phoneNumber = phoneNumber;
    _password = password;
    _newPassword = newPassword;
}

  ChangePassword.fromJson(dynamic json) {
    _phoneNumber = json["phone_number"];
    _password = json["password"];
    _newPassword = json["new_password"];
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map["phone_number"] = _phoneNumber;
    map["password"] = _password;
    map["new_password"] = _newPassword;
    return map;
  }

}