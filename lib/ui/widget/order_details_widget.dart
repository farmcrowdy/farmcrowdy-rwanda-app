import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:rwandashop/core/model/product/order_details_model.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:intl/intl.dart';

// This is the widget for order details
class OrderDetailsWidget extends StatelessWidget {
  final OrdersDetails orderDetailsModel;
  final Function onTap;

  OrderDetailsWidget({this.orderDetailsModel, this.onTap});
  final oCcy = new NumberFormat("#,##0.00", "en_US");
  @override
  Widget build(BuildContext context) {
    return Container(
        padding: EdgeInsets.all(10.w),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Row(children: [
              // Container(
              //   height: 70.w,
              //   width: 70.w,
              //   child: Image.asset('assets/images/cide.png'),
              // ),
              Container(
                height: 70.w,
                width: 70.w,
                decoration:
                BoxDecoration(
                  image: DecorationImage(
                      image: CachedNetworkImageProvider(
                          orderDetailsModel.image),
                      fit: BoxFit
                          .fill),
                ),
              ),
              // Container(
              // height: 70.w,
              // width: 70.w,
              //   decoration:
              //   BoxDecoration(
              //     image: DecorationImage(
              //         image: CachedNetworkImageProvider(
              //             orders.image),
              //         fit: BoxFit
              //             .fill),
              //   ),
              // ),
              SizedBox(
                height: 10.h,
              ),
              Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Text(
                      orderDetailsModel.name,
                      style: TextStyle(
                          fontSize: 16.ssp, fontWeight: FontWeight.w600),
                    ),
                    SizedBox(
                      height: 5.h,
                    ),
                    Text(
                      orderDetailsModel.quantity.toString(),
                      style:
                          TextStyle(fontSize: 14.ssp, color: Color(0xff7C7C7C)),
                    ),
                  ]),
            ]),
            Column(
              children: [
                InkWell(
                    onTap: () {
                      if (onTap != null) {
                        onTap();
                      }
                    },
                    child: Container(
                      height: 15.w,
                      width: 15.w,
                      child: Image.asset(''),
                    )),
                SizedBox(
                  height: 10.h,
                ),
                Text(
                  'RF ${oCcy.format((orderDetailsModel.amount))}',
                  style:
                      TextStyle(fontSize: 16.ssp, fontWeight: FontWeight.w600),
                ),
              ],
            )
          ],
        ));
  }
}
