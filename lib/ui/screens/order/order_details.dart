import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider_architecture/provider_architecture.dart';
import 'package:rwandashop/core/model/product/order_details_model.dart'
    as details;
import 'package:rwandashop/core/services/analytics_service.dart';
import 'package:rwandashop/ui/screens/order/order_view_model.dart';
import 'package:rwandashop/ui/widget/order_details_widget.dart';
import 'package:rwandashop/utils/app_localizations.dart';
import 'package:rwandashop/utils/colors.dart';
import 'package:intl/intl.dart';
import 'package:rwandashop/core/model/product/orders.dart';
import 'package:rwandashop/utils/locator.dart';
import 'package:rwandashop/utils/screensize.dart';
import 'package:shimmer/shimmer.dart';

class OrderDetails extends StatefulWidget {
  final Data orderDetails;

  const OrderDetails({Key key, this.orderDetails});
  @override
  _OrderDetails createState() => _OrderDetails();
}

class _OrderDetails extends State<OrderDetails> {
  final oCcy = new NumberFormat("#,##0.00", "en_US");
  final Analytics _analytics = locator<Analytics>();
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    AppLocalizations tr = AppLocalizations.of(context);
    return ViewModelProvider<OrderViewModel>.withConsumer(
        onModelReady: (model) {
          _analytics.setCurrentScreen(
              'Order details', 'This gives details of an order');
        },
        viewModelBuilder: () => OrderViewModel(),
        builder: (context, model, child) {
          return Scaffold(
            body: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Container(
                  padding: EdgeInsets.only(top: 60),
                  child: Row(
                    children: <Widget>[
                      GestureDetector(
                        onTap: () {
                          Navigator.pop(context);
                        },
                        child: Container(
                          height: 60,
                          width: 60,
                          //margin: EdgeInsets.only(top: 20),
                          // padding: EdgeInsets.only(top: 20,),
                          child: Icon(Icons.arrow_back_ios),
                        ),
                      ),
                      Text(
                        tr.text('orderDetails.orderId') + " "+
                            widget.orderDetails.id.toString(),
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 20.0,
                        ),
                      ),
                    ],
                  ),
                ),
                Expanded(
                  child: FutureBuilder<List<details.OrdersDetails>>(
                      future: model
                          .getOrderDetails(widget.orderDetails.id.toString()),
                      builder: (context, snapshot) {
                        if (!snapshot.hasData) {
                          return Container(
                            padding: EdgeInsets.all(20.0),
                            child: Shimmer.fromColors(
                                direction: ShimmerDirection.ltr,
                                period: Duration(seconds: 2),
                                child: ListView(
                                  children: [0, 1, 2, 3, 4, 5]
                                      .map((_) => Padding(
                                            padding: const EdgeInsets.all(8.0),
                                            child: Row(
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.start,
                                              children: [
                                                Container(
                                                  width: Responsive.width(
                                                          context) -
                                                      20,
                                                  height: 150,
                                                  color: Colors.white,
                                                ),
                                                Padding(
                                                  padding: const EdgeInsets
                                                          .symmetric(
                                                      horizontal: 8.0),
                                                ),
                                                Expanded(
                                                  child: Column(
                                                    crossAxisAlignment:
                                                        CrossAxisAlignment
                                                            .start,
                                                    children: [
                                                      Container(
                                                        width: double.infinity,
                                                        height: 8.0,
                                                        color: Colors.white,
                                                      ),
                                                      Padding(
                                                        padding:
                                                            const EdgeInsets
                                                                    .symmetric(
                                                                vertical: 2.0),
                                                      ),
                                                      Container(
                                                        width: double.infinity,
                                                        height: 8.0,
                                                        color: Colors.white,
                                                      ),
                                                      Padding(
                                                        padding:
                                                            const EdgeInsets
                                                                    .symmetric(
                                                                vertical: 2.0),
                                                      ),
                                                      Container(
                                                        width: 40.0,
                                                        height: 8.0,
                                                        color: Colors.white,
                                                      ),
                                                    ],
                                                  ),
                                                )
                                              ],
                                            ),
                                          ))
                                      .toList(),
                                ),
                                baseColor: AppColors.grey,
                                highlightColor: Colors.white),
                          );
                        } else if (snapshot.data.isNotEmpty) {
                          return ListView(
                              // scrollDirection: Axis.horizontal,
                              shrinkWrap: true,
                              children: snapshot.data
                                  .map((feed) => OrderDetailsWidget(
                                      orderDetailsModel: feed))
                                  .toList());
                        } else if (snapshot.hasError) {
                          return Center(
                              child: Column(
                            children: <Widget>[
                              SizedBox(
                                height: 100,
                              ),
                              Text(
                                tr.text('orderDetails.error'),
                                style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  fontSize: 20,
                                ),
                              ),
                              SizedBox(
                                height: 10,
                              ),
                              Text(tr.text('orderDetails.notConnected')),
                              SizedBox(
                                height: 100,
                              ),
                            ],
                          ));
                        } else {
                          return Center(
                              child: Column(
                            children: <Widget>[
                              SizedBox(
                                height: 100,
                              ),
                              Text(
                                tr.text('orderDetails.noOrder'),
                                style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  fontSize: 20,
                                ),
                              ),
                              SizedBox(
                                height: 100,
                              ),
                            ],
                          ));
                        }
                      }),
                ),
                Container(
                    padding: EdgeInsets.all(20),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[
                        Text(
                          tr.text('orderDetails.dropLocation'),
                          style: TextStyle(color: Colors.black, fontSize: 16),
                        ),
                        SizedBox(
                          height: 15,
                        ),
                        // Text(
                        //   widget.orderDetails.location,
                        //   style: TextStyle(
                        //       color: Colors.black,
                        //       fontWeight: FontWeight.bold,
                        //       fontSize: 16),
                        // ),
                        SizedBox(
                          height: 15,
                        ),
                        Text(
                          tr.text('orderDetails.total'),
                          style: TextStyle(fontSize: 16),
                        ),
                        SizedBox(
                          height: 15,
                        ),
                        Text(
                          widget.orderDetails.amount != null
                              ? 'RF ' + oCcy.format(widget.orderDetails.amount)
                              : '0',
                          style: TextStyle(
                              color: Colors.black,
                              fontWeight: FontWeight.bold,
                              fontSize: 16),
                        ),
                        SizedBox(
                          height: 15,
                        ),
                        Text(
                          tr.text('orderDetails.createdOn'),
                          style: TextStyle(fontSize: 16),
                        ),
                        SizedBox(
                          height: 15,
                        ),
                        Text(
                          widget.orderDetails.createdAt,
                          style: TextStyle(
                              color: Colors.black,
                              fontWeight: FontWeight.bold,
                              fontSize: 16),
                        ),
                      ],
                    )),
                SizedBox(
                  height: 15,
                ),
                Container(
                    margin: EdgeInsets.only(left: 10),
                    height: 25,
                    child: FlatButton(
                        textColor: AppColors.grey,
                        child: Text(
                          widget.orderDetails.status,
                          style: TextStyle(color: Color(0xff7D650E)),
                        ),
                        color: Color(0xFFB08B00).withOpacity(.25),
                        shape: RoundedRectangleBorder(
                          borderRadius: new BorderRadius.circular(18.0),
                          //side: BorderSide(color: Colors.red)
                        ),
                        onPressed: () {
                          //   Navigator.of(context).pushNamed('/signup');
                        })),
                SizedBox(
                  height: 20,
                )
              ],
            ),
          );
        });
  }
}
