import 'package:badges/badges.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider_architecture/provider_architecture.dart';
import 'package:rwandashop/core/model/product/catergories.dart' as category;
import 'package:rwandashop/core/model/product/search.dart';
import 'package:rwandashop/core/services/analytics_service.dart';
import 'package:rwandashop/core/services/auth_service.dart';
import 'package:rwandashop/core/services/products_service.dart';
import 'package:rwandashop/ui/screens/packages/packages_view_model.dart';
import 'package:rwandashop/ui/widget/package_widget.dart';
import 'package:rwandashop/utils/app_localizations.dart';
import 'package:rwandashop/utils/colors.dart';
import 'package:rwandashop/utils/locator.dart';
import 'package:rwandashop/utils/router/navigationService.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:rwandashop/core/model/product/products.dart';
import 'package:rwandashop/utils/screensize.dart';
import 'package:get/get.dart';
import 'package:shimmer/shimmer.dart';

final GlobalKey<ScaffoldState> scaffoldKey = GlobalKey<ScaffoldState>();

class PackagePage extends StatefulWidget {
  final category.Categories categoriesDetails;

  const PackagePage({Key key, this.categoriesDetails}) : super(key: key);
  @override
  ProductMoreState createState() => new ProductMoreState();
}

class ProductMoreState extends State<PackagePage> {
  TextEditingController numberController = TextEditingController();
  final Authentication _authentication = locator<Authentication>();

  final Product _product = locator<Product>();
  final NavigationService _navigationService = locator<NavigationService>();
  final Analytics _analytics = locator<Analytics>();
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    AppLocalizations tr = AppLocalizations.of(context);
    final double itemHeight = Responsive.height(context) / 2.5;
    final double itemWidth = Responsive.width(context) / 2.3;

    return ViewModelProvider<PackageViewModel>.withConsumer(
        onModelReady: (model) {
          _analytics.setCurrentScreen(
              'Items page', 'User see all the items of a particular category');
        },
        viewModelBuilder: () => PackageViewModel(),
        builder: (context, model, child) {
          return Scaffold(
            key: scaffoldKey,
            body: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  SizedBox(
                    height: 80,
                  ),
                  Container(
                    padding: EdgeInsets.only(left: 20, right: 20),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        InkWell(
                          onTap: () {
                            _navigationService.pop();
                          },
                          child: Icon(
                            Icons.arrow_back_ios,
                            color: Colors.black,
                          ),
                        ),
                        Container(
                          child: Text(
                            widget.categoriesDetails.name,
                            style: TextStyle(
                                fontSize: 18.ssp, fontWeight: FontWeight.bold),
                          ),
                        ),
                        Row(
                          children: [
                            _authentication.currentUser.isBlank
                                ? Badge(
                                    borderRadius: BorderRadius.circular(5.r),
                                    badgeColor: AppColors.themeGreen,
                                    badgeContent: Text(
                                      _product.cartSummary.data.basketSize
                                          .toString(),
                                      style: TextStyle(
                                          color: Colors.white,
                                          fontWeight: FontWeight.bold),
                                    ),
                                    child: Container(
                                      height: 20.h,
                                      width: 20.w,
                                      child:
                                          Image.asset('assets/images/cart.png'),
                                    ),
                                  )
                                : Container(),
                            SizedBox(
                              width: 20.w,
                            ),

                            Text('')
                            // InkWell(
                            //   onTap: () {
                            //     scaffoldKey.currentState.showBottomSheet(
                            //         (context) => FeaturesWidget(
                            //             featureValue: featureValue));
                            //   },
                            //   child: Container(
                            //     height: 20.h,
                            //     width: 20.w,
                            //     child: Image.asset('assets/images/filter.png'),
                            //   ),
                            // ),
                          ],
                        )
                      ],
                    ),
                  ),
                  SizedBox(height: 20.h),
                  Container(
                    padding: EdgeInsets.only(left: 20.w, right: 20.w),
                    child: TextField(
                      decoration: InputDecoration(
                        focusedBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(15.0.r),
                            borderSide: BorderSide(
                                color: Color(0xffF2F3F2),
                                width: 0.0,
                                style: BorderStyle.none)),
                        enabledBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(15.0.r),
                            borderSide: BorderSide(
                                color: Color(0xffF2F3F2),
                                width: 0.0,
                                style: BorderStyle.none)),
                        errorBorder: InputBorder.none,
                        disabledBorder: InputBorder.none,
                        fillColor: Color(0xffF2F3F2),
                        filled: true,
                        border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(15.0.r),
                            borderSide: BorderSide(
                                color: Color(0xffF2F3F2),
                                width: 0.0,
                                style: BorderStyle.none)),
                        hintText: tr.text('package.searchStore'),
                        hintStyle: TextStyle(
                            color: Color(0xff7C7C7C),
                            fontSize: 14.ssp,
                            fontWeight: FontWeight.w500),
                        contentPadding:
                            EdgeInsets.fromLTRB(10.0, 10.0, 10.0, 10.0),
                        prefixIcon: Icon(
                          Icons.search,
                          color: Colors.black,
                        ),
                      ),
                      textInputAction: TextInputAction.search,
                      onSubmitted: (value) {
                        // model.searchUser(context, value);
                        // model.searchGroup(context, value);
                      },
                      onChanged: (value) {
                        model.initiateSearch(value, model);
                        //if (value.length > 1 && value.isNotEmpty)
                        //   model.searchUser(context, value);
                        // model.searchGroup(context, value);
                      },
                    ),
                  ),
                  SizedBox(
                    height: 30.h,
                  ),
                  model.searchName == null
                      ? Expanded(
                          child: FutureBuilder<List<Data>>(
                              future: model.getItem(
                                  widget.categoriesDetails.id.toString()),
                              builder: (context, snapshot) {
                                if (!snapshot.hasData) {
                                  return Container(
                                      padding: EdgeInsets.all(20.0),
                                      child: Center(
                                        child: Shimmer.fromColors(
                                            direction: ShimmerDirection.ltr,
                                            period: Duration(seconds: 2),
                                            child: GridView.count(
                                              padding: EdgeInsets.all(20.w),
                                              childAspectRatio: (200.w / 330.h),
                                              crossAxisCount: 2,
                                              mainAxisSpacing: 12.0.w,
                                              crossAxisSpacing: 12.0.w,
                                              children: [0, 1, 2, 3, 4, 5, 6]
                                                  .map((_) => Padding(
                                                      padding:
                                                          const EdgeInsets.only(
                                                              bottom: 8.0),
                                                      child: PackageWidget(
                                                        model: model,
                                                      )))
                                                  .toList(),
                                            ),
                                            baseColor: AppColors.grey,
                                            highlightColor: Colors.white),
                                      ));
                                } else if (snapshot.data.isNotEmpty) {
                                  return GridView.count(
                                    children: snapshot.data
                                        .map((feed) => PackageWidget(
                                              onTap: () {
                                                _analytics.addToCart(
                                                    feed.price,
                                                    feed.id,
                                                    feed.name,
                                                    feed.name,
                                                    1);
                                              },
                                              packageModel: feed,
                                              model: model,
                                            ))
                                        .toList(),
                                    padding: EdgeInsets.all(20.w),
                                    childAspectRatio: (200.w / 330.h),
                                    crossAxisCount: 2,
                                    mainAxisSpacing: 10.0,
                                    crossAxisSpacing: 10.0,
                                  );
                                } else if (snapshot.hasError) {
                                  return Column(
                                    children: <Widget>[
                                      SizedBox(
                                        height: 10.h,
                                      ),
                                      Text(
                                        tr.text('package.error'),
                                        style: TextStyle(
                                          fontWeight: FontWeight.bold,
                                          fontSize: 20.ssp,
                                        ),
                                      ),
                                      SizedBox(
                                        height: 10,
                                      ),
                                      Text(
                                        tr.text('package.notConnected'),
                                      ),
                                      SizedBox(
                                        height: 100.h,
                                      ),
                                    ],
                                  );
                                } else {
                                  return Text(
                                    tr.text('package.databaseEmpty'),
                                    style: TextStyle(
                                      fontWeight: FontWeight.bold,
                                      fontSize: 20.ssp,
                                    ),
                                  );
                                }
                              }),
                        )
                      : Expanded(
                          child: FutureBuilder<List<Data>>(
                              future: model.getSearchedPackages(
                                  Search(searchInput: model.searchName)),
                              builder: (context, snapshot) {
                                if (!snapshot.hasData) {
                                  return Container(
                                      padding: EdgeInsets.all(20.0),
                                      child: Center(
                                        child: Shimmer.fromColors(
                                            direction: ShimmerDirection.ltr,
                                            period: Duration(seconds: 2),
                                            child: GridView.count(
                                              padding: EdgeInsets.all(20.w),
                                              childAspectRatio: (200.w / 330.h),
                                              crossAxisCount: 2,
                                              mainAxisSpacing: 12.0.w,
                                              crossAxisSpacing: 12.0.w,
                                              children: [0, 1, 2, 3, 4, 5, 6]
                                                  .map((_) => Padding(
                                                      padding:
                                                          const EdgeInsets.only(
                                                              bottom: 8.0),
                                                      child: PackageWidget(
                                                        model: model,
                                                      )))
                                                  .toList(),
                                            ),
                                            baseColor: AppColors.grey,
                                            highlightColor: Colors.white),
                                      ));
                                } else if (snapshot.data.isNotEmpty) {
                                  return GridView.count(
                                    children: snapshot.data
                                        .map((feed) => PackageWidget(
                                              packageModel: feed,
                                              model: model,
                                            ))
                                        .toList(),
                                    padding: EdgeInsets.all(20.w),
                                    childAspectRatio: (200.w / 330.h),
                                    crossAxisCount: 2,
                                    mainAxisSpacing: 10.0,
                                    crossAxisSpacing: 10.0,
                                  );
                                } else if (snapshot.hasError) {
                                  return Column(
                                    children: <Widget>[
                                      SizedBox(
                                        height: 10.h,
                                      ),
                                      Text(
                                        tr.text('package.error'),
                                        style: TextStyle(
                                          fontWeight: FontWeight.bold,
                                          fontSize: 20.ssp,
                                        ),
                                      ),
                                      SizedBox(
                                        height: 10,
                                      ),
                                      Text(
                                        tr.text('package.notConnected'),
                                      ),
                                      SizedBox(
                                        height: 100.h,
                                      ),
                                    ],
                                  );
                                } else {
                                  return Text(
                                    tr.text('package.notFound'),
                                    style: TextStyle(
                                      fontWeight: FontWeight.bold,
                                      fontSize: 20.ssp,
                                    ),
                                  );
                                }
                              }),
                        ),
                ]),
          );
        });
  }
}
