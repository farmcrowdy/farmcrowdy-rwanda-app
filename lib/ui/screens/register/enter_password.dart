import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:provider_architecture/provider_architecture.dart';
import 'package:rwandashop/core/model/authentication/final_reg.dart';
import 'package:rwandashop/core/services/analytics_service.dart';
import 'package:rwandashop/core/services/auth_service.dart';
import 'package:rwandashop/ui/screens/register/registerViewModel.dart';
import 'package:rwandashop/ui/widget/generalButton.dart';
import 'package:rwandashop/utils/app_localizations.dart';
import 'package:rwandashop/utils/colors.dart';
import 'package:rwandashop/utils/helpers.dart';
import 'package:rwandashop/utils/locator.dart';
import 'package:rwandashop/utils/router/navigationService.dart';
import 'package:rwandashop/utils/screensize.dart';

class EnterPassword extends StatefulWidget {
  @override
  EnterPasswordState createState() => new EnterPasswordState();
}

class EnterPasswordState extends State<EnterPassword> {
  TextEditingController passwordController1 = TextEditingController();
  TextEditingController passwordController2 = TextEditingController();
  final Authentication _authentication = locator<Authentication>();
  final NavigationService _navigationService = locator<NavigationService>();
  final Analytics _analytics = locator<Analytics>();
  bool passwordVisible1,passwordVisible2;
  // bool insertedPasswordIsSimilarimilar = false;
  String gender = 'Select City';
  List zone = List();
  int zoneId;
  List city = List();
  List district = List();
  int districtId = 0;
  int cityId;

  @override
  void initState() {
    super.initState();
    passwordVisible1 = true;
    passwordVisible2 = true;
  }

  getZone(RegisterViewModel model) async {
    print('ddd');
    var zones = await model.getZone();
    setState(() {
      zone = zones;
      print('ddd:::::::::::::::::::::');
      print(zone);
    });
  }

  getCity(RegisterViewModel model, String id) async {
    print('city: $id');
    var cities = await model.getCity(id);
    setState(() {
      city = cities;
      print('ddd:::::::::::::::::::::');
      print(city);
    });
  }

  getDistrict(RegisterViewModel model, String id) async {
    print('district: $id');
    var districts = await model.getDistrict('7');
    setState(() {
      district = districts;
      print('ddd:::::::::::::::::::::');
      print(district);
    });
  }

  @override
  Widget build(BuildContext context) {
    AppLocalizations tr = AppLocalizations.of(context);
    return ViewModelProvider<RegisterViewModel>.withConsumer(
        onModelReady: (model) {
          _analytics.setCurrentScreen(
              'Set Password page', 'Set password and zone page');
        },
        viewModelBuilder: () => RegisterViewModel(),
        builder: (context, model, child) {
          if (zone.isEmpty) {
            getZone(model);
          }

          return Scaffold(
            body: SingleChildScrollView(
              child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Container(
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Stack(
                            children: [
                              Container(
                                width: Responsive.width(context),
                                height: Responsive.height(context) * .35,
                                child: Image.asset(
                                  'assets/images/passwordBanner.png',
                                  fit: BoxFit.cover,
                                ),
                              ),
                              InkWell(
                                onTap: () {
                                  _navigationService.pop();
                                },
                                child: Padding(
                                  padding: EdgeInsets.fromLTRB(20, 50, 10, 10),
                                    child: Icon(Icons.arrow_back_ios,color: AppColors.white,)),
                              ),
                            ],
                          ),
                        ],
                      ),
                    ),
                    SizedBox(
                      height: 40.h,
                    ),
                    Align(
                      alignment: Alignment.centerLeft,
                      child: Container(
                        margin: EdgeInsets.only(left: 20),
                        child: Text(
                          tr.text('enterPassword.enterPasswordLocation'),
                          style: TextStyle(
                              fontSize: 20.ssp, fontWeight: FontWeight.bold),
                        ),
                      ),
                    ),
                    SizedBox(
                      height: 10.h,
                    ),
                    Align(
                      alignment: Alignment.centerLeft,
                      child: Container(
                        margin: EdgeInsets.only(left: 20),
                        child: Text(
                          tr.text('enterPassword.enterPasswordLocationDesc'),
                          style: TextStyle(
                              fontSize: 12.ssp,
                              color: Color(0xff7C7C7C),
                              fontWeight: FontWeight.w400),
                        ),
                      ),
                    ),
                    Container(
                      child: Text(
                        tr.text('enterPassword.signUp'),
                        style: TextStyle(
                            fontSize: 12.ssp,
                            color: Color(0xff7C7C7C),
                            fontWeight: FontWeight.w400),
                      ),
                    ),
                    // SizedBox(height: 20.h),
                    // Row(
                    //     mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    //     children: [
                    //       Container(
                    //         padding: EdgeInsets.only(left: 20, right: 20),
                    //         child: Text(
                    //           tr.text('enterPassword.yourZone'),
                    //           style: TextStyle(
                    //               fontSize: 14.ssp,
                    //               color: Color(0xff7C7C7C),
                    //               fontWeight: FontWeight.w400),
                    //         ),
                    //       ),
                    //       Text('')
                    //     ]),
                    // SizedBox(height: 10.h),
                    // // Row(
                    // //     mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    // //     children: [
                    // //       Container(
                    // //           padding: EdgeInsets.only(left: 20, right: 20),
                    // //           child: Text(
                    // //             'The Northern Province',
                    // //             style: TextStyle(fontWeight: FontWeight.bold),
                    // //           ))
                    // //     ]),
                    // Container(
                    //     padding: EdgeInsets.only(left: 20.w, right: 20.w),
                    //     child: Center(
                    //       child: new DropdownButtonFormField(
                    //         decoration: InputDecoration.collapsed(hintText:  tr.text('enterPassword.yourZone')),
                    //         isExpanded: true,
                    //         validator: (value) => value == null ? '' : null,
                    //         items: zone.map((item) {
                    //           return new DropdownMenuItem(
                    //             child: new Text(item['name']),
                    //             value: item,
                    //           );
                    //         }).toList(),
                    //         onChanged: (newVal) {
                    //           setState(() {
                    //             zoneId = newVal['id'];
                    //             print(zoneId);
                    //             city.clear();
                    //             getCity(model, zoneId.toString());
                    //           });
                    //         },
                    //         //value: unitPrice,
                    //       ),
                    //     )),
                    // SizedBox(height: 20.h),
                    // Row(
                    //     mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    //     children: [
                    //       Container(
                    //         padding: EdgeInsets.only(left: 20, right: 20),
                    //         child: Text(
                    //           tr.text('enterPassword.yourCity'),
                    //           style: TextStyle(
                    //               fontSize: 14.ssp,
                    //               color: Color(0xff7C7C7C),
                    //               fontWeight: FontWeight.w400),
                    //         ),
                    //       ),
                    //       Text('')
                    //     ]),
                    // SizedBox(height: 10.h),
                    // // Row(
                    // //     mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    // //     children: [
                    // //       Container(
                    // //         padding: EdgeInsets.only(left: 20, right: 20),
                    // //         child: Text(
                    // //           tr.text('enterPassword.district'),
                    // //           style: TextStyle(
                    // //             fontWeight: FontWeight.bold,
                    // //           ),
                    // //         ),
                    // //       ),
                    // //       Text('')
                    // //     ]),
                    // Container(
                    //     padding: EdgeInsets.only(left: 20.w, right: 20.w),
                    //     child: Center(
                    //       child: new DropdownButtonFormField(
                    //         decoration: InputDecoration.collapsed(hintText: tr.text('enterPassword.yourCity')),
                    //         isExpanded: true,
                    //
                    //         validator: (value) => value == null ? '' : null,
                    //         items: city.map((item) {
                    //           return new DropdownMenuItem(
                    //             child: new Text(item['name']),
                    //             value: item,
                    //           );
                    //         }).toList(),
                    //         onChanged: (newVal) {
                    //           setState(() {
                    //             cityId = newVal['id'];
                    //             print(cityId);
                    //             district.clear();
                    //             getDistrict(model, cityId.toString());
                    //           });
                    //         },
                    //         //value: unitPrice,
                    //       ),
                    //     )),
                    //
                    // SizedBox(height: 20.h),
                    // Row(
                    //     mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    //     children: [
                    //       Container(
                    //         padding: EdgeInsets.only(left: 20, right: 20),
                    //         child: Text(
                    //           tr.text('enterPassword.district'),
                    //           style: TextStyle(
                    //               fontSize: 16.ssp,
                    //               color: Color(0xff7C7C7C),
                    //               fontWeight: FontWeight.w400),
                    //         ),
                    //       ),
                    //       Text('')
                    //     ]),
                    // SizedBox(height: 10.h),
                    // Container(
                    //     padding: EdgeInsets.only(left: 20.w, right: 20.w),
                    //     child: Center(
                    //       child: new DropdownButtonFormField(
                    //         decoration: InputDecoration.collapsed(
                    //             hintText:
                    //                 tr.text('enterPassword.selectSector')),
                    //         isExpanded: true,
                    //
                    //         validator: (value) => value == null ? '' : null,
                    //         items: district.map((item) {
                    //           return new DropdownMenuItem(
                    //             child: new Text(item['name']),
                    //             value: item,
                    //           );
                    //         }).toList(),
                    //         onChanged: (newVal) {
                    //           setState(() {
                    //             districtId = newVal['id'];
                    //             print(districtId);
                    //           });
                    //         },
                    //         //value: unitPrice,
                    //       ),
                    //     )),
                    SizedBox(
                      height: 20.h,
                    ),
                   Align(
                     alignment: Alignment.centerLeft,
                     child:  Container(
                       padding: EdgeInsets.only(left: 20, right: 20),
                       child: Text(
                         tr.text('enterPassword.password'),
                         style: TextStyle(
                             fontSize: 14.ssp,
                             color: Color(0xff7C7C7C),
                             fontWeight: FontWeight.w400
                         ),
                       ),
                     ),
                   ),
                    Container(
                      padding: EdgeInsets.only(left: 20, right: 20),
                      child: TextFormField(
                        controller: passwordController1,
                        // maxLength: 11,
                        decoration: InputDecoration(
                          suffixIcon: IconButton(
                            icon: Icon(
                              // Based on passwordVisible state choose the icon
                              passwordVisible1
                                  ? Icons.visibility
                                  : Icons.visibility_off,
                              color: Colors.black,
                            ),
                            onPressed: () {
                              // Update the state i.e. toogle the state of passwordVisible variable
                              setState(() {
                                passwordVisible1 = !passwordVisible1;
                              });
                            },
                          ),
                          // errorText: insertedPasswordIsSimilar
                          //     ? null
                          //     : 'Password not similar',
                          hintText: '....',
                          hintStyle: TextStyle(
                            color: Colors.black45,
                          ),
                          labelStyle: TextStyle(color: Colors.blue),
                          // border: new OutlineInputBorder(
                          //   borderRadius: new BorderRadius.circular(5.0),
                          //   borderSide: new BorderSide(),
                          // ),
                        ),
                        obscureText: passwordVisible1,
                        keyboardType: TextInputType.visiblePassword,
                        style: TextStyle(color: Colors.black),
                        cursorColor: Colors.black,
                      ),
                    ),
                    SizedBox(
                      height: 20.h,
                    ),
                    Align(
                      alignment: Alignment.centerLeft,
                      child:  Container(
                        padding: EdgeInsets.only(left: 20, right: 20),
                        child: Text(
                          tr.text('registration.confirmPassword'),
                          style: TextStyle(
                              fontSize: 14.ssp,
                              color: Color(0xff7C7C7C),
                              fontWeight: FontWeight.w400),
                        ),
                      ),
                    ),
                    Container(
                      padding: EdgeInsets.only(left: 20, right: 20),
                      child: TextFormField(
                        controller: passwordController2,
                        // maxLength: 11,
                        decoration: InputDecoration(
                          suffixIcon: IconButton(
                            icon: Icon(
                              // Based on passwordVisible state choose the icon
                              passwordVisible2
                                  ? Icons.visibility
                                  : Icons.visibility_off,
                              color: Colors.black,
                            ),
                            onPressed: () {
                              // Update the state i.e. toogle the state of passwordVisible variable
                              setState(() {
                                passwordVisible2 = !passwordVisible2;
                              });
                            },
                          ),
                          // errorText: insertedPasswordIsSimilar
                          //     ? null
                          //     : 'Password not similar',
                          hintText: '....',
                          hintStyle: TextStyle(
                            color: Colors.black45,
                          ),
                          labelStyle: TextStyle(color: Colors.blue),
                          // border: new OutlineInputBorder(
                          //   borderRadius: new BorderRadius.circular(5.0),
                          //   borderSide: new BorderSide(),
                          // ),
                        ),
                        obscureText: passwordVisible2,
                        keyboardType: TextInputType.visiblePassword,
                        style: TextStyle(color: Colors.black),
                        cursorColor: Colors.black,
                      ),
                    ),
                    SizedBox(
                      height: 20.h,
                    ),
                    Container(
                        padding: EdgeInsets.all(20.w),
                        child: GeneralButton(
                          buttonText: tr.text('general.next'),
                          onPressed: () async{
                            if (passwordController1.text ==
                                passwordController2.text) {
                              // insertedPasswordIsSimilar = true;
                              int zoneId = await model.getZoneId();
                              int cityId = await model.getCityId();
                              model.finalReg(FinalReg(
                                  phoneNumber: _authentication.phoneNumber,
                                  password: passwordController1.text,
                                  zoneId: zoneId,
                                  cityId: cityId,
                                  sectorId: 1,// districtId
                              ));
                            } else {
                              showToast('Password not similar');
                              // setState(() {
                              //   insertedPasswordIsSimilar = false;
                              // });
                            }
                          },
                        )),
                  ]),
            ),
          );
        });
  }
}
