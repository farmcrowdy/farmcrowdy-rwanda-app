import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:intl_phone_number_input/intl_phone_number_input.dart';
import 'package:provider_architecture/provider_architecture.dart';
import 'package:rwandashop/core/model/authentication/register_phone_no.dart';
import 'package:rwandashop/ui/screens/register/registerViewModel.dart';
import 'package:rwandashop/ui/widget/generalButton.dart';
import 'package:rwandashop/utils/app_localizations.dart';
import 'package:rwandashop/utils/colors.dart';
import 'package:rwandashop/utils/helpers.dart';
import 'package:rwandashop/utils/locator.dart';
import 'package:rwandashop/utils/router/navigationService.dart';
import 'package:rwandashop/utils/screensize.dart';

class Register extends StatefulWidget {
  @override
  RegisterState createState() => new RegisterState();
}

class RegisterState extends State<Register> {
  TextEditingController numberController = TextEditingController();

  final NavigationService _navigationService = locator<NavigationService>();
  bool passwordVisible;
  List zone = List();
  int zoneId;
  List city = List();
  List district = List();
  int districtId;
  int cityId;
  String phoneNumber;
  String phoneIsoCode;
  bool visible = false;

  String initialCountry = 'RW';
  PhoneNumber number = PhoneNumber(isoCode: 'RW');

  void getPhoneNumber(String phoneNumber) async {
    PhoneNumber number =
    await PhoneNumber.getRegionInfoFromPhoneNumber(phoneNumber, 'RW');

    setState(() {
      this.number = number;
    });
  }

  @override
  void initState() {
    super.initState();
    passwordVisible = true;
  }

  getZone(RegisterViewModel model) async {
    print('ddd');
    var zones = await model.getZone();
    setState(() {
      zone.clear();
      zone = zones;
      print('ddd:::::::::::::::::::::');
      print(zone);
    });
  }

  getCity(RegisterViewModel model, String id) async {
    print('city: $id');
    var cities = await model.getCity(id);
    setState(() {
      city.clear();
      city = cities;
      print('ddd:::::::::::::::::::::');
      print(city);
    });
  }

  getDistrict(RegisterViewModel model, String id) async {
    print('district: $id');
    var districts = await model.getDistrict('7');
    setState(() {
      district.clear();
      district = districts;
      print('ddd:::::::::::::::::::::');
      print(district);
    });
  }

  @override
  Widget build(BuildContext context) {
    AppLocalizations tr = AppLocalizations.of(context);
    return ViewModelProvider<RegisterViewModel>.withConsumer(
        viewModelBuilder: () => RegisterViewModel(),
        onModelReady: (model) {
          getZone(model);
        },
        builder: (context, model, child) {
          return Scaffold(
            body: ListView(children: <Widget>[
              Stack(
                children: [
                  Container(
                    height: Responsive.height(context) * .3,
                    width: Responsive.width(context),
                    child: Image.asset(
                      'assets/images/registrationBanner.png',
                      fit: BoxFit.cover,
                    ),
                  ),
                  InkWell(
                      onTap: () {
                        _navigationService.pop();
                      },
                      child: Container(
                        padding: EdgeInsets.all(20.w),
                        child: Icon(Icons.arrow_back_ios),
                      )),
                ],
              ),
              Container(
                padding: EdgeInsets.only(right: 20, left: 20,top: 20),
                child: Text(
                  tr.text('registration.letsGetStarted'),
                  style:
                      TextStyle(fontSize: 26.ssp, fontWeight: FontWeight.bold),
                ),
              ),
              SizedBox(
                height: 10.h,
              ),

              Container(
                padding: EdgeInsets.only(right: 20, left: 20),
                child: Text(
                  tr.text('registration.4digitCode'),
                  style:
                  TextStyle(fontSize: 16.ssp, fontWeight: FontWeight.w400,color: AppColors.deepGrey),
                ),
              ),
              SizedBox(
                height: 20.h,
              ),
              Container(
                padding: EdgeInsets.only(left: 20.w, right: 20.w),
                child: Text(
                  tr.text('register.mobileNumber'),
                  style: TextStyle(fontSize: 15.ssp, fontWeight: FontWeight.w500,color: AppColors.deepGrey,),
                ),
              ),
              Container(
                padding: EdgeInsets.only(left: 20.w, right: 20.w),
                child:  InternationalPhoneNumberInput(
                  onInputChanged: (PhoneNumber number) {
                    print(number.phoneNumber);
                  },
                  countries: ['RW'],
                  onInputValidated: (bool value) {
                    print(value);
                  },
                  selectorConfig: SelectorConfig(
                    selectorType: PhoneInputSelectorType.BOTTOM_SHEET,
                  ),
                  ignoreBlank: false,
                  autoValidateMode: AutovalidateMode.disabled,
                  textStyle: TextStyle(fontWeight: FontWeight.w700),
                  selectorTextStyle: TextStyle(color: Colors.black),
                  initialValue: number,
                  textFieldController: numberController,
                  formatInput: false,
                  keyboardType:
                  TextInputType.numberWithOptions(signed: true, decimal: true),
                  onSaved: (PhoneNumber number) {
                    print('On Saved: $number');
                  },
                ),

                // InternationalPhoneInput(
                //   decoration: InputDecoration.collapsed(hintText: '81000000004'),
                //   onPhoneNumberChange: onPhoneNumberChange,
                //   initialPhoneNumber: phoneNumber,
                //   initialSelection: phoneIsoCode,
                //   enabledCountries: ['+250'],
                //   showCountryCodes: true,
                //   showCountryFlags: true,
                // ),
//                 TextFormField(
//                   controller: numberController,
//                   // maxLength: 11,
//                   decoration: InputDecoration(
// //                    errorText: residentialValid
// //                        ? null
// //                        : 'Kindly update this field',
//
//                     prefixIcon: Image.asset('asset/images/rwanda_flag.png'),
//                     hintText: '08100000000',
//                     hintStyle: TextStyle(
//                       color: Colors.black45,
//                     ),
//                     labelStyle: TextStyle(color: Colors.blue),
//                     // border: new OutlineInputBorder(
//                     //   borderRadius: new BorderRadius.circular(5.0),
//                     //   borderSide: new BorderSide(),
//                     // ),
//                   ),
//                   keyboardType: TextInputType.phone,
//                   style: TextStyle(color: Colors.black),
//                   cursorColor: Colors.black,
//                 ),
              ),
              // Container(
              //   padding: EdgeInsets.only(left: 20.w, right: 20.w),
              //   child: Divider(thickness: 0.7,color: AppColors.lowGrey,),
              // ),
              SizedBox(height: 10.h),
              Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
                Container(
                  padding: EdgeInsets.only(left: 20, right: 20),
                  child: Text(
                    tr.text('enterPassword.yourZone'),
                    style: TextStyle(
                        fontSize: 14.ssp,
                        color:  AppColors.deepGrey,
                        fontWeight: FontWeight.w400),
                  ),
                ),
                Text('')
              ]),
              SizedBox(height: 10.h),
              Container(
                  padding: EdgeInsets.only(left: 20.w, right: 20.w),
                  child: Center(
                    child: new DropdownButtonFormField(
                      decoration: InputDecoration.collapsed(
                          hintText: tr.text('enterPassword.yourZone'),
                          hintStyle: TextStyle(fontWeight: FontWeight.w700)),
                      isExpanded: true,
                      validator: (value) => value == null ? '' : null,
                      items: zone.map((item) {
                        return new DropdownMenuItem(
                          child: new Text(
                            item['name'],
                            style: TextStyle(fontWeight: FontWeight.w700),
                          ),
                          value: item,
                        );
                      }).toList(),
                      onChanged: (newVal) {
                        setState(() {
                          zoneId = newVal['id'];
                          print(zoneId);
                          city.clear();
                          getCity(model, zoneId.toString());
                        });
                      },
                      //value: unitPrice,
                    ),
                  )),
              Padding(
                padding: EdgeInsets.fromLTRB(20, 20, 20, 10),
                child: Divider(
                  color: Colors.black87,
                ),
              ),
              Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
                Container(
                  padding: EdgeInsets.only(left: 20, right: 20),
                  child: Text(
                    tr.text('enterPassword.yourCity'),
                    style: TextStyle(
                        fontSize: 14.ssp,
                        color:  AppColors.deepGrey,
                        fontWeight: FontWeight.w400),
                  ),
                ),
                Text('')
              ]),
              SizedBox(height: 10.h),
              Container(
                  padding: EdgeInsets.only(left: 20.w, right: 20.w),
                  child: Center(
                    child: new DropdownButtonFormField(
                      decoration: InputDecoration.collapsed(
                          hintText: tr.text('enterPassword.yourCity'),
                          hintStyle: TextStyle(fontWeight: FontWeight.w700)),
                      isExpanded: true,
                      validator: (value) => value == null ? '' : null,
                      items: city.map((item) {
                        return new DropdownMenuItem(
                          child: new Text(item['name'],
                              style: TextStyle(fontWeight: FontWeight.w700)),
                          value: item,
                        );
                      }).toList(),
                      onChanged: (newVal) {
                        setState(() {
                          cityId = newVal['id'];
                          print(cityId);
                          district.clear();
                          getDistrict(model, cityId.toString());
                        });
                      },
                      //value: unitPrice,
                    ),
                  )),
              Padding(
                padding: EdgeInsets.fromLTRB(20, 20, 20, 10),
                child: Divider(
                  color: Colors.black87,
                ),
              ),

              Container(
                  padding: EdgeInsets.all(20.w),
                  child: GeneralButton(
                    buttonText:
                    tr.text('general.next'),
                    onPressed: () {
                      print('phoneNumbera: ${numberController.text}');
                      if(zoneId != null && cityId != null && numberController.text.isNotEmpty){
                        model.saveZoneAndCity(zoneId, cityId);
                        model.getPhone(numberController.text);
                        model.signUp(RegisterPhoneNo(
                            phoneNumber: numberController.text,
                            requestType: 'registerWithPhone'));
                      } else {
                        showToast('Kindly fill all fields');
                      }
                    },
                  )),
              // InkWell(
              //   onTap: (){
              //     model.getPhone(numberController.text);
              //     model.signUp(RegisterPhoneNo(
              //         phoneNumber: numberController.text,
              //         requestType: 'registerWithPhone'));
              //   },
              //   child: Container(
              //       padding: EdgeInsets.all(20.w),
              //       margin: EdgeInsets.all(20),
              //       color: AppColors.deepGreen,
              //       width: Responsive.width(context) * .7,
              //       height: 67,
              //       child: Center(
              //         child: Text(
              //           'Next',
              //           style: TextStyle(color: AppColors.white),
              //         ),
              //       )),
              // ),
            ]),
          );
        });
  }
}
