/// status : true
/// message : "Please check your phone for the verification code."
/// data : 27410

class RegisterPhoneNoResponse {
  bool _status;
  String _message;
  int _data;

  bool get status => _status;
  String get message => _message;
  int get data => _data;

  RegisterPhoneNoResponse({
      bool status, 
      String message, 
      int data}){
    _status = status;
    _message = message;
    _data = data;
}

  RegisterPhoneNoResponse.fromJson(dynamic json) {
    _status = json["status"];
    _message = json["message"];
    _data = json["data"];
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map["status"] = _status;
    map["message"] = _message;
    map["data"] = _data;
    return map;
  }

}