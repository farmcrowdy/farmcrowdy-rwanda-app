import 'package:flutter/material.dart';

// All the colors used in the app is declared here
class AppColors {

  // static const Color primaryColor = Color(0xFF1DA1F2);
  static const Color secondaryColor = Color.fromARGB(255, 246, 247, 255);

  static const Color backgroundColor = Color(0xFFF4F3F8);

  //Black
  static const Color black = Color(0xFF000000);
  static const Color filterBlack = Color(0XFF181725);

  //White
  static const Color white = Color(0xFFFFFFFF);

  //Grey
  static const Color grey = Color(0xFFE0E0E0);
  static const Color deepGrey = Color(0XFF7C7C7C);


  //Grey
  static const Color lowGrey = Color(0xFF7C7C7C);

  //Red
  static const Color red = Color(0xFFF1291A);

  //Green
  static const Color green = Color(0xFF54AD6D);

  //Blue
  static const Color blue = Color(0xFF5582AD);

  //Yellow
  static const Color yellow = Color(0xFFFFE603);

  //Lemon
  static const Color lemon = Color(0xFFE7FFE4);

  //deepGreen
  static const Color deepGreen = Color(0xFF2F8D26);

  //themeGreen
  static const Color themeGreen = Color(0xFF7DBD64);


}