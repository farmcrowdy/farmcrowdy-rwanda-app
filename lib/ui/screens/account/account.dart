import 'dart:io';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:provider_architecture/provider_architecture.dart';
import 'package:rwandashop/core/services/analytics_service.dart';
import 'package:rwandashop/core/services/auth_service.dart';
import 'package:rwandashop/ui/screens/account/account_view_model.dart';
import 'package:rwandashop/ui/screens/bottomNavigation/bottomNav_modelView.dart';
import 'package:rwandashop/ui/screens/login/second_login_view.dart';
import 'package:rwandashop/ui/widget/generalButton.dart';
import 'package:rwandashop/utils/app_localizations.dart';
import 'package:rwandashop/utils/colors.dart';
import 'package:rwandashop/utils/locator.dart';
import 'package:rwandashop/utils/router/navigationService.dart';
import 'package:rwandashop/utils/router/routeNames.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:get/get.dart';
import 'package:image/image.dart' as Im;
import 'package:path_provider/path_provider.dart';

class Account extends StatefulWidget {
  @override
  AccountState createState() => new AccountState();
}

class AccountState extends State<Account> {
  final AppStateProvider appStateProvider = locator<AppStateProvider>();
  final Authentication _authentication = locator<Authentication>();
  final NavigationService _navigationService = locator<NavigationService>();
  final Analytics _analytics = locator<Analytics>();
  File file;
  var rating;
  var noRating;
  Map<String, dynamic> responseData;
  var data;
  var body;
  var token;
  var provider;
  bool isUploading = false;
  String fileName;
  String dp;
  final DateTime timestamp = DateTime.now();
  AccountViewModel profileViewModel = AccountViewModel();
  @override
  void initState() {
    super.initState();
  }

  handleTakePhoto() async {
    // Timer(Duration(seconds: 5),(){
    //   Navigator.pop(context);
    // });

    File file = await ImagePicker.pickImage(
        source: ImageSource.camera, maxWidth: 960, maxHeight: 675);
    setState(() {
      this.file = file;
      _startUploading();
    });
  }

  handleChooseFromGallery() async {
    File file = await ImagePicker.pickImage(source: ImageSource.gallery);
    setState(() {
      fileName = file.path.split('/').last;
      this.file = file;
      _startUploading();
    });
    // Navigator.pop(context);
  }

  selectImage() {
    return showDialog(
        context: context,
        builder: (context) {
          return SimpleDialog(
            title: Text('Update Display Picture'),
            children: <Widget>[
              SimpleDialogOption(
                child: Text('Photo with Camera'),
                onPressed: handleTakePhoto,
              ),
              SimpleDialogOption(
                child: Text('Image from Gallery'),
                onPressed: handleChooseFromGallery,
              ),
              SimpleDialogOption(
                child: Text('Cancel'),
                onPressed: () => Navigator.pop(context),
              )
            ],
          );
        });
  }

  compressImage() async {
    final tempDir = await getTemporaryDirectory();
    final path = tempDir.path;
    Im.Image imageFile = Im.decodeImage(file.readAsBytesSync());
    final compressedImageFile = File('$path/"$timestamp.jpg')
      ..writeAsBytesSync(Im.encodeJpg(imageFile, quality: 85));
    setState(() {
      file = compressedImageFile;
    });
  }

  void _startUploading() async {
    // profilePr.show();
    var response = await profileViewModel.uploadDp(file);
    print(response);
    // Check if any error occured

    if (response == null) {
      //profilePr.hide();
      _resetState();
      print('error');
    } else {
      //profilePr.hide();
      dp = response.data['data'];
      // CachedNetworkImage.evictFromCache(dp);
      print(dp);
      // showToast('You have successfully updated your picture');
    }
  }

  void _resetState() {
    setState(() {
      //profilePr.hide();
      isUploading = false;
      //file = null;
    });
  }

  @override
  Widget build(BuildContext context) {
    AppLocalizations tr = AppLocalizations.of(context);
    if (_authentication.currentUser.isNull) {
      return Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Container(
                padding: EdgeInsets.only(top: 60, left: 20),
                child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Row(
                        children: <Widget>[
                          Text(
                            tr.text('account.account'),
                            style: TextStyle(
                                color: Colors.black,
                                fontWeight: FontWeight.bold,
                                fontSize: 20),
                          ),
                        ],
                      ),
                      Divider(
                        thickness: 1,
                      ),
                      Center(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: <Widget>[
                            SizedBox(
                              height: 150,
                            ),
                            Text(
                              tr.text('account.signOrLog'),
                              style: TextStyle(
                                fontWeight: FontWeight.bold,
                                fontSize: 20,
                              ),
                            ),
                            Text(
                              tr.text('account.toViewOrder'),
                              style: TextStyle(
                                fontWeight: FontWeight.bold,
                                fontSize: 20,
                              ),
                            ),
                            SizedBox(
                              height: 20,
                            ),
                            Container(
                              padding: EdgeInsets.all(20),
                              child: GeneralButton(
                                onPressed: () {
                                  _navigationService.navigateTo(WelcomeRoute);
                                },
                                buttonText: tr.text('account.getStarted'),
                              ),
                            ),
                            InkWell(
                              onTap: () {
                                // appStateProvider.setCurrentTabTo(
                                //     newTabIndex: 2);
                                // _navigationService
                                //     .navigateTo(BottomNavigationRoute);
                                Navigator.push(
                                  context,
                                  new MaterialPageRoute(
                                      builder: (context) =>
                                          new SecondLoginPage(pageIndex: 1)),
                                );
                              },
                              child: Text(
                                tr.text('account.logIn'),
                                style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  color: AppColors.green,
                                  fontSize: 15,
                                ),
                              ),
                            )
                          ],
                        ),
                      )
                    ]))
          ]);
    }
    return ViewModelProvider<AccountViewModel>.withConsumer(
        onModelReady: (model) {
          _analytics.setCurrentScreen('Account page', 'For settings');
        },
        viewModelBuilder: () => AccountViewModel(),
        builder: (context, model, child) {
          return Scaffold(
            body: SingleChildScrollView(
              child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    SizedBox(
                      height: 100.h,
                    ),
                    Divider(
                      thickness: 1,
                    ),
                    InkWell(
                      onTap: () {
                        _navigationService.navigateTo(YourInfoRoute);
                      },
                      child: Container(
                        padding: EdgeInsets.all(10.w),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Row(children: [
                              Container(
                                height: 15.w,
                                width: 15.w,
                                child: Image.asset(
                                  'assets/images/myDetails.png',
                                  fit: BoxFit.fill,
                                ),
                              ),
                              SizedBox(
                                width: 15.w,
                              ),
                              Text(
                                tr.text('account.yourInfo'),
                                style: TextStyle(
                                    fontSize: 14.ssp,
                                    fontWeight: FontWeight.bold),
                              ),
                            ]),
                            Icon(
                              Icons.arrow_forward_ios,
                              color: Colors.black,
                              size: 18.w,
                            )
                          ],
                        ),
                      ),
                    ),
                    Divider(
                      thickness: 1,
                    ),
                    InkWell(
                      onTap: () {
                        _navigationService.navigateTo(ChangePasswordRoute);
                      },
                      child: Container(
                        padding: EdgeInsets.all(10.w),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Row(children: [
                              Container(
                                height: 15.w,
                                width: 15.w,
                                child: Image.asset(
                                  'assets/images/location.png',
                                  fit: BoxFit.fill,
                                ),
                              ),
                              SizedBox(
                                width: 15.w,
                              ),
                              Text(
                                tr.text('account.changePassword'),
                                style: TextStyle(
                                    fontSize: 14.ssp,
                                    fontWeight: FontWeight.bold),
                              ),
                            ]),
                            Icon(
                              Icons.arrow_forward_ios,
                              color: Colors.black,
                              size: 18.w,
                            )
                          ],
                        ),
                      ),
                    ),
                    Divider(
                      thickness: 1,
                    ),
                    InkWell(
                      onTap: () {
                        _navigationService
                            .navigateTo(LanguageScreenRoute);
                      },
                      child: Container(
                        padding: EdgeInsets.all(10.w),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Row(children: [
                              Container(
                                height: 15.w,
                                width: 15.w,
                                child: Image.asset(
                                  'assets/images/location.png',
                                  fit: BoxFit.fill,
                                ),
                              ),
                              SizedBox(
                                width: 15.w,
                              ),
                              Text(
                                tr.text('account.language'),
                                style: TextStyle(
                                    fontSize: 14.ssp,
                                    fontWeight: FontWeight.bold),
                              ),
                            ]),
                            Icon(
                              Icons.arrow_forward_ios,
                              color: Colors.black,
                              size: 18.w,
                            )
                          ],
                        ),
                      ),
                    ),
                    Divider(
                      thickness: 1,
                    ),

                    // InkWell(
                    //   onTap: () {
                    //     setState(() {
                    //       appStateProvider.setCurrentTabTo(newTabIndex: 2);
                    //
                    //       _navigationService.navigateTo(BottomNavigationRoute);
                    //     });
                    //   },
                    //   child: Container(
                    //     padding: EdgeInsets.all(20),
                    //     child: Row(
                    //       mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    //       children: [
                    //         Row(children: [
                    //           Container(
                    //             height: 20,
                    //             width: 20,
                    //             child: Image.asset(
                    //               'images/order.png',
                    //               fit: BoxFit.fill,
                    //             ),
                    //           ),
                    //           SizedBox(
                    //             width: 15,
                    //           ),
                    //           Text(
                    //             'Orders',
                    //             style: TextStyle(
                    //                 fontSize: 14, fontWeight: FontWeight.bold),
                    //           ),
                    //         ]),
                    //         Icon(
                    //           Icons.arrow_forward_ios,
                    //           color: Colors.black,
                    //           size: 18,
                    //         )
                    //       ],
                    //     ),
                    //   ),
                    // ),
                    // Container(
                    //   padding: EdgeInsets.all(10.w),
                    //   child: Row(
                    //     mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    //     children: [
                    //       Row(children: [
                    //         Container(
                    //           height: 15.w,
                    //           width: 15.w,
                    //           child: Image.asset(
                    //             'images/aboutIcon.png',
                    //             fit: BoxFit.fill,
                    //           ),
                    //         ),
                    //         SizedBox(
                    //           width: 15.w,
                    //         ),
                    //         Text(
                    //           'About',
                    //           style: TextStyle(
                    //               fontSize: 14.ssp,
                    //               fontWeight: FontWeight.bold),
                    //         ),
                    //       ]),
                    //       Icon(
                    //         Icons.arrow_forward_ios,
                    //         color: Colors.black,
                    //         size: 18.w,
                    //       )
                    //     ],
                    //   ),
                    // ),
                    // Divider(
                    //   thickness: 1,
                    // ),
                    SizedBox(
                      height: 40.h,
                    ),
                  ]),
            ),
            bottomNavigationBar: Container(
                margin: EdgeInsets.all(10.w),
                padding: EdgeInsets.only(left: 20.w, right: 20.w),
                height: 57.h,
                child: RaisedButton(
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.all(Radius.circular(15.r)),
                      side: BorderSide(color: Color(0xffF2F3F2)),
                    ),
                    color: Color(0xffF2F3F2),
                    onPressed: () async {
                      SharedPreferences prefs =
                          await SharedPreferences.getInstance();
                      prefs.remove('profile');
                      _navigationService.navigateClearRouteTo(LoginRoute);
                      //  onPressed();
                    },
                    // },
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Container(
                          height: 20.w,
                          width: 20.w,
                          child: Image.asset(
                            'assets/images/logOut.png',
                            fit: BoxFit.fill,
                          ),
                        ),
                        Text(
                          tr.text('account.logOut'),
                          style: TextStyle(
                              fontSize: 14,
                              fontWeight: FontWeight.bold,
                              color: Color(0xff7DBD64)),
                        ),
                        Text(
                          '',
                          style: TextStyle(
                              fontSize: 14.ssp, fontWeight: FontWeight.bold),
                        ),
                      ],
                    ))),
          );
        });
  }
}
