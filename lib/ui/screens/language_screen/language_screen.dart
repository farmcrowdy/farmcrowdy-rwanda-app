import 'package:flutter/material.dart';
import 'package:provider_architecture/provider_architecture.dart';
import 'package:rwandashop/core/services/language_services.dart';
import 'package:rwandashop/main.dart';
import 'package:rwandashop/ui/screens/language_screen/language_view_model.dart';
import 'package:rwandashop/ui/widget/generalButton.dart';
import 'package:rwandashop/utils/app_localizations.dart';
import 'package:rwandashop/utils/locator.dart';
import 'package:rwandashop/utils/router/navigationService.dart';

//Created by Daniel Makinde
//This the screen to select the preferred language

class LanguageScreen extends StatefulWidget {
  //This is to check of the user is coming from the launching of the application or settings
  final bool appStart;


  LanguageScreen({this.appStart = true}) : super();

  @override
  _LanguageScreenState createState() => _LanguageScreenState();
}

class _LanguageScreenState extends State<LanguageScreen> {
  final LanguageScreenViewModel languageScreenViewModel = locator<LanguageScreenViewModel>();
  AppLocalizations tr;
  String langCode;
  getLangCode() async{
    langCode = await languageScreenViewModel.selectedDropdownLanguageString;
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    final NavigationService _navigationService = locator<NavigationService>();
    // this send the bool value to the view model
    final bool start = widget.appStart;
    // this initialize the language localizations
    tr = AppLocalizations.of(context);
    final String heading = start == true ? tr.text('') : tr.text('') ;
    return ViewModelProvider<LanguageScreenViewModel>.withConsumer(
        viewModelBuilder: () => LanguageScreenViewModel(),
        builder: (context, model, child) {
          model.appStart = widget.appStart;
          getLangCode();

          return Scaffold(
            body:SingleChildScrollView(
                child: Padding(
                  padding: EdgeInsets.only(
                      top: start == true ? 15 : 0, left: 20, right: 20),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[

                      heading != null
                          ? Text(
                              heading,
                              style: TextStyle(
                                  fontSize: 20, fontWeight: FontWeight.bold),
                            )
                          : SizedBox.shrink(),
                      SizedBox(
                        height: 25,
                      ),
                      SizedBox(
                        height: MediaQuery.of(context).size.height -
                            100 -
                            (start == true ? 200 : 240),
                        child: ListView(
                          padding: const EdgeInsets.all(0),
                          children: <Widget>[
                            SizedBox(
                              height: 160,
                            ),
                            Align(
                              alignment: Alignment.center,
                              child: Text(
                                tr.text('languageScreen.selectLanguage'),
                                style: TextStyle(
                                    fontSize: 16, fontWeight: FontWeight.bold),
                              ),
                            ),
                            SizedBox(height: 16),
                            Container(
                              width: 200,
                              padding: EdgeInsets.all(15),
                              decoration: BoxDecoration(
                                  color: Color(0xffEEEEEE),
                                  border:
                                      Border.all(width: 0, color: Colors.white),
                                  borderRadius: BorderRadius.circular(25)),
                              child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  Expanded(
                                    child: DropdownButtonHideUnderline(
                                      child: ButtonTheme(
                                        alignedDropdown: true,
                                        child: DropdownButton<String>(
                                          isDense: true,
                                          hint: new Text("Select Language"),
                                          value: model.selectedDropdownLanguage, //langCode, //model.selectedDropdownLanguage,//
                                          onChanged: (String newValue) async {
                                            // this send the language selected to the view model
                                            model.selectedDropdownLanguage =
                                                newValue;
                                            setState(() {});

                                            // this send the language selected to the view model
                                            print(
                                                model.selectedDropdownLanguage);
                                            // this saves the selected language
                                            await model
                                                .setPreferredLanguageChanged(context);
                                          },
                                          items: model.myJsonLanguage
                                              .map((Map map) {
                                            return new DropdownMenuItem<String>(
                                              value: map["code"].toString(),
                                              child: Row(
                                                children: [
                                                  Image.asset(map["image"],
                                                      width: 28),
                                                  Container(
                                                      margin: EdgeInsets.only(
                                                        left: 16,
                                                      ),
                                                      child: Text(map["name"])),
                                                ],
                                              ),
                                            );
                                          }).toList(),
                                        ),
                                      ),
                                    ),
                                  )
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                      GeneralButton(
                        onPressed: () async {
                          await model.setPreferredLanguage(context);
                        },
                        buttonText: tr.text('languageScreen.save'),
                      )
                    ],
                  ),
                ),
              ),
          );
        });
  }
}
