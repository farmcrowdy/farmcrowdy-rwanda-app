import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:rwandashop/ui/widget/generalButton.dart';
import 'package:rwandashop/utils/app_localizations.dart';
import 'package:rwandashop/utils/colors.dart';
import 'package:rwandashop/utils/locator.dart';
import 'package:rwandashop/utils/router/navigationService.dart';
import 'package:rwandashop/utils/router/routeNames.dart';

// The welcome page where you can select either to login or register
class WelcomePage extends StatefulWidget {
  @override
  WelcomePageState createState() => new WelcomePageState();
}

class WelcomePageState extends State<WelcomePage> {
  final NavigationService _navigationService = locator<NavigationService>();
  @override
  void initState() {
    super.initState();
  }

  //
  @override
  Widget build(BuildContext context) {
    AppLocalizations tr = AppLocalizations.of(context);
    final deviceHeight = MediaQuery.of(context).size.height;
    final deviceWidth = MediaQuery.of(context).size.width;
    return
      Stack(// <-- STACK AS THE SCAFFOLD PARENT
        children: [
      Container(
        height: deviceHeight,
        width: deviceWidth,
        decoration: BoxDecoration(
          color: Colors.black.withOpacity(0.9),
          image: DecorationImage(
            colorFilter: new ColorFilter.mode(
                Colors.black.withOpacity(0.6), BlendMode.dstATop),
            image: AssetImage(
              'assets/images/welcomepage_background.png',
            ),
            fit: BoxFit.fill,
          ),
        ),
      ),
      Scaffold(
        backgroundColor: Colors.transparent,
        body: Container(
          padding: EdgeInsets.all(10),
          child: SingleChildScrollView(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                SizedBox(
                  height: deviceHeight * .35,
                ),
                Container(
                  height: 40.w,
                  width: 40.w,
                  child: Image.asset('assets/images/icon_new.png'),
                ),
                SizedBox(height: 20),

                Text(
                  tr.text('welcome.welcome'),
                  textAlign: TextAlign.center,
                  style: TextStyle(
                      fontSize: 46.ssp,
                      color: AppColors.white,
                      fontWeight: FontWeight.bold),
                ),
                Text(
                  tr.text('welcome.ourStore'),
                  style: TextStyle(
                      fontSize: 46.ssp,
                      color: AppColors.white,
                      fontWeight: FontWeight.bold),
                ),
                SizedBox(height: 20.h,),
                Text(
                  tr.text('welcome.getInputs'),
                  textAlign: TextAlign.center,
                  style: TextStyle(
                      fontSize: 16.ssp,
                      color: AppColors.white,
                      fontWeight: FontWeight.w400),
                ),
                Container(
                    padding: EdgeInsets.all(20.w),
                    child: GeneralButton(
                      buttonText:
                      tr.text('welcome.getStarted'),
                      onPressed: () {
                        _navigationService.navigateTo(RegisterRoute);
                      },
                    )),
                InkWell(
                  onTap: () {
                    _navigationService.navigateTo(LoginRoute);
                  },
                  child: Text(
                    tr.text('welcome.logIn'),
                    style: TextStyle(
                        color: Colors.white,
                        fontWeight: FontWeight.bold,
                        fontSize: 18.ssp),
                  ),
                )
              ],
            ),
          ),
        ),
      )
    ]);
  }
}
