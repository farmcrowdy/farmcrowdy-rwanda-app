/// status : true
/// message : "Success"
/// data : {"access_token":"eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiIzIiwianRpIjoiZjc0MWJmZjE4YzBiYTQzNjUzNjgwNDhlZmU4NzA0ODUxMjVmMjI0ZGJkMWU0MGU2ODRmYzM1YWZkYjFjNDM0ZDRlYzA1NTg5NWZmOTVlYmEiLCJpYXQiOjE2MTczMDI4NzgsIm5iZiI6MTYxNzMwMjg3OCwiZXhwIjoxNjQ4ODM4ODc4LCJzdWIiOiIxNTI1Iiwic2NvcGVzIjpbXX0.b6uybBhl71KHiP_x1Trsr8UJj3UykhVHVJ0XU-mdHxAa6xLV4OVGd32Ylp3RHMxdNEhw8EKxdsFb4cNXyGdfdG-XE6I0naY8UnbgE8IEJXvXkvI0MKyIDxjFbBCLObibk8VKJXQLy4KJg46RA-EK5blw6ndze6pTKCxuW6rkSQbAVBWPPtVBgLiBgj7xpevhFHebVakW1FZqZK1qTI9D3DmRpPTIwzvbp6kk2P6PeqbtTUyAyZnn1Z9gDNNHSkwko_5W1AMx60vbvHSH6UEgDdAJ8Jp31ukhkUtvQ30C5BQn4s8btbW2VBqFmdItfbJvfGGubYKFUHrDfOjUoiM1Ewphy03Unm63dLJuIrO54j6Kx_8sPxaRNQrYYeTRWiSHUpShED2axux3WpylQeeZy3CuRn_rJANNZ-g0-HRT9wu3WSBjuE7FrLgBktdkWdgmD9ra6LjgLAShX-iDU4M4c0e3okE7m043NSbdWe7uGntdYRrdlU8r6mGLK7gdToC94ViLdfrGBNQnVRCf0HjueC2FadXI8u59h7WJcKxbM9jgiSjfQ_xMo6wARpUsJ4vkcyRjg6gwzbYd63VKcaPFVVU2172HB-RqrBAjG0oY1ROAd9pqbiBMBpnt2jpPNxKIzl0OD7nGqdGmaC3bQurdGkgilsXhF1GJMQPwuj2u984","user":{"id":1525,"name":null,"email":null,"phone_number":"+2508169545793","gender":null,"address":null,"city":null,"email_verified_at":null,"created_at":"2021-04-01 18:47:45","updated_at":"2021-04-01 18:47:58","is_phone_verified":1,"is_default_pass_changed":"No","avatar":"user.jpg","role_id":1,"state_id":null,"zone_id":null,"city_id":null},"locations":[{"id":5,"name":"Lekki Phase 1","created_at":"2020-04-18 00:00:00","updated_at":"2020-04-18 00:00:00","address":"Lekki Phase 1\r\n","phone_number":"08153731891","contact_person":"Mr IGE","user_id":1},{"id":6,"name":"Foodables","created_at":"2020-05-21 08:48:41","updated_at":"2020-05-21 08:48:41","address":"Chris Maduike","phone_number":"08058885937","contact_person":"Ayodeji","user_id":855}]}

class VerifyOtpResponse {
  bool _status;
  String _message;
  Data _data;

  bool get status => _status;
  String get message => _message;
  Data get data => _data;

  VerifyOtpResponse({
      bool status, 
      String message, 
      Data data}){
    _status = status;
    _message = message;
    _data = data;
}

  VerifyOtpResponse.fromJson(dynamic json) {
    _status = json["status"];
    _message = json["message"];
    _data = json["data"] != null ? Data.fromJson(json["data"]) : null;
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map["status"] = _status;
    map["message"] = _message;
    if (_data != null) {
      map["data"] = _data.toJson();
    }
    return map;
  }

}

/// access_token : "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiIzIiwianRpIjoiZjc0MWJmZjE4YzBiYTQzNjUzNjgwNDhlZmU4NzA0ODUxMjVmMjI0ZGJkMWU0MGU2ODRmYzM1YWZkYjFjNDM0ZDRlYzA1NTg5NWZmOTVlYmEiLCJpYXQiOjE2MTczMDI4NzgsIm5iZiI6MTYxNzMwMjg3OCwiZXhwIjoxNjQ4ODM4ODc4LCJzdWIiOiIxNTI1Iiwic2NvcGVzIjpbXX0.b6uybBhl71KHiP_x1Trsr8UJj3UykhVHVJ0XU-mdHxAa6xLV4OVGd32Ylp3RHMxdNEhw8EKxdsFb4cNXyGdfdG-XE6I0naY8UnbgE8IEJXvXkvI0MKyIDxjFbBCLObibk8VKJXQLy4KJg46RA-EK5blw6ndze6pTKCxuW6rkSQbAVBWPPtVBgLiBgj7xpevhFHebVakW1FZqZK1qTI9D3DmRpPTIwzvbp6kk2P6PeqbtTUyAyZnn1Z9gDNNHSkwko_5W1AMx60vbvHSH6UEgDdAJ8Jp31ukhkUtvQ30C5BQn4s8btbW2VBqFmdItfbJvfGGubYKFUHrDfOjUoiM1Ewphy03Unm63dLJuIrO54j6Kx_8sPxaRNQrYYeTRWiSHUpShED2axux3WpylQeeZy3CuRn_rJANNZ-g0-HRT9wu3WSBjuE7FrLgBktdkWdgmD9ra6LjgLAShX-iDU4M4c0e3okE7m043NSbdWe7uGntdYRrdlU8r6mGLK7gdToC94ViLdfrGBNQnVRCf0HjueC2FadXI8u59h7WJcKxbM9jgiSjfQ_xMo6wARpUsJ4vkcyRjg6gwzbYd63VKcaPFVVU2172HB-RqrBAjG0oY1ROAd9pqbiBMBpnt2jpPNxKIzl0OD7nGqdGmaC3bQurdGkgilsXhF1GJMQPwuj2u984"
/// user : {"id":1525,"name":null,"email":null,"phone_number":"+2508169545793","gender":null,"address":null,"city":null,"email_verified_at":null,"created_at":"2021-04-01 18:47:45","updated_at":"2021-04-01 18:47:58","is_phone_verified":1,"is_default_pass_changed":"No","avatar":"user.jpg","role_id":1,"state_id":null,"zone_id":null,"city_id":null}
/// locations : [{"id":5,"name":"Lekki Phase 1","created_at":"2020-04-18 00:00:00","updated_at":"2020-04-18 00:00:00","address":"Lekki Phase 1\r\n","phone_number":"08153731891","contact_person":"Mr IGE","user_id":1},{"id":6,"name":"Foodables","created_at":"2020-05-21 08:48:41","updated_at":"2020-05-21 08:48:41","address":"Chris Maduike","phone_number":"08058885937","contact_person":"Ayodeji","user_id":855}]

class Data {
  String _accessToken;
  User _user;
  List<Locations> _locations;

  String get accessToken => _accessToken;
  User get user => _user;
  List<Locations> get locations => _locations;

  Data({
      String accessToken, 
      User user, 
      List<Locations> locations}){
    _accessToken = accessToken;
    _user = user;
    _locations = locations;
}

  Data.fromJson(dynamic json) {
    _accessToken = json["access_token"];
    _user = json["user"] != null ? User.fromJson(json["user"]) : null;
    if (json["locations"] != null) {
      _locations = [];
      json["locations"].forEach((v) {
        _locations.add(Locations.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map["access_token"] = _accessToken;
    if (_user != null) {
      map["user"] = _user.toJson();
    }
    if (_locations != null) {
      map["locations"] = _locations.map((v) => v.toJson()).toList();
    }
    return map;
  }

}

/// id : 5
/// name : "Lekki Phase 1"
/// created_at : "2020-04-18 00:00:00"
/// updated_at : "2020-04-18 00:00:00"
/// address : "Lekki Phase 1\r\n"
/// phone_number : "08153731891"
/// contact_person : "Mr IGE"
/// user_id : 1

class Locations {
  int _id;
  String _name;
  String _createdAt;
  String _updatedAt;
  String _address;
  String _phoneNumber;
  String _contactPerson;
  int _userId;

  int get id => _id;
  String get name => _name;
  String get createdAt => _createdAt;
  String get updatedAt => _updatedAt;
  String get address => _address;
  String get phoneNumber => _phoneNumber;
  String get contactPerson => _contactPerson;
  int get userId => _userId;

  Locations({
      int id, 
      String name, 
      String createdAt, 
      String updatedAt, 
      String address, 
      String phoneNumber, 
      String contactPerson, 
      int userId}){
    _id = id;
    _name = name;
    _createdAt = createdAt;
    _updatedAt = updatedAt;
    _address = address;
    _phoneNumber = phoneNumber;
    _contactPerson = contactPerson;
    _userId = userId;
}

  Locations.fromJson(dynamic json) {
    _id = json["id"];
    _name = json["name"];
    _createdAt = json["created_at"];
    _updatedAt = json["updated_at"];
    _address = json["address"];
    _phoneNumber = json["phone_number"];
    _contactPerson = json["contact_person"];
    _userId = json["user_id"];
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map["id"] = _id;
    map["name"] = _name;
    map["created_at"] = _createdAt;
    map["updated_at"] = _updatedAt;
    map["address"] = _address;
    map["phone_number"] = _phoneNumber;
    map["contact_person"] = _contactPerson;
    map["user_id"] = _userId;
    return map;
  }

}

/// id : 1525
/// name : null
/// email : null
/// phone_number : "+2508169545793"
/// gender : null
/// address : null
/// city : null
/// email_verified_at : null
/// created_at : "2021-04-01 18:47:45"
/// updated_at : "2021-04-01 18:47:58"
/// is_phone_verified : 1
/// is_default_pass_changed : "No"
/// avatar : "user.jpg"
/// role_id : 1
/// state_id : null
/// zone_id : null
/// city_id : null

class User {
  int _id;
  dynamic _name;
  dynamic _email;
  String _phoneNumber;
  dynamic _gender;
  dynamic _address;
  dynamic _city;
  dynamic _emailVerifiedAt;
  String _createdAt;
  String _updatedAt;
  int _isPhoneVerified;
  String _isDefaultPassChanged;
  String _avatar;
  int _roleId;
  dynamic _stateId;
  dynamic _zoneId;
  dynamic _cityId;

  int get id => _id;
  dynamic get name => _name;
  dynamic get email => _email;
  String get phoneNumber => _phoneNumber;
  dynamic get gender => _gender;
  dynamic get address => _address;
  dynamic get city => _city;
  dynamic get emailVerifiedAt => _emailVerifiedAt;
  String get createdAt => _createdAt;
  String get updatedAt => _updatedAt;
  int get isPhoneVerified => _isPhoneVerified;
  String get isDefaultPassChanged => _isDefaultPassChanged;
  String get avatar => _avatar;
  int get roleId => _roleId;
  dynamic get stateId => _stateId;
  dynamic get zoneId => _zoneId;
  dynamic get cityId => _cityId;

  User({
      int id, 
      dynamic name, 
      dynamic email, 
      String phoneNumber, 
      dynamic gender, 
      dynamic address, 
      dynamic city, 
      dynamic emailVerifiedAt, 
      String createdAt, 
      String updatedAt, 
      int isPhoneVerified, 
      String isDefaultPassChanged, 
      String avatar, 
      int roleId, 
      dynamic stateId, 
      dynamic zoneId, 
      dynamic cityId}){
    _id = id;
    _name = name;
    _email = email;
    _phoneNumber = phoneNumber;
    _gender = gender;
    _address = address;
    _city = city;
    _emailVerifiedAt = emailVerifiedAt;
    _createdAt = createdAt;
    _updatedAt = updatedAt;
    _isPhoneVerified = isPhoneVerified;
    _isDefaultPassChanged = isDefaultPassChanged;
    _avatar = avatar;
    _roleId = roleId;
    _stateId = stateId;
    _zoneId = zoneId;
    _cityId = cityId;
}

  User.fromJson(dynamic json) {
    _id = json["id"];
    _name = json["name"];
    _email = json["email"];
    _phoneNumber = json["phone_number"];
    _gender = json["gender"];
    _address = json["address"];
    _city = json["city"];
    _emailVerifiedAt = json["email_verified_at"];
    _createdAt = json["created_at"];
    _updatedAt = json["updated_at"];
    _isPhoneVerified = json["is_phone_verified"];
    _isDefaultPassChanged = json["is_default_pass_changed"];
    _avatar = json["avatar"];
    _roleId = json["role_id"];
    _stateId = json["state_id"];
    _zoneId = json["zone_id"];
    _cityId = json["city_id"];
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map["id"] = _id;
    map["name"] = _name;
    map["email"] = _email;
    map["phone_number"] = _phoneNumber;
    map["gender"] = _gender;
    map["address"] = _address;
    map["city"] = _city;
    map["email_verified_at"] = _emailVerifiedAt;
    map["created_at"] = _createdAt;
    map["updated_at"] = _updatedAt;
    map["is_phone_verified"] = _isPhoneVerified;
    map["is_default_pass_changed"] = _isDefaultPassChanged;
    map["avatar"] = _avatar;
    map["role_id"] = _roleId;
    map["state_id"] = _stateId;
    map["zone_id"] = _zoneId;
    map["city_id"] = _cityId;
    return map;
  }

}