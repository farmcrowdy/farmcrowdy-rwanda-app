import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:rwandashop/core/model/product/cart.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:rwandashop/ui/screens/cart/cart_view_model.dart';
import 'package:rwandashop/utils/colors.dart';

// This is the widget
class CartWidget extends StatelessWidget {
  final Orders cartModel;
  final Function onTap;
  final Function onAdd;
  final Function onSub;
  final Function onCall;
  final CartViewModel model;

  CartWidget({this.onCall, this.cartModel, this.onTap, this.onAdd, this.onSub,this.model});

  @override
  Widget build(BuildContext context) {
    return Container(
        color: Colors.white,
        padding: EdgeInsets.all(10.w),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Row(children: [
              // Container(
              //   height: 70.w,
              //   width: 70.w,
              //   child: Image.asset('assets/images/cide.png'),
              // ),
              Container(
                height: 60.w,
                width: 60.w,
                decoration: BoxDecoration(
                  image: DecorationImage(
                      image: CachedNetworkImageProvider(cartModel.image),
                      fit: BoxFit.fill),
                ),
              ),
              SizedBox(
                width: 10.h,
              ),
              Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    SizedBox(
                      width: MediaQuery.of(context).size.width * 0.5,
                      child: Text(
                        cartModel.name,
                        overflow: TextOverflow.ellipsis,
                        softWrap: false,
                        style: TextStyle(
                            fontSize: 12.ssp, fontWeight: FontWeight.w600),
                      ),
                    ),
                    SizedBox(
                      height: 5.h,
                    ),
                    Container(
                      padding: EdgeInsets.only(left: 20.w, right: 20.w),
                      child: Row(
                        children: [
                          InkWell(
                            onTap: () {
                              if (onSub != null) {
                                onSub();
                              }
                              //model.minus();
                            },
                            child: Icon(
                              Icons.remove,
                              size: 25,
                            ),
                          ),
                          SizedBox(
                            width: 5.w,
                          ),
                          Container(
                            width: 25.w,
                            height: 25.0.w,
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(7.0.r),
                                border: Border.all(
                                  color: Colors.grey,
                                )),
                            child:
                                // Text(
                                //   cartModel.quantity.toString(),
                                //   style:
                                //   TextStyle(fontSize: 14.ssp, color: Color(0xff7C7C7C)),
                                // ),
                                //
                                Center(
                              child: Text(
                                cartModel.quantity.toString(),
                                // model.quantity.toString(),
                                style: TextStyle(fontWeight: FontWeight.bold),
                              ),
                            ),
                          ),
                          SizedBox(
                            width: 5,
                          ),
                          InkWell(
                              onTap: () {
                                if (onAdd != null) {
                                  onAdd();
                                }
                                //model.add();
                              },
                              child: Icon(
                                Icons.add,
                                color: AppColors.themeGreen,
                                size: 25.w,
                              )),
                          SizedBox(
                            width: 40,
                          ),
                          InkWell(
                              onTap: () async{
                                await onCall();
                                model.callStore();
                              },
                              child: Icon(
                                Icons.phone,
                                color: AppColors.themeGreen,
                                size: 30.w,
                              )),
                        ],
                      ),
                    ),
                  ]),
            ]),
            Column(
              children: [
                InkWell(
                    onTap: () {
                      if (onTap != null) {
                        onTap();
                      }
                    },
                    child: Container(
                      height: 15.w,
                      width: 15.w,
                      child: Image.asset(
                        'assets/images/cancel.png',
                        color: Colors.red,
                      ),
                    )),
                SizedBox(
                  height: 20.h,
                ),
                Text(
                  'RF ${cartModel.price}',
                  style:
                      TextStyle(fontSize: 12.ssp, fontWeight: FontWeight.w600),
                ),
              ],
            )
          ],
        ));
  }
}
