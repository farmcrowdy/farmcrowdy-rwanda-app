import 'dart:async';
import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

//Created by Daniel Makinde
//This manages the language localization

const List<String> supportedLanguages = ['en', 'ky'];

class AppLocalizations {
  final Locale locale;

  AppLocalizations(this.locale);

  static const List<String> _supportedLanguages = supportedLanguages;

  static Iterable<Locale> supportedLocales() =>
      _supportedLanguages.map<Locale>((lang) => new Locale(lang, ''));

  static AppLocalizations of(BuildContext context) {
    return Localizations.of<AppLocalizations>(context, AppLocalizations);
  }

  static const LocalizationsDelegate<AppLocalizations> delegate =
      _AppLocalizationsDelegate();

  Map<dynamic, dynamic> _localizedStrings;

  Future<bool> load() async {
    String jsonString =
        await rootBundle.loadString('assets/lang/${locale.languageCode}.json');
    _localizedStrings = json.decode(jsonString);
    return true;
  }

  String text(String key, {Map<String, String> params}) {
    String value;

    value = _localizedStrings['$key'];

    if (value == null || value == '') {
      return value = key;
    }

    if (params != null) {
      value = mapParamsToTranslatedString(value, params);
    }
    return value;
  }

  String mapParamsToTranslatedString(String value, Map<String, String> params) {
    params.forEach((key, value1) {
      value = value.replaceAll('@$key', value1);
    });
    return value;
  }
}

class _AppLocalizationsDelegate
    extends LocalizationsDelegate<AppLocalizations> {
  const _AppLocalizationsDelegate();

  @override
  bool isSupported(Locale locale) {
    return supportedLanguages.contains(locale.languageCode);
  }

  @override
  Future<AppLocalizations> load(Locale locale) async {
    AppLocalizations localizations = new AppLocalizations(locale);
    await localizations.load();
    return localizations;
  }

  @override
  bool shouldReload(_AppLocalizationsDelegate old) => false;
}
