import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:rwandashop/core/model/error_model.dart';
import 'package:rwandashop/core/model/product/add_cart.dart';
import 'package:rwandashop/core/model/product/catergories.dart';
import 'package:rwandashop/core/model/product/products.dart' as product;
import 'package:rwandashop/core/model/product/search.dart';
import 'package:rwandashop/core/model/success_model.dart';
import 'package:rwandashop/core/services/auth_service.dart';
import 'package:rwandashop/core/services/products_service.dart';
import 'package:rwandashop/utils/app_localizations.dart';
import 'package:rwandashop/utils/baseModel.dart';
import 'package:rwandashop/utils/colors.dart';
import 'package:rwandashop/utils/helpers.dart';
import 'package:rwandashop/utils/locator.dart';
import 'package:rwandashop/utils/router/navigationService.dart';
import 'package:url_launcher/url_launcher.dart';

// View model for shop page
class ShopViewModel extends BaseModel {
  final Product _product = locator<Product>();
  final NavigationService _navigationService = locator<NavigationService>();
  final Authentication _authentication = locator<Authentication>();

  Catergories categories;
  bool loading;
  int _categoryId = 6;
  int get categoryId => _categoryId;

  void setCategoryId(int categoryId){
    _categoryId = categoryId;
    notifyListeners();
  }

  String _searchName;
  String get searchName => _searchName;

  int _filterAction = 0;
  int get filterAction => _filterAction;

  void setFilterAction(int filterInt) {
    _filterAction = filterInt;
    notifyListeners();
  }


  bool loadingNew = false;

  callStore(String contact) async {
    var  url = "tel:$contact";
    if (await canLaunch(url)) {
      print(url);
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }

  Future<List<product.Data>> getPackagesByCategory(String categoryId) async {
    //loadingNew = true;
    //notifyListeners();
    //setBusy(true);

    var result = await _product.getPackages(categoryId);
    if (result is ErrorModel) {
     // loadingNew = false;
      // showToast('Login failed');
      print('errorr: ${result.error}');
      //notifyListeners();
      throw Exception('Failed to load internet');
      //return ErrorModel(result.error);
    } else {
     // loadingNew = false;
      //notifyListeners();
      print('category: $_categoryId  result: $result');
      print('HDHDHL:::::::::::::::::::::::');
      print(result);
      return result;
    }
  }

  Future<List<product.Data>> getPackagesByCategoryAndDistrict(String categoryId,String district) async {
    //loadingNew = true;
    //notifyListeners();
    //setBusy(true);

    var result = await _product.getPackagesByCategoryAndDistrict(categoryId,district);
    if (result is ErrorModel) {
      // loadingNew = false;
      // showToast('Login failed');
      print('errorr: ${result.error}');
      //notifyListeners();
      throw Exception('Failed to load internet');
      //return ErrorModel(result.error);
    } else {
      // loadingNew = false;
      //notifyListeners();
      print('category: $_categoryId  result: $result');
      print('HDHDHL:::::::::::::::::::::::');
      print(result);
      return result;
    }
  }

  Future<List<product.Data>> getSearchedPackages(Search search) async {
    //setBusy(true);
    var result = await _product.getSearchedPackages(search.toJson());
    if (result is ErrorModel) {
      // showToast('Login failed');
      print(result.error);
      notifyListeners();
      throw Exception('Failed to load internet');
      //return ErrorModel(result.error);
    }
    print('HDHDHL:::::::::::::::::::::::');
    print(result);
    return result;
  }

  initiateSearch(String val, ShopViewModel model) {
    if (val.length > 1 && val.isNotEmpty) {
      model.getSearchedPackages(Search(searchInput: val));
      notifyListeners();
      _searchName = val;
    } else {
      notifyListeners();
      _searchName = null;
    }
  }

  Future<List<Categories>> getCategories() async {
    if (searchName != null) {

      _searchName = null;
      // notifyListeners();
    }

    //setBusy(true);
    var result = await _product.getCategories();
    if (result is ErrorModel) {
      // showToast('Login failed');
      print(result.error);
      // notifyListeners();
      throw Exception('Failed to load internet');
      //return ErrorModel(result.error);
    } else {
      print('HDHDHL:::::::::::::::::::::::');
      print(result);
      return result;
    }
  }

  getCartSummary() async {
    //  loading = true;
    var result = await _product.getCartSummary();
    if (result is ErrorModel) {
      //  loading = false;
      showErrorToast(result.error);
      print(result.error);
      notifyListeners();
      return ErrorModel(result.error);
    }
    if (result is SuccessModel) {
      print('SUMMARY:::::::::::');
      print(result.data);
      //loading = false;
      //cart = result.data;
      notifyListeners();
      // return categories;
    }
  }

  addAndRemoveFromSavedPackages(String userId,String packageId, BuildContext context) async {
    AppLocalizations tr = AppLocalizations.of(context);
    setBusy(true);
    var result = await _product.addAndRemoveFromSavedPackage(userId,packageId);

    if (result is ErrorModel) {
      setBusy(false);
      showErrorToast(result.error);
      print(result.error);
      notifyListeners();
      return ErrorModel(result.error);
    }
    if (result is SuccessModel) {
      setBusy(false);
      Get.snackbar(
        tr.text('shop.productSaved'),
        result.data['message'],
        backgroundColor: AppColors.green,
      );
      // getCartSummary();

      //_product.getCartSummary();
      // Get.back();
      //setBusy(false);
      // _navigationService.navigateTo(BottomNavigationRoute);

      notifyListeners();
      //_navigationService.pop();
      // return SuccessModel(result.data);
    }
  }

  addToCart(AddCart addCart, BuildContext context) async {
    AppLocalizations tr = AppLocalizations.of(context);
    setBusy(true);
    var result = await _product.addToCart(addCart.toJson());

    if (result is ErrorModel) {
      setBusy(false);
      showErrorToast(result.error);
      print(result.error);
      notifyListeners();
      return ErrorModel(result.error);
    }
    if (result is SuccessModel) {
      setBusy(false);
      Get.snackbar(
        tr.text('shop.added'),
        result.data['message'],
        backgroundColor: AppColors.green,
      );
      getCartSummary();
      //_product.getCartSummary();
      // Get.back();
      //setBusy(false);
      // _navigationService.navigateTo(BottomNavigationRoute);

      notifyListeners();
      //_navigationService.pop();
      // return SuccessModel(result.data);
    }
  }


  getZone() async {
    var result = await _authentication.getZone();

    if (result is ErrorModel) {
      //setBusy(false);
      showErrorToast(result.error);
      print(result.error);
      notifyListeners();
      return ErrorModel(result.error);
    }
    if (result is SuccessModel) {
      //showToast(result.data['message']);

      //_user = result.data;

      //notifyListeners();
      print('''''''''''''');
      print(result.data);
      return result.data['data'];
    }
  }

  getCity(String id) async {
    var result = await _authentication.getCity(id);

    if (result is ErrorModel) {
      //setBusy(false);
      showErrorToast(result.error);
      print(result.error);
      notifyListeners();
      return ErrorModel(result.error);
    }
    if (result is SuccessModel) {
      //showToast(result.data['message']);

      //_user = result.data;

      //notifyListeners();
      return result.data['data'];
    }
  }

  getDistrict(String id) async {
    // setBusy(true);
    var result = await _authentication.getDistrict(id);

    if (result is ErrorModel) {
      // setBusy(false);
      showErrorToast(result.error);
      print(result.error);
      notifyListeners();
      return ErrorModel(result.error);
    }
    if (result is SuccessModel) {
      // setBusy(false);
      //showToast(result.data['message']);

      //_user = result.data;

      //notifyListeners();
      return result.data['data'];
    }
  }


}
