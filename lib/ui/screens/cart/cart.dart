import 'package:badges/badges.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:provider_architecture/provider_architecture.dart';
import 'package:rwandashop/core/model/product/cart.dart';
import 'package:rwandashop/core/model/product/edit_cart.dart';
import 'package:rwandashop/core/model/product/pay.dart';
import 'package:rwandashop/core/services/analytics_service.dart';
import 'package:rwandashop/core/services/auth_service.dart';
import 'package:rwandashop/core/services/products_service.dart';
import 'package:rwandashop/ui/screens/cart/cart_view_model.dart';
import 'package:rwandashop/ui/screens/login/loginViewModel.dart';
import 'package:rwandashop/ui/screens/login/second_login_view.dart';
import 'package:rwandashop/core/model/product/products.dart' as product;
import 'package:rwandashop/ui/widget/cart_widget.dart';
import 'package:rwandashop/ui/widget/generalButton.dart';
import 'package:rwandashop/ui/widget/package_widget.dart';
import 'package:rwandashop/utils/app_localizations.dart';
import 'package:rwandashop/utils/colors.dart';
import 'package:rwandashop/utils/locator.dart';
import 'package:rwandashop/utils/router/navigationService.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:intl/intl.dart';
import 'package:rwandashop/utils/router/routeNames.dart';
import 'package:rwandashop/utils/screensize.dart';
import 'package:shimmer/shimmer.dart';
import 'package:url_launcher/url_launcher.dart';

class CartPage extends StatefulWidget {
  final bool back;

  const CartPage({Key key, this.back = false});
  @override
  CartState createState() => new CartState();
}

class CartState extends State<CartPage> {
  TextEditingController numberController = TextEditingController();
  final NavigationService _navigationService = locator<NavigationService>();
  final Product _product = locator<Product>();
  final Authentication _authentication = locator<Authentication>();
  final oCcy = new NumberFormat("#,##0.00", "en_US");
  LoginViewModel loginViewModel = LoginViewModel();
  final Analytics _analytics = locator<Analytics>();
  String contact;
  TextEditingController phoneController = TextEditingController();

  deleteAlert(CartViewModel model, String id) {
    AppLocalizations tr = AppLocalizations.of(context);
    return showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            title: Text(
              tr.text('cart.deleteCart'),
            ),
            content: Text(
              tr.text('cart.sureDeleteCart'),
            ),
            actions: <Widget>[
              FlatButton(
                child: Text(
                  tr.text('cart.cancel'),
                  style: TextStyle(color: Colors.white, fontSize: 16),
                ),
                onPressed: () => Navigator.pop(context),
                color: Colors.grey,
              ),
              FlatButton(
                child: Text(
                  tr.text('cart.delete'),
                  style: TextStyle(color: AppColors.green, fontSize: 16),
                ),
                onPressed: () {
                  Navigator.pop(context);
                  model.deleteCart(id, context);
                },
              )
            ],
          );
        }
        );
  }

  callStore() async {
    var  url = "tel:$contact";
    if (await canLaunch(url)) {
      print(url);
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }

  payDialog(CartViewModel model) {
    AppLocalizations tr = AppLocalizations.of(context);
    return showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            title: Text(
              tr.text('Check Out'),
            ),
            content: Container(
                height: MediaQuery.of(context).size.height / 2.6,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Row(children: [
                      Text(
                        tr.text('cart.deliverySector'),
                        style: TextStyle(
                          // fontWeight: FontWeight.w800,
                          fontSize: 12.0.ssp,
                          color: Colors.black,
                        ),
                      ),
                      Text(': '),
                      Text(
                        _authentication.currentUser.sectorName != null
                            ? _authentication.currentUser.sectorName
                            : tr.text('changeAddress.sectorNotAddedYet'),
                        style: TextStyle(
                            fontSize: 12.ssp, fontWeight: FontWeight.bold),
                      ),
                    ]),
                    SizedBox(
                      height: 10,
                    ),
                    Container(
                        child: Row(
                      children: <Widget>[
                        Text(
                          tr.text('cart.total'),
                          style: TextStyle(
                            fontSize: 14.0.ssp,
                            color: Colors.black,
                          ),
                        ),
                        Text(': '),
                        Row(children: <Widget>[
                          new Text(
                            'RF ',
                            style: TextStyle(
                                color: Colors.black,
                                fontWeight: FontWeight.bold,
                                fontSize: 14.ssp),
                          ),
                          Text(
                            _product.cartSummary.data.finalCharge != null
                                ? oCcy.format(_product
                                    .cartSummary.data.finalCharge
                                    .round())
                                : '0.00',
                            style: TextStyle(
                              fontWeight: FontWeight.bold,
                              fontSize: 14.0.ssp,
                              color: Colors.black,
                            ),
                          ),
                        ]),
                      ],
                    )),
                    SizedBox(
                      height: 20,
                    ),
                    Container(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: <Widget>[
                          Text(
                            tr.text('cart.payPhone'),
                            style: TextStyle(
                                color: Colors.black45,
                                fontWeight: FontWeight.bold,
                                fontSize: 12),
                          ),
                          SizedBox(
                            height: 10,
                          ),
                          TextFormField(
                            //controller: emailController,
                            decoration: InputDecoration(
//                    errorText: residentialValid
//                        ? null
//                        : 'Kindly update this field',
                              hintText: '',
                              hintStyle: TextStyle(
                                color: Colors.black45,
                              ),
                              labelStyle: TextStyle(color: Colors.blue),
                              border: new OutlineInputBorder(
                                borderRadius: new BorderRadius.circular(5.0),
                                borderSide: new BorderSide(),
                              ),
                            ),
                            keyboardType: TextInputType.phone,
                            style: TextStyle(color: Colors.black),
                            cursorColor: Colors.black,
                            controller: phoneController,
                          ),
                        ],
                      ),
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    Container(
                        padding: EdgeInsets.only(left: 20.w, right: 20.w),
                        height: 37.h,
                        width: 200.w,
                        child: GeneralButton(
                          buttonText: tr.text('cart.pay'),
                          onPressed: () {
                            // model.pay(
                            //     Pay(
                            //         currency: 'RWF',
                            //         amount: double.parse(_product
                            //             .cartSummary.data.finalCharge
                            //             .toString()),
                            //         phonenumber: phoneController.text),
                            //     context);
                          },
                        )),
                  ],
                )),
          );
        });
  }

  bool log;
  loggedIn() {
    if (_authentication.currentUser.isNull) {
      log = false;
      // return false;
    } else {
      log = true;
    }
  }

  @override
  void initState() {
    super.initState();
    loggedIn();
    if (!_authentication.currentUser.isNull) {
      phoneController.text = _authentication.currentUser.phoneNumber.toString();
    }
  }

  @override
  Widget build(BuildContext context) {
    AppLocalizations tr = AppLocalizations.of(context);
    return ViewModelProvider<CartViewModel>.withConsumer(
        onModelReady: (model) {
          _analytics.setCurrentScreen('Saved page', 'Items added to cart');
          model.getCart();
        },
        viewModelBuilder: () => CartViewModel(),
        builder: (context, model, child) {
          if (_authentication.currentUser.isNull) {
            return Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Container(
                      padding: EdgeInsets.only(top: 60, left: 20, right: 20),
                      child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Row(
                              children: <Widget>[
                                Text(
                                  tr.text('general.saved'),
                                  style: TextStyle(
                                      color: Colors.black,
                                      fontWeight: FontWeight.bold,
                                      fontSize: 20),
                                ),
                              ],
                            ),
                            Divider(
                              thickness: 1,
                            ),
                            Center(
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: <Widget>[
                                  SizedBox(
                                    height: 150,
                                  ),
                                  Text(
                                    tr.text('cart.signOrLog'),
                                    style: TextStyle(
                                      fontWeight: FontWeight.bold,
                                      fontSize: 20,
                                    ),
                                  ),
                                  Text(
                                    tr.text('cart.toViewCart'),
                                    style: TextStyle(
                                      fontWeight: FontWeight.bold,
                                      fontSize: 20,
                                    ),
                                  ),
                                  SizedBox(
                                    height: 20,
                                  ),
                                  Container(
                                    padding: EdgeInsets.all(20),
                                    child: GeneralButton(
                                      onPressed: () {
                                        _navigationService
                                            .navigateTo(WelcomeRoute);
                                      },
                                      buttonText: tr.text('cart.getStarted'),
                                    ),
                                  ),
                                  InkWell(
                                    onTap: () {
                                      Navigator.push(
                                        context,
                                        new MaterialPageRoute(
                                            builder: (context) =>
                                                new SecondLoginPage(
                                                    pageIndex: 1)),
                                      );
                                    },
                                    child: Text(
                                      tr.text('cart.logIn'),
                                      style: TextStyle(
                                        fontWeight: FontWeight.bold,
                                        color: AppColors.green,
                                        fontSize: 15,
                                      ),
                                    ),
                                  )
                                ],
                              ),
                            )
                          ]))
                ]);
          } else
            return Scaffold(
                body: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      SizedBox(
                        height: 50.h,
                      ),
                      Container(
                          padding: EdgeInsets.only(left: 20.w, right: 20.w),
                          child:
                              // InkWell(
                              //   onTap: (){
                              //     _navigationService.pop();
                              //   },
                              //   child:
                              //   Icon(
                              //
                              //     Icons.arrow_back_ios, color: Colors.black,
                              //   ),),
                              Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Row(
                                children: [
                                  widget.back
                                      ? InkWell(
                                          onTap: () {
                                            _navigationService.pop();
                                          },
                                          child: Icon(Icons.arrow_back_ios),
                                        )
                                      : Text(''),
                                  Container(
                                    child: Text(
                                      tr.text('general.saved'),
                                      style: TextStyle(
                                          fontSize: 24.ssp,
                                          fontWeight: FontWeight.bold),
                                    ),
                                  ),
                                ],
                              ),
                              // Badge(
                              //   borderRadius: BorderRadius.circular(5.r),
                              //   badgeColor: AppColors.themeGreen,
                              //   badgeContent: Text(
                              //     _product.cartSummary.data.basketSize
                              //         .toString(),
                              //     style: TextStyle(
                              //         color: Colors.white,
                              //         fontWeight: FontWeight.bold),
                              //   ),
                              //   child: Container(
                              //     height: 20.h,
                              //     width: 20.w,
                              //     child: Image.asset('assets/images/cart.png'),
                              //   ),
                              // ),
                            ],
                          )),
                      SizedBox(
                        height: 10.h,
                      ),
                      Expanded(
                        child: Container(
                          margin: EdgeInsets.only(right: 10,left: 10),
                          child: FutureBuilder<List<product.Data>>(
                              future: model.getSavedPackages(),
                              builder: (context, snapshot) {
                                if (!snapshot.hasData) {
                                  return Container(
                                    padding: EdgeInsets.all(20.0),
                                    child: Shimmer.fromColors(
                                        direction: ShimmerDirection.ltr,
                                        period: Duration(seconds: 2),
                                        child: ListView(
                                          children: [0, 1, 2, 3, 4, 5]
                                              .map((_) => Padding(
                                                    padding:
                                                        const EdgeInsets.all(8.0),
                                                    child: Row(
                                                      crossAxisAlignment:
                                                          CrossAxisAlignment
                                                              .start,
                                                      children: [
                                                        Container(
                                                          width: Responsive.width(
                                                                  context) -
                                                              20,
                                                          height: 150,
                                                          color: Colors.white,
                                                        ),
                                                        Padding(
                                                          padding:
                                                              const EdgeInsets
                                                                      .symmetric(
                                                                  horizontal:
                                                                      8.0),
                                                        ),
                                                        Expanded(
                                                          child: Column(
                                                            crossAxisAlignment:
                                                                CrossAxisAlignment
                                                                    .start,
                                                            children: [
                                                              Container(
                                                                width: double
                                                                    .infinity,
                                                                height: 8.0,
                                                                color:
                                                                    Colors.white,
                                                              ),
                                                              Padding(
                                                                padding:
                                                                    const EdgeInsets
                                                                            .symmetric(
                                                                        vertical:
                                                                            2.0),
                                                              ),
                                                              Container(
                                                                width: double
                                                                    .infinity,
                                                                height: 8.0,
                                                                color:
                                                                    Colors.white,
                                                              ),
                                                              Padding(
                                                                padding:
                                                                    const EdgeInsets
                                                                            .symmetric(
                                                                        vertical:
                                                                            2.0),
                                                              ),
                                                              Container(
                                                                width: 40.0,
                                                                height: 8.0,
                                                                color:
                                                                    Colors.white,
                                                              ),
                                                            ],
                                                          ),
                                                        )
                                                      ],
                                                    ),
                                                  ))
                                              .toList(),
                                        ),
                                        baseColor: AppColors.grey,
                                        highlightColor: Colors.white),
                                  );
                                } else if (snapshot.data.isNotEmpty) {
                                  return ListView.builder(
                                      itemCount: snapshot.data.length,
                                      itemBuilder: (context, index) {
                                        return Card(
                                          elevation: 2,
                                          child: Container(
                                            child: PackageWidget(
                                              packageModel: snapshot.data[index],
                                              model: model,
                                              screenName: 'savedScreen',
                                              //categoryId: productId.toString(),
                                            ),
                                          ),
                                        );
                                      });
                                    // ListView(
                                    //   // scrollDirection: Axis.horizontal,
                                    //   shrinkWrap: true,
                                    //   children: snapshot.data
                                    //       .map((feed) => CartWidget(
                                    //             cartModel: feed,
                                    //             onCall: () async{
                                    //               contact = await model.getSinglePackage(feed.id.toString());
                                    //               callStore();
                                    //             },
                                    //             onTap: () {
                                    //               deleteAlert(
                                    //                   model, feed.id.toString());
                                    //             },
                                    //             onAdd: () {
                                    //               model.editCart(
                                    //                   feed.id.toString(),
                                    //                   context,
                                    //                   EditCart(
                                    //                       userId: _authentication
                                    //                           .currentUser.id,
                                    //                       packageId:
                                    //                           feed.packageId,
                                    //                       quantity:
                                    //                           feed.quantity + 1,
                                    //                       weight: 1,
                                    //                       statusId: 4));
                                    //             },
                                    //             onSub: () {
                                    //               model.editCart(
                                    //                   feed.id.toString(),
                                    //                   context,
                                    //                   EditCart(
                                    //                       userId: _authentication
                                    //                           .currentUser.id,
                                    //                       packageId:
                                    //                           feed.packageId,
                                    //                       quantity:
                                    //                           feed.quantity - 1,
                                    //                       weight: 1,
                                    //                       statusId: 4));
                                    //             },
                                    //           ))
                                    //       .toList());

                                } else if (snapshot.hasError) {
                                  return Center(
                                      child: Column(
                                    children: <Widget>[
                                      SizedBox(
                                        height: 100,
                                      ),
                                      Text(
                                        tr.text('cart.error'),
                                        style: TextStyle(
                                          fontWeight: FontWeight.bold,
                                          fontSize: 20,
                                        ),
                                      ),
                                      SizedBox(
                                        height: 10,
                                      ),
                                      Text(tr.text('cart.notConnected')),
                                      SizedBox(
                                        height: 100,
                                      ),
                                    ],
                                  ));
                                } else {
                                  return Center(
                                      child: Column(
                                    children: <Widget>[
                                      SizedBox(
                                        height: 100,
                                      ),
                                      Text(
                                        tr.text('saved.noSavedProduct'),
                                        style: TextStyle(
                                          fontWeight: FontWeight.bold,
                                          fontSize: 20,
                                        ),
                                      ),
                                      SizedBox(
                                        height: 10,
                                      ),
                                      Text(
                                        tr.text('saved.pageInfo'),
                                      ),
                                      SizedBox(
                                        height: 100,
                                      ),
                                    ],
                                  ));
                                }
                              }),
                        ),
                      ),
                    ]),
                // bottomNavigationBar: Container(
                //   color:Colors.grey[100],
                //   height: Responsive.height(context) * .2,
                //   child: Column(
                //     crossAxisAlignment: CrossAxisAlignment.center,
                //     mainAxisAlignment: MainAxisAlignment.center,
                //     children: [
                //       Container(
                //         padding: EdgeInsets.only(left: 20.w, top: 10.w),
                //         child: Text(
                //           tr.text('cart.orderSummary'),
                //           style: TextStyle(
                //             fontWeight: FontWeight.w800,
                //             fontSize: 15.0.ssp,
                //             color: Colors.black,
                //           ),
                //         ),
                //       ),
                //       SizedBox(
                //         height: 10.h,
                //       ),
                //       Container(
                //           padding: EdgeInsets.only(left: 20.w, right: 20.w, top: 10),
                //           child: Row(
                //             mainAxisAlignment: MainAxisAlignment.spaceBetween,
                //             children: <Widget>[
                //               Text(
                //                 tr.text('cart.total'),
                //                           style: TextStyle(
                //                             fontWeight: FontWeight.bold,
                //                             fontSize: 14.0.ssp,
                //                             color: Colors.black,
                //
                //                 // tr.text('cart.subTotal'),
                //                 // style: TextStyle(
                //                 //   // fontWeight: FontWeight.w800,
                //                 //   fontSize: 16.0.ssp,
                //                 //   color: Colors.black,
                //                 ),
                //               ),
                //               Row(children: <Widget>[
                //                 new Text(
                //                   'RF ',
                //                   style: TextStyle(
                //                       color: Colors.black, fontSize: 16.ssp),
                //                 ),
                //                 Text(
                //                   _product.cartSummary.data.subtotal != null
                //                       ? oCcy.format(
                //                           _product.cartSummary.data.subtotal)
                //                       : '',
                //                   style: TextStyle(
                //                     // fontWeight: FontWeight.w800,
                //                     fontSize: 16.0.ssp,
                //                     color: Colors.black,
                //                   ),
                //                 ),
                //               ]),
                //             ],
                //           )),
                //       Divider(),
                //       SizedBox(
                //         height: 5.h,
                //       ),
                //       // Container(
                //       //     padding:
                //       //         EdgeInsets.only(left: 20, right: 20, top: 10),
                //       //     child: Row(
                //       //       mainAxisAlignment: MainAxisAlignment.spaceBetween,
                //       //       children: <Widget>[
                //       //         Text(
                //       //           tr.text('cart.delivery'),
                //       //           style: TextStyle(
                //       //             // fontWeight: FontWeight.w800,
                //       //             fontSize: 16.0.ssp,
                //       //             color: Colors.black,
                //       //           ),
                //       //         ),
                //       //         Row(children: <Widget>[
                //       //           new Text(
                //       //             'RF ',
                //       //             style: TextStyle(
                //       //                 color: Colors.black, fontSize: 16.ssp),
                //       //           ),
                //       //           Text(
                //       //             _product.cartSummary.data.deliveryCharges !=
                //       //                     null
                //       //                 ? oCcy.format(_product
                //       //                     .cartSummary.data.deliveryCharges)
                //       //                 : '0.00',
                //       //             style: TextStyle(
                //       //               // fontWeight: FontWeight.w800,
                //       //               fontSize: 16.0.ssp,
                //       //               color: Colors.black,
                //       //             ),
                //       //           ),
                //       //         ]),
                //       //       ],
                //       //     )),
                //       // Divider(),
                //       // SizedBox(
                //       //   height: 10.h,
                //       // ),
                //       // Container(
                //       //     padding: EdgeInsets.only(left: 20.w, right: 20.w),
                //       //     child: Row(
                //       //       mainAxisAlignment: MainAxisAlignment.spaceBetween,
                //       //       children: <Widget>[
                //       //         Text(
                //       //           tr.text('cart.total'),
                //       //           style: TextStyle(
                //       //             fontWeight: FontWeight.bold,
                //       //             fontSize: 14.0.ssp,
                //       //             color: Colors.black,
                //       //           ),
                //       //         ),
                //       //         Row(children: <Widget>[
                //       //           new Text(
                //       //             'RF ',
                //       //             style: TextStyle(
                //       //                 color: Colors.black,
                //       //                 fontWeight: FontWeight.bold,
                //       //                 fontSize: 14.ssp),
                //       //           ),
                //       //           Text(
                //       //             _product.cartSummary.data.finalCharge != null
                //       //                 ? oCcy.format(_product
                //       //                     .cartSummary.data.finalCharge
                //       //                     .round())
                //       //                 : '0.00',
                //       //             style: TextStyle(
                //       //               fontWeight: FontWeight.bold,
                //       //               fontSize: 14.0.ssp,
                //       //               color: Colors.black,
                //       //             ),
                //       //           ),
                //       //         ]),
                //       //       ],
                //       //     )),
                //       // SizedBox(
                //       //   height: 20,
                //       // ),
                //       // Container(
                //       //     padding: EdgeInsets.only(left: 20.w, right: 20.w),
                //       //     height: 37.h,
                //       //     width: 200.w,
                //       //     child: GeneralButton(
                //       //       buttonText: tr.text('cart.checkOut'),
                //       //       onPressed: () {
                //       //         //_navigationService.navigateTo(PaymentRoute);
                //       //         //Navigator.push(context, MaterialPageRoute(builder: (BuildContext context) => PaymentPage()));
                //       //         model.handlePayment(
                //       //             context,
                //       //             _product.cartSummary.data.finalCharge
                //       //                 .round()
                //       //                 .toString());
                //       //         //payDialog(model);
                //       //         //_navigationService.navigateTo(CheckOutRoute);
                //       //         // scaffoldKey.currentState.showBottomSheet(
                //       //         //       (context) => CheckOutPage(),
                //       //         //   elevation: 10,
                //       //         //   shape: RoundedRectangleBorder(
                //       //         //     borderRadius: BorderRadius.circular(15.0),
                //       //         //   ),
                //       //         // );
                //       //
                //       //         //  _navigationService.navigateTo(CheckOutRoute);
                //       //       },
                //       //     )),
                //     ],
                //   ),
                // )

            );
        });
  }
}
