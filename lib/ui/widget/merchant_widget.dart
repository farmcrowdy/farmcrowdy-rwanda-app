import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:rwandashop/core/model/product/catergories.dart';
import 'package:rwandashop/ui/screens/packages/pakages.dart';
import 'package:rwandashop/utils/screensize.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

// This is the widget for products(items)

class MerchantWidget extends StatelessWidget {
  final Categories categoriesModel;
  final Function onTap;
  final Color color;

  MerchantWidget({this.categoriesModel, this.onTap, this.color});

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        Navigator.push(context, MaterialPageRoute(builder: (BuildContext context) => PackagePage(categoriesDetails: categoriesModel)));
        if (onTap != null) {
          onTap();
        }
      },
      child: Container(
          width: Responsive.width(context) / 2,
          padding: EdgeInsets.all(10.w),
          height: 190.0.h,
          decoration: BoxDecoration(
              color: color,
              borderRadius: BorderRadius.circular(15.0.r),
              border: Border.all(
                color: Colors.grey,
              )),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              SizedBox(
                height: 20,
              ),
              // Center(
              //   child: Container(
              //     height: 100.w,
              //     width: 100.w,
              //     child: Image.asset('assets/images/cide.png'),
              //   ),
              // ),
          Center(
            child:(null != categoriesModel.image)?
            Container(
                height: 100.w,
                width: 100.w,
                decoration:
                BoxDecoration(
                  image: DecorationImage(
                      image: CachedNetworkImageProvider(
                          categoriesModel.image),
                      fit: BoxFit
                          .fill),
                ),
              ) : Container(
              height: 100.w,
              width: 100.w,
            ),
          ),
              // Center(
              //   child: Container(
              //     height: 100.w,
              //     width: 100.w,
              //     child: Image.network(categoriesModel.image),
              //   ),
              // ),
              SizedBox(
                height: 10.h,
              ),
              Expanded(
                  child: Text(
                categoriesModel.name,
                style: TextStyle(
                    color: Colors.black,
                    fontSize: 12,
                    fontWeight: FontWeight.w600),
              ))
            ],
          )),
    );
  }
}
