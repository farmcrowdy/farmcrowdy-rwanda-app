import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:rwandashop/core/model/product/catergories.dart';
import 'package:rwandashop/utils/colors.dart';
import 'package:rwandashop/utils/screensize.dart';


// This is the widget for Merchant circle avatar on the home page
class CategoriesItem extends StatelessWidget {
  final Categories categoriesModel;
  final bool active;
  final Function onTap;

  CategoriesItem({this.categoriesModel, this.active = false, this.onTap});

  @override
  Widget build(BuildContext context) {
    return InkWell(
        onTap: () {
          print(categoriesModel.id);
          if (onTap != null) {
            onTap();
          }
        },
        child: Container(
            padding: EdgeInsets.all(10),
            child: Column(children: [
              // SizedBox(
              //   height: 10,
              // ),
              Text(
                categoriesModel.name.length > 12
                    ? categoriesModel.name.substring(0, 12) + '...'
                    : categoriesModel.name,
                style: TextStyle(
                    color: active? AppColors.green : Colors.grey,
                    fontSize: 14,
                    fontWeight: FontWeight.w700),
              ),
              SizedBox(
                height: 10,
              ),
              active ? Container(
                height: 2,
                width: Responsive.width(context) * .22,
                color: AppColors.green,
              ): Container()
            ])));
  }
}
