import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:rwandashop/core/model/product/add_cart.dart';
import 'package:rwandashop/core/model/error_model.dart';
import 'package:rwandashop/core/model/product/products.dart' as product;
import 'package:rwandashop/core/model/product/single_package.dart';
import 'package:rwandashop/core/model/success_model.dart';
import 'package:rwandashop/core/services/products_service.dart';
import 'package:rwandashop/utils/app_localizations.dart';
import 'package:rwandashop/utils/baseModel.dart';
import 'package:rwandashop/utils/colors.dart';
import 'package:rwandashop/utils/helpers.dart';
import 'package:rwandashop/utils/locator.dart';
import 'package:rwandashop/utils/router/navigationService.dart';
import 'package:url_launcher/url_launcher.dart';

class PackageDetailsViewModel extends BaseModel {
  final Product _product = locator<Product>();
  final NavigationService _navigationService = locator<NavigationService>();

  int _quantity= 1;
  int get quantity => _quantity;

  String _contact= '080000000000';
  String get contact => _contact;


  add() {
      _quantity++;
      notifyListeners();
      return _quantity;
  }

   minus() {
      if (_quantity != 1) _quantity--;
      notifyListeners();
      return _quantity;
  }

  callStore() async {
  var  url = "tel:$contact";
    if (await canLaunch(url)) {
      print(url);
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }

  getCartSummary() async {
    //  loading = true;
    var result = await _product.getCartSummary();
    if (result is ErrorModel) {
      //  loading = false;
      showErrorToast(result.error);
      print(result.error);
      notifyListeners();
      return ErrorModel(result.error);
    }
    if (result is SuccessModel) {
      print('SUMMARY:::::::::::');
      print(result.data);
      //loading = false;
      //cart = result.data;
      notifyListeners();
      // return categories;
    }
  }
  getSinglePackage(String id) async {
    //  loading = true;
    var result = await _product.getSinglePackages(id);
    if (result is ErrorModel) {
      //  loading = false;
      showErrorToast(result.error);
      print(result.error);
      notifyListeners();
      return ErrorModel(result.error);
    }
    if (result is SuccessModel) {
      print('SUMMARY:::::::::::');
      print(result.data);
      SinglePackage packageData = SinglePackage.fromJson(result.data);
      //loading = false;
      //cart = result.data;
      _contact = packageData.data.contact;
      print(_contact);
      notifyListeners();
      return packageData.data.contact;
    }
  }

  Future<List<product.Data>> getItem(String id) async {
    //loadingNew = true;
    //notifyListeners();
    //setBusy(true);
    var result = await _product.getPackages(id);
    if (result is ErrorModel) {
      // loadingNew = false;
      // showToast('Login failed');
      print('errorr: ${result.error}');
      //notifyListeners();
      throw Exception('Failed to load internet');
      //return ErrorModel(result.error);
    } else {
      // loadingNew = false;
      //notifyListeners();
      print('HDHDHL:::::::::::::::::::::::');
      print(result);
      return result;
    }
  }

  addAndRemoveFromSavedPackages(String userId,String packageId, BuildContext context) async {
    AppLocalizations tr = AppLocalizations.of(context);
    setBusy(true);
    var result = await _product.addAndRemoveFromSavedPackage(userId,packageId);

    if (result is ErrorModel) {
      setBusy(false);
      showErrorToast(result.error);
      print(result.error);
      notifyListeners();
      return ErrorModel(result.error);
    }
    if (result is SuccessModel) {
      setBusy(false);
      Get.snackbar(
        tr.text('shop.productSaved'),
        result.data['message'],
        backgroundColor: AppColors.green,
      );
      // getCartSummary();

      //_product.getCartSummary();
      // Get.back();
      //setBusy(false);
      // _navigationService.navigateTo(BottomNavigationRoute);

      notifyListeners();
      //_navigationService.pop();
      // return SuccessModel(result.data);
    }
  }

  addToCart(AddCart addCart, BuildContext context) async {
    AppLocalizations tr = AppLocalizations.of(context);
    setBusy(true);
    var result = await _product.addToCart(addCart.toJson());

    if (result is ErrorModel) {
      setBusy(false);
      showErrorToast(result.error);
      print(result.error);
      notifyListeners();
      return ErrorModel(result.error);
    }
    if (result is SuccessModel) {
      setBusy(false);
      Get.snackbar(tr.text('shop.added'),result.data['message'],backgroundColor: AppColors.green,
      );
      getCartSummary();
      //_product.getCartSummary();
     // Get.back();
      //setBusy(false);
    // _navigationService.navigateTo(BottomNavigationRoute);

      notifyListeners();
      //_navigationService.pop();
      // return SuccessModel(result.data);
    }
  }
}
