import 'dart:convert';
import 'package:rwandashop/core/model/authentication/auth_model.dart';
import 'package:rwandashop/core/model/error_model.dart';
import 'package:rwandashop/core/model/product/cart.dart';
import 'package:rwandashop/core/model/product/cart_summary.dart';
import 'package:rwandashop/core/model/product/catergories.dart';
import 'package:rwandashop/core/model/product/order_details_model.dart';
import 'package:rwandashop/core/model/product/orders.dart' as order;
import 'package:rwandashop/core/model/product/products.dart' as prod;
import 'package:rwandashop/core/model/success_model.dart';
import 'package:rwandashop/core/services/index.dart';
import 'package:rwandashop/utils/http/paths.dart';
import 'package:rwandashop/utils/locator.dart';
import 'package:rwandashop/utils/router/navigationService.dart';
import 'package:shared_preferences/shared_preferences.dart';

class Product {
  final NavigationService _navigationService = locator<NavigationService>();
  CartSummary _cartSummary;

  CartSummary get cartSummary => _cartSummary;

  String _phoneNumber;

  String get phoneNumber => _phoneNumber;

  String _productId;

  String get productId => _productId;

  dynamic _categoriesListStore;

  dynamic get categoriesListStore => _categoriesListStore;

  dynamic _filteredCategoriesListStore;

  dynamic get filteredCategoriesListStore => _filteredCategoriesListStore;

  dynamic _packageListStore;

  dynamic get packageListStore => _packageListStore;

  AuthModel _token;

  AuthModel get token => _token;

  getCartSummary() async {
    try {
      final result = await http.get(Paths.CART_SUMMARY);
      if (result is ErrorModel) {
        print("ERROR");
        print(result.error);
        return ErrorModel(result.error);
      }

      print("RESULT");
      print(result.data);
      CartSummary cartSummary = CartSummary.fromJson(result.data);
      _cartSummary = cartSummary;
      SharedPreferences prefs;
      prefs = await SharedPreferences.getInstance();
      var s = json.encode(_cartSummary);
      await prefs.setString('cartSummary', s);
      print('firstname' + cartSummary.data.finalCharge.toString());
      // return SuccessModel({'auth': auth, 'user': user});
      return SuccessModel(cartSummary);
    } catch (e) {
      print(e.toString());
      return ErrorModel('Error fetching cart summary.');
    }
  }

  alreadyLoggedInCart() async {
    SharedPreferences prefs;
    prefs = await SharedPreferences.getInstance();
    var d = prefs.getString('cartSummary');
    CartSummary cartSummary = CartSummary.fromJson(json.decode(d));
    _cartSummary = cartSummary;
    print(_cartSummary);
    print('refresh');
    // return SuccessModel(user);
  }

  getCategories() async {
    if (categoriesListStore != null) {
      print(categoriesListStore);
      List<Categories> categoriesList = List<Categories>.from(
          _categoriesListStore.map((item) => Categories.fromJson(item)));
      return categoriesList;
    } else {
      try {
        // final result = await http.get(Paths.MERCHANT);
        final result = await http.get(Paths.CATEGORIES);
        if (result is ErrorModel) {
          print("ERROR");
          print(result.error);
          var data = result.error;
          List<Categories> categoriesList = List<Categories>.from(
              data.map((item) => Categories.fromJson(item)));
          return ErrorModel(categoriesList);
        }
        print("RESULT");
        //print(result.data);
        var data = result.data['data']['categories'];//['data'];
        _categoriesListStore = data;
        List<Categories> categoriesList = List<Categories>.from(
            data.map((item) => Categories.fromJson(item)));
        return categoriesList;
      } catch (e) {
        print(e.toString());
        return ErrorModel('$e');
      }
    }
  }

  getMerchants() async {
    if (categoriesListStore != null) {
      print(categoriesListStore);
      List<Categories> categoriesList = List<Categories>.from(
          _categoriesListStore.map((item) => Categories.fromJson(item)));
      return categoriesList;
    } else {
      try {
        final result = await http.get(Paths.MERCHANT);
        if (result is ErrorModel) {
          print("ERROR");
          print(result.error);
          var data = result.error;
          List<Categories> categoriesList = List<Categories>.from(
              data.map((item) => Categories.fromJson(item)));
          return ErrorModel(categoriesList);
        }
        print("RESULT");
        //print(result.data);
        var data = result.data['data']['companies']['data'];
        _categoriesListStore = data;
        List<Categories> categoriesList = List<Categories>.from(
            data.map((item) => Categories.fromJson(item)));
        return categoriesList;
      } catch (e) {
        print(e.toString());
        return ErrorModel('$e');
      }
    }
  }

  getCategoriesByDistrict(String districtName) async {
    // if (categoriesListStore != null) {
    //   print(categoriesListStore);
    //   List<Categories> categoriesList = List<Categories>.from(
    //       _categoriesListStore.map((item) => Categories.fromJson(item)));
    //   return categoriesList;
    // } else {
      try {
        final result = await http.get(Paths.MERCHANT_BY_DISTRICT + districtName);
        if (result is ErrorModel) {
          print("ERROR ");
          print(result.error);
          var data = result.error;
          List<Categories> categoriesList = List<Categories>.from(
              data.map((item) => Categories.fromJson(item)));
          return ErrorModel(categoriesList);
        }
        print("RESULT");
        // var data = result.data['data']['companies']['data'];
        var data = result.data['data']['companies'];
        _filteredCategoriesListStore = data;
        List<Categories> categoriesList = List<Categories>.from(
            data.map((item) => Categories.fromJson(item)));
        print('cat length ${categoriesList.length}');
        return categoriesList;
      } catch (e) {
        print(e.toString());
        return ErrorModel('$e');
      }
    // }
  }

  getPackages(String id) async {
    if (id == '6') {
      if (packageListStore != null) {
        print(packageListStore);
        List<prod.Data> packageList = List<prod.Data>.from(
            _packageListStore.map((item) => prod.Data.fromJson(item)));
        return packageList;
      } else {
        try {
          final result = await http.get(Paths.PRODUCT_BY_CATEGORY + '$id');
          if (result is ErrorModel) {
            print("ERROR");
            var data = result.error;
            print(result.error);
            List<prod.Data> packageList = List<prod.Data>.from(
                data.map((item) => prod.Data.fromJson(item)));
            return ErrorModel(packageList);
          }
          var data = result.data['data'];
          _packageListStore = data;
          List<prod.Data> packageList = List<prod.Data>.from(
              data.map((item) => prod.Data.fromJson(item)));
          return packageList;
        } catch (e) {
          print(e.toString());
          return ErrorModel('$e');
        }
      }
    } else {
      try {
        final result = await http.get(Paths.MERCHANT_PACKAGE + '$id/packages');
        if (result is ErrorModel) {
          print("ERROR");
          var data = result.error;
          print(result.error);
          List<prod.Data> packageList = List<prod.Data>.from(
              data.map((item) => prod.Data.fromJson(item)));
          return ErrorModel(packageList);
        }
        var data = result.data['data'];
        //_packageListStore = data;
        List<prod.Data> packageList =
            List<prod.Data>.from(data.map((item) => prod.Data.fromJson(item)));
        return packageList;
      } catch (e) {
        print(e.toString());
        return ErrorModel('$e');
      }
    }
  }

  getPackagesByCategoryAndDistrict(String categoryId,String district) async {
    if (categoryId == '6') {
      if (packageListStore != null) {
        print(packageListStore);
        List<prod.Data> packageList = List<prod.Data>.from(
            _packageListStore.map((item) => prod.Data.fromJson(item)));
        return packageList;
      } else {
        try {
          final result = await http.get(Paths.PACKAGES_BY_CATEGORY_AND_DISTRICT + '?district=$district&category_id=$categoryId');
          if (result is ErrorModel) {
            print("ERROR");
            var data = result.error;
            print(result.error);
            List<prod.Data> packageList = List<prod.Data>.from(
                data.map((item) => prod.Data.fromJson(item)));
            return ErrorModel(packageList);
          }
          var data = result.data['data'];
          _packageListStore = data;
          List<prod.Data> packageList = List<prod.Data>.from(
              data.map((item) => prod.Data.fromJson(item)));
          return packageList;
        } catch (e) {
          print(e.toString());
          return ErrorModel('$e');
        }
      }
    } else {
      try {
        final result = await http.get(Paths.PACKAGES_BY_CATEGORY_AND_DISTRICT + '?district=$district&category_id=$categoryId');
        if (result is ErrorModel) {
          print("ERROR");
          var data = result.error;
          print(result.error);
          List<prod.Data> packageList = List<prod.Data>.from(
              data.map((item) => prod.Data.fromJson(item)));
          return ErrorModel(packageList);
        }
        var data = result.data['data'];
        //_packageListStore = data;
        List<prod.Data> packageList =
        List<prod.Data>.from(data.map((item) => prod.Data.fromJson(item)));
        return packageList;
      } catch (e) {
        print(e.toString());
        return ErrorModel('$e');
      }
    }
  }

  getSinglePackages(String id) async {
    try {
      final result = await http.get(Paths.SINGLE_PACKAGE + '$id');
      if (result is ErrorModel) {
        print("ERROR");
        var data = result.error;
        print(result.error);
      }
      var data = result.data;
      return SuccessModel(result.data);
    } catch (e) {
      print(e.toString());
      return ErrorModel('$e');
    }
  }

  getSearchedPackages(Map<dynamic, dynamic> payload) async {
    try {
      final result = await http.post(Paths.SEARCH, payload);
      if (result is ErrorModel) {
        print("ERROR");
        var data = result.error;
        print(result.error);
        List<prod.Data> packageList =
            List<prod.Data>.from(data.map((item) => prod.Data.fromJson(item)));
        return ErrorModel(packageList);
      }
      var data = result.data['data'];
      List<prod.Data> packageList =
          List<prod.Data>.from(data.map((item) => prod.Data.fromJson(item)));
      return packageList;
    } catch (e) {
      print(e.toString());
      return ErrorModel('$e');
    }
  }
  getSearchedMerchant(String name, ) async {
    try {
      final result = await http.get(Paths.MERCHANT_SEARCH + name);
      if (result is ErrorModel) {
        print("ERROR");
        var data = result.error;
        print(result.error);
        List<Categories> packageList =
        List<Categories>.from(data.map((item) => Categories.fromJson(item)));
        return ErrorModel(packageList);
      }
      var data = result.data['data']['companies'];
      List<Categories> packageList =
      List<Categories>.from(data.map((item) => Categories.fromJson(item)));
      return packageList;
    } catch (e) {
      print(e.toString());
      return ErrorModel('$e');
    }
  }

  getSearchedMerchantByDistrict(String name) async {
    try {
      final result = await http.get(Paths.MERCHANT_SEARCH_DISTRICT + name);
      if (result is ErrorModel) {
        print("ERROR");
        var data = result.error;
        print(result.error);
        List<Categories> packageList =
        List<Categories>.from(data.map((item) => Categories.fromJson(item)));
        return ErrorModel(packageList);
      }
      var data = result.data['data']['companies'];
      List<Categories> packageList =
      List<Categories>.from(data.map((item) => Categories.fromJson(item)));
      return packageList;
    } catch (e) {
      print(e.toString());
      return ErrorModel('$e');
    }
  }

  getCart() async {
    // if(cartListStore != null){
    //   List<Orders> cartList =
    //   List<Orders>.from(cartListStore.map((item) => Orders.fromJson(item)));
    //   return cartList;
    // }else {
    try {
      final result = await http.get(Paths.CART_ALL);
      if (result is ErrorModel) {
        print("ERROR");
        print(result.error);

        return ErrorModel(result.error);
      }
      print("RESULT");
      //print(result.data);
      var data = result.data['data']['orders'];
      //_cartListStore = data;
      List<Orders> cartList =
          List<Orders>.from(data.map((item) => Orders.fromJson(item)));
      return cartList;
    } catch (e) {
      print(e.toString());
      return ErrorModel('$e');
    }
  }

  getSavedPackagess() async {
    // if (userId == '6') {
    //   if (packageListStore != null) {
    //     print(packageListStore);
    //     List<prod.Data> packageList = List<prod.Data>.from(
    //         _packageListStore.map((item) => prod.Data.fromJson(item)));
    //     return packageList;
    //   } else {
    //     try {
    //       final result = await http.get(Paths.SAVED_PACKAGES);
    //       if (result is ErrorModel) {
    //         print("ERROR");
    //         var data = result.error;
    //         print(result.error);
    //         List<prod.Data> packageList = List<prod.Data>.from(
    //             data.map((item) => prod.Data.fromJson(item)));
    //         return ErrorModel(packageList);
    //       }
    //       var data = result.data['data'];
    //       _packageListStore = data;
    //       List<prod.Data> packageList = List<prod.Data>.from(
    //           data.map((item) => prod.Data.fromJson(item)));
    //       return packageList;
    //     } catch (e) {
    //       print(e.toString());
    //       return ErrorModel('$e');
    //     }
    //   }
    // } else {
      try {
        final result = await http.get(Paths.SAVED_PACKAGES);
        if (result is ErrorModel) {
          print("ERROR");
          var data = result.error;
          print(result.error);
          List<prod.Data> packageList = List<prod.Data>.from(
              data.map((item) => prod.Data.fromJson(item)));
          return ErrorModel(packageList);
        }
        var data = result.data['data'];
        //_packageListStore = data;
        List<prod.Data> packageList =
        List<prod.Data>.from(data.map((item) => prod.Data.fromJson(item)));
        return packageList;
      } catch (e) {
        print(e.toString());
        return ErrorModel('$e');
      }
    // }
  }

  getSavedPackages(String userId) async {
    // if(cartListStore != null){
    //   List<Orders> cartList =
    //   List<Orders>.from(cartListStore.map((item) => Orders.fromJson(item)));
    //   return cartList;
    // }else {
    try {
      final result = await http.get(Paths.SAVED_PACKAGES+ '$userId');
      if (result is ErrorModel) {
        print("ERROR");
        print(result.error);

        return ErrorModel(result.error);
      }
      print("RESULT");
      //print(result.data);
      var data = result.data['data']['orders'];
      //_cartListStore = data;
      List<Orders> cartList =
      List<Orders>.from(data.map((item) => Orders.fromJson(item)));
      return cartList;
    } catch (e) {
      print(e.toString());
      return ErrorModel('$e');
    }
  }

  //}
  deleteCart(String id) async {
    try {
      final result = await http.delete(Paths.DELETE_CART + id);
      if (result is ErrorModel) {
        print("ERROR");
        print(result.error);

        return ErrorModel(result.error);
      }

      print("RESULT");
      print(result.data);
      //
      // VerifyOtpResponse verifyOtpResponse =
      //     VerifyOtpResponse.fromJson(result.data);
      // print(verifyOtpResponse.message);
      return SuccessModel(result.data['message']);
    } catch (e) {
      print(e.toString());
      return ErrorModel('$e');
    }
  }
  editCart(String id,Map<dynamic, dynamic> payload) async {
    try {
      final result = await http.patch(Paths.EDIT_CART + id, payload );
      if (result is ErrorModel) {
        print("ERROR");
        print(result.error);

        return ErrorModel(result.error);
      }

      print("RESULT");
      print(result.data);
      //
      // VerifyOtpResponse verifyOtpResponse =
      //     VerifyOtpResponse.fromJson(result.data);
      // print(verifyOtpResponse.message);
      return SuccessModel(result.data['message']);
    } catch (e) {
      print(e.toString());
      return ErrorModel('$e');
    }
  }

  getOrderHistory() async {
    try {
      final result = await http.get(Paths.ORDER_HISTORY);
      if (result is ErrorModel) {
        print("ERROR");
        var data = result.error;
        List<order.Data> ordersList = List<order.Data>.from(
            data.map((item) => order.Data.fromJson(item)));
        return ErrorModel(ordersList);
      }
      var data = result.data['data'];
      List<order.Data> ordersList =
          List<order.Data>.from(data.map((item) => order.Data.fromJson(item)));
      return ordersList;
    } catch (e) {
      print(e.toString());
      return ErrorModel('$e');
    }
  }

  getDetails(String id) async {
    try {
      final result = await http.get(Paths.SINGLE_ORDER + id);
      if (result is ErrorModel) {
        print("ERROR");
        var data = result.error;
        List<OrdersDetails> ordersList = List<OrdersDetails>.from(
            data.map((item) => OrdersDetails.fromJson(item)));
        return ErrorModel(ordersList);
      }
      var data = result.data['data']['orders'];
      List<OrdersDetails> ordersList = List<OrdersDetails>.from(
          data.map((item) => OrdersDetails.fromJson(item)));
      return ordersList;
    } catch (e) {
      print(e.toString());
      return ErrorModel('$e');
    }
  }

  getOrderOngoing() async {
    try {
      final result = await http.get(Paths.ORDER_ONGOING);
      if (result is ErrorModel) {
        print("ERROR");
        var data = result.error;
        List<order.Data> ordersList = List<order.Data>.from(
            data.map((item) => order.Data.fromJson(item)));
        return ErrorModel(ordersList);
      }
      var data = result.data['data'];
      List<order.Data> ordersList =
          List<order.Data>.from(data.map((item) => order.Data.fromJson(item)));
      return ordersList;
    } catch (e) {
      print(e.toString());
      return ErrorModel('$e');
    }
  }

  addAndRemoveFromSavedPackage(String userId, String packageId) async {
    try {
      final result = await http.get(Paths.ADD_SAVED_PACKAGE+ '?userid=$userId&packageid=$packageId');
      if (result is ErrorModel) {
        print("ERROR");
        print(result.error);

        return ErrorModel(result.error);
      }

      print("RESULT");
      print(result.data);
      //
      // VerifyOtpResponse verifyOtpResponse =
      //     VerifyOtpResponse.fromJson(result.data);
      // print(verifyOtpResponse.message);
      return SuccessModel(result.data);
    } catch (e) {
      print(e.toString());
      return ErrorModel('$e');
    }
  }

  addToCart(Map<dynamic, dynamic> payload) async {
    try {
      final result = await http.post(Paths.ADD_CART, payload);
      if (result is ErrorModel) {
        print("ERROR");
        print(result.error);

        return ErrorModel(result.error);
      }

      print("RESULT");
      print(result.data);
      //
      // VerifyOtpResponse verifyOtpResponse =
      //     VerifyOtpResponse.fromJson(result.data);
      // print(verifyOtpResponse.message);
      return SuccessModel(result.data);
    } catch (e) {
      print(e.toString());
      return ErrorModel('$e');
    }
  }

  approvePayment(Map<dynamic, dynamic> payload) async {
    try {
      final result = await http.post(Paths.PAYMENT, payload);
      if (result is ErrorModel) {
        print("ERROR");
        print(result.error);
        return ErrorModel(result.error);
      }
      print("RESULT");
      print(result.data);
      //
      // VerifyOtpResponse verifyOtpResponse =
      //     VerifyOtpResponse.fromJson(result.data);
      // print(verifyOtpResponse.message);
      return SuccessModel(result.data);
    } catch (e) {
      print(e.toString());
      return ErrorModel('$e');
    }
  }


}
