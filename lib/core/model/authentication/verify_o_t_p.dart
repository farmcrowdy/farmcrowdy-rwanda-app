/// otp_code : ""
/// phone : ""

class VerifyOTP {
  String _otpCode;
  String _phone;

  String get otpCode => _otpCode;
  String get phone => _phone;

  VerifyOTP({
      String otpCode, 
      String phone}){
    _otpCode = otpCode;
    _phone = phone;
}

  VerifyOTP.fromJson(dynamic json) {
    _otpCode = json["otp_code"];
    _phone = json["phone"];
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map["otp_code"] = _otpCode;
    map["phone"] = _phone;
    return map;
  }

}