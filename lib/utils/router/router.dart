import 'package:flutter/material.dart';
import 'package:rwandashop/ui/screens/acceptedBackground.dart';
import 'package:rwandashop/ui/screens/account/account.dart';
import 'package:rwandashop/ui/screens/account/change_address.dart';
import 'package:rwandashop/ui/screens/account/change_contact.dart';
import 'package:rwandashop/ui/screens/account/change_name.dart';
import 'package:rwandashop/ui/screens/account/change_password.dart';
import 'package:rwandashop/ui/screens/account/your_info.dart';
import 'package:rwandashop/ui/screens/bottomNavigation/bottomNavigationView.dart';
import 'package:rwandashop/ui/screens/cart/cart.dart';
import 'package:rwandashop/ui/screens/checkOut/checkOutPage.dart';
import 'package:rwandashop/ui/screens/fliter/filter_view.dart';
import 'package:rwandashop/ui/screens/language_screen/language_screen.dart';
import 'package:rwandashop/ui/screens/login/loginView.dart';
import 'package:rwandashop/ui/screens/login/second_login_view.dart';
import 'package:rwandashop/ui/screens/package_details/package_details_view.dart';
import 'package:rwandashop/ui/screens/packages/pakages.dart';
import 'package:rwandashop/ui/screens/product/product.dart';
import 'package:rwandashop/ui/screens/register/enter_password.dart';
import 'package:rwandashop/ui/screens/register/forgot_password/change_password.dart';
import 'package:rwandashop/ui/screens/register/forgot_password/forgetPasswordView.dart';
import 'package:rwandashop/ui/screens/register/forgot_password/fp_otpView.dart';
import 'package:rwandashop/ui/screens/register/otpView.dart';
import 'package:rwandashop/ui/screens/register/registerView.dart';
import 'package:rwandashop/ui/screens/shop/shop_view.dart';
import 'package:rwandashop/ui/screens/welcomePage.dart';
import 'package:rwandashop/utils/router/routeNames.dart';


// This manages all the Routing of the app, routing must be declared here first to make available on the view models
Route<dynamic> generateRoute(RouteSettings settings) {
  switch (settings.name) {

    //
    // case OnBoardingRoute:
    // return _getPageRoute(
    // routeName: settings.name,
    // viewToShow: OnBoardingScreen(),
    // );
    case WelcomeRoute:
      return _getPageRoute(
        routeName: settings.name,
        viewToShow: WelcomePage(),
      );
    case LoginRoute:
      return _getPageRoute(
        routeName: settings.name,
        viewToShow: LoginPage(),
      );
    case RegisterRoute:
      return _getPageRoute(
        routeName: settings.name,
        viewToShow: Register(),
      );

    case OTPRoute:
      return _getPageRoute(
        routeName: settings.name,
        viewToShow: OTP(),
      );
    case EnterPasswordRoute:
      return _getPageRoute(
        routeName: settings.name,
        viewToShow: EnterPassword(),
      );
    case BottomNavigationRoute:
      return _getPageRoute(
        routeName: settings.name,
        viewToShow: BottomNavigation(),
      );
    case ShopRoute:
      return _getPageRoute(
        routeName: settings.name,
        viewToShow: ShopPage(),
      );
    case ProductDetailsPageRoute:
      return _getPageRoute(
        routeName: settings.name,
        viewToShow: PackageDetailsPage(),
      );

    case ProductRoute:
      return _getPageRoute(
        routeName: settings.name,
        viewToShow: Product(),
      );
    case ProductMoreRoute:
      return _getPageRoute(
        routeName: settings.name,
        viewToShow: PackagePage(),
      );
    //
    case FiltersRoute:
      return _getPageRoute(
        routeName: settings.name,
        viewToShow: Filters(),
      );

    case AccountRoute:
      return _getPageRoute(
        routeName: settings.name,
        viewToShow: Account(),
      );
    case AcceptedRoute:
      return _getPageRoute(
        routeName: settings.name,
        viewToShow: AcceptedPage(),
      );
    case CheckOutRoute:
      return _getPageRoute(
        routeName: settings.name,
        viewToShow: CheckOutPage(),
      );
    case ForgotPasswordRoute:
      return _getPageRoute(
        routeName: settings.name,
        viewToShow: ForgotPassword(),
      );
    case ForgotOTPRoute:
      return _getPageRoute(
        routeName: settings.name,
        viewToShow: ForgotOTP(),
      );
    case EnterPasswordFGRoute:
      return _getPageRoute(
        routeName: settings.name,
        viewToShow: EnterPasswordFG(),
      );
    case ChangePasswordRoute:
      return _getPageRoute(
        routeName: settings.name,
        viewToShow: ChangePasswordPage(),
      );
    case YourInfoRoute:
      return _getPageRoute(
        routeName: settings.name,
        viewToShow: YourInfo(),
      );
    case ChangeNameRoute:
      return _getPageRoute(
        routeName: settings.name,
        viewToShow: ChangeNamePage(),
      );
    case ChangeContactRoute:
      return _getPageRoute(
        routeName: settings.name,
        viewToShow: ChangeContact(),
      );
    case ChangeAddressRoute:
      return _getPageRoute(
        routeName: settings.name,
        viewToShow: ChangeAddress(),
      );
    case SecondLoginRoute:
      return _getPageRoute(
        routeName: settings.name,
        viewToShow: SecondLoginPage(),
      );
    case LanguageScreenRoute:
      return _getPageRoute(
        routeName: settings.name,
        viewToShow: LanguageScreen(appStart: false),
      );
    case CartScreenRoute:
      return _getPageRoute(
        routeName: settings.name,
        viewToShow: CartPage(back: true),
      );

    // case ForecastSingleRoute:
    //   var daily = settings.arguments as Daily;
    //
    //   return _getPageRoute(
    //     routeName: settings.name,
    //       viewToShow: FullForecast(daily: daily),
    //   );

    default:
      return MaterialPageRoute(
          builder: (_) => Scaffold(
                body: Center(
                    child: Text('No route defined for ${settings.name}')),
              ));
  }
}

PageRoute _getPageRoute({String routeName, Widget viewToShow}) {
  return MaterialPageRoute(
      settings: RouteSettings(
        name: routeName,
      ),
      builder: (_) => viewToShow);
}
