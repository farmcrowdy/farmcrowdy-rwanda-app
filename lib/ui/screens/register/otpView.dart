import 'dart:async';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider_architecture/provider_architecture.dart';
import 'package:rwandashop/core/model/authentication/resend_o_t_p.dart';
import 'package:rwandashop/core/model/authentication/verify_o_t_p.dart';
import 'package:rwandashop/core/services/analytics_service.dart';
import 'package:rwandashop/core/services/auth_service.dart';
import 'package:rwandashop/ui/screens/register/registerViewModel.dart';
import 'package:rwandashop/ui/widget/generalButton.dart';
import 'package:rwandashop/utils/app_localizations.dart';
import 'package:rwandashop/utils/colors.dart';
import 'package:rwandashop/utils/locator.dart';
import 'package:rwandashop/utils/router/navigationService.dart';
import 'package:rwandashop/utils/screensize.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class OTP extends StatefulWidget {
  @override
  OTPState createState() => new OTPState();
}

class OTPState extends State<OTP> {
  ScrollController _scrollController = new ScrollController();
  TextEditingController numberController = TextEditingController();
  final Authentication _authentication = locator<Authentication>();
  final Analytics _analytics = locator<Analytics>();
  final NavigationService _navigationService = locator<NavigationService>();
  bool passwordVisible;
  @override
  void initState() {
    super.initState();
    startTimeout();
    passwordVisible = true;
  }

  final interval = const Duration(seconds: 1);

  final int timerMaxSeconds = 60;

  int currentSeconds = 0;

  String get timerText =>
      '${((timerMaxSeconds - currentSeconds) ~/ 60).toString().padLeft(2, '0')}: ${((timerMaxSeconds - currentSeconds) % 60).toString().padLeft(2, '0')}';

  startTimeout([int milliseconds]) {
    var duration = interval;
    Timer.periodic(duration, (timer) {
      setState(() {
        //print(timer.tick);
        currentSeconds = timer.tick;
        if (timer.tick >= timerMaxSeconds) timer.cancel();
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    AppLocalizations tr = AppLocalizations.of(context);
    return ViewModelProvider<RegisterViewModel>.withConsumer(
        onModelReady: (model) {
          _analytics.setCurrentScreen('OTP page', 'Enter OTP');
        },
        viewModelBuilder: () => RegisterViewModel(),
        builder: (context, model, child) {
          return Scaffold(
            body: Container(
              child: SingleChildScrollView(
                child: Column( children: <Widget>[ //controller: _scrollController,
                  Container(
                    padding: EdgeInsets.fromLTRB(20, 50, 20, 20),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        InkWell(
                          onTap: () {
                            _navigationService.pop();
                          },
                          child: Icon(Icons.arrow_back_ios),
                        ),
                        Text(''),
                      ],
                    ),
                  ),
                  SizedBox(
                    height: 60.h,
                  ),
                  Align(
                    alignment: Alignment.centerLeft,
                    child: Container(
                      padding: EdgeInsets.only(left: 20.w, right: 20.w),
                      child: Text(
                        tr.text('otp.enterOtp'),
                        style:
                            TextStyle(fontSize: 26.ssp, fontWeight: FontWeight.w700),
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 30.h,
                  ),
                  Align(
                    alignment: Alignment.centerLeft,
                    child: Container(
                      padding: EdgeInsets.only(left: 20.w, right: 20.w),
                      child: Text(
                        tr.text('otp.code'),
                        style: TextStyle(
                            color: Color(0xff7C7C7C),
                            fontSize: 16.ssp,
                            fontWeight: FontWeight.w500),
                      ),
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.only(left: 20.w, right: 20.w),
                    child: TextFormField(
                      onTap: () {
                        _scrollController.animateTo(
                            _scrollController.position.maxScrollExtent,
                            duration: Duration(milliseconds: 500),
                            curve: Curves.fastOutSlowIn);
                      },
                      controller: numberController,
                      onChanged: (ss) {
                        _scrollController.animateTo(
                            _scrollController.position.maxScrollExtent,
                            duration: Duration(milliseconds: 500),
                            curve: Curves.fastOutSlowIn);
                      },
                      // maxLength: 11,
                      decoration: InputDecoration(
//                    errorText: residentialValid
//                        ? null
//                        : 'Kindly update this field',
                        hintText: '- - - -',
                        hintStyle: TextStyle(
                          color: Colors.black45,
                        ),
                        labelStyle: TextStyle(color: Colors.blue),
                        // border: new OutlineInputBorder(
                        //   borderRadius: new BorderRadius.circular(5.0),
                        //   borderSide: new BorderSide(),
                        // ),
                      ),
                      keyboardType: TextInputType.number,
                      style: TextStyle(color: Colors.black),
                      cursorColor: Colors.black,
                    ),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Container(
                      padding: EdgeInsets.all(20.w),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                InkWell(
                                  onTap: () {
                                    model.resendOtp(ResendOTP(
                                        phoneNumber: _authentication.phoneNumber,
                                        requestType: 'resendOTP'));
                                  },
                                  child: Text(
                                    tr.text('otp.resendCode'),
                                    style: TextStyle(
                                        fontSize: 15.ssp,
                                        color: AppColors.themeGreen,
                                        fontWeight: FontWeight.bold),
                                  ),
                                ),
                                SizedBox(width: 10,),
                                Text(
                                  '$timerText',
                                  style: TextStyle(
                                      fontSize: 15.ssp,
                                      color: Color(0xff7C7C7C),
                                      fontWeight: FontWeight.bold),
                                ),
                              ]),

                        ],
                      )),
                  SizedBox(
                    height: Responsive.height(context) * 0.35,
                  ),
                  Container(
                      padding: EdgeInsets.all(20.w),
                      child: GeneralButton(
                        buttonText:
                        tr.text('general.next'),
                        onPressed: () {
                          model.verifyOtp(VerifyOTP(
                              phone: _authentication.phoneNumber,
                              otpCode: numberController.text));
                        },
                      )),
                ]),
              ),
            ),
          );
        });
  }
}
