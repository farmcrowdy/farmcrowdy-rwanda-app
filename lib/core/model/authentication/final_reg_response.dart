/// status : true
/// message : "Password set successfully."
/// data : {"id":34,"name":null,"email":null,"phone_number":"+2348169545792","gender":null,"address":null,"city":null,"email_verified_at":null,"password":"$2y$10$JzqvS9Mn/.0jJGFHEpV4w.GRwhnP72OIrgHjFM./RwuvmsUkXWj8C","remember_token":null,"created_at":"2020-04-18 18:57:20","updated_at":"2021-03-24 10:14:10","is_phone_verified":1,"is_default_pass_changed":"No","avatar":"user.jpg","role_id":1,"state_id":null,"zone_id":"1","city_id":null}

class FinalRegResponse {
  bool _status;
  String _message;
  Data _data;

  bool get status => _status;
  String get message => _message;
  Data get data => _data;

  FinalRegResponse({
      bool status, 
      String message, 
      Data data}){
    _status = status;
    _message = message;
    _data = data;
}

  FinalRegResponse.fromJson(dynamic json) {
    _status = json["status"];
    _message = json["message"];
    _data = json["data"] != null ? Data.fromJson(json["data"]) : null;
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map["status"] = _status;
    map["message"] = _message;
    if (_data != null) {
      map["data"] = _data.toJson();
    }
    return map;
  }

}

/// id : 34
/// name : null
/// email : null
/// phone_number : "+2348169545792"
/// gender : null
/// address : null
/// city : null
/// email_verified_at : null
/// password : "$2y$10$JzqvS9Mn/.0jJGFHEpV4w.GRwhnP72OIrgHjFM./RwuvmsUkXWj8C"
/// remember_token : null
/// created_at : "2020-04-18 18:57:20"
/// updated_at : "2021-03-24 10:14:10"
/// is_phone_verified : 1
/// is_default_pass_changed : "No"
/// avatar : "user.jpg"
/// role_id : 1
/// state_id : null
/// zone_id : "1"
/// city_id : null

class Data {
  int _id;
  dynamic _name;
  dynamic _email;
  String _phoneNumber;
  dynamic _gender;
  dynamic _address;
  dynamic _city;
  dynamic _emailVerifiedAt;
  String _password;
  dynamic _rememberToken;
  String _createdAt;
  String _updatedAt;
  int _isPhoneVerified;
  String _isDefaultPassChanged;
  String _avatar;
  int _roleId;
  dynamic _stateId;
  dynamic _zoneId;
  dynamic _cityId;
  String  _zoneName;
  String  _cityName;

  int get id => _id;
  dynamic get name => _name;
  dynamic get email => _email;
  String get phoneNumber => _phoneNumber;
  dynamic get gender => _gender;
  dynamic get address => _address;
  dynamic get city => _city;
  dynamic get emailVerifiedAt => _emailVerifiedAt;
  String get password => _password;
  dynamic get rememberToken => _rememberToken;
  String get createdAt => _createdAt;
  String get updatedAt => _updatedAt;
  int get isPhoneVerified => _isPhoneVerified;
  String get isDefaultPassChanged => _isDefaultPassChanged;
  String get avatar => _avatar;
  int get roleId => _roleId;
  dynamic get stateId => _stateId;
  dynamic get zoneId => _zoneId;
  dynamic get cityId => _cityId;
  String get zoneName => _zoneName;
  String get cityName => _cityName;


  Data({
      int id, 
      dynamic name, 
      dynamic email, 
      String phoneNumber, 
      dynamic gender, 
      dynamic address, 
      dynamic city, 
      dynamic emailVerifiedAt, 
      String password, 
      dynamic rememberToken, 
      String createdAt, 
      String updatedAt, 
      int isPhoneVerified, 
      String isDefaultPassChanged, 
      String avatar, 
      int roleId, 
      dynamic stateId,
    dynamic zoneId,
      dynamic cityId,
    String zoneName,
    String cityName

  }){
    _id = id;
    _name = name;
    _email = email;
    _phoneNumber = phoneNumber;
    _gender = gender;
    _address = address;
    _city = city;
    _emailVerifiedAt = emailVerifiedAt;
    _password = password;
    _rememberToken = rememberToken;
    _createdAt = createdAt;
    _updatedAt = updatedAt;
    _isPhoneVerified = isPhoneVerified;
    _isDefaultPassChanged = isDefaultPassChanged;
    _avatar = avatar;
    _roleId = roleId;
    _stateId = stateId;
    _zoneId = zoneId;
    _cityId = cityId;
    _zoneName= zoneName;
    _cityName = cityName;
}

  Data.fromJson(dynamic json) {
    _id = json["id"];
    _name = json["name"];
    _email = json["email"];
    _phoneNumber = json["phone_number"];
    _gender = json["gender"];
    _address = json["address"];
    _city = json["city"];
    _emailVerifiedAt = json["email_verified_at"];
    _password = json["password"];
    _rememberToken = json["remember_token"];
    _createdAt = json["created_at"];
    _updatedAt = json["updated_at"];
    _isPhoneVerified = json["is_phone_verified"];
    _isDefaultPassChanged = json["is_default_pass_changed"];
    _avatar = json["avatar"];
    _roleId = json["role_id"];
    _stateId = json["state_id"];
    _zoneId = json["zone_id"];
    _cityId = json["city_id"];
    _zoneName = json["zone_name"];
    _cityName = json["city_name"];
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map["id"] = _id;
    map["name"] = _name;
    map["email"] = _email;
    map["phone_number"] = _phoneNumber;
    map["gender"] = _gender;
    map["address"] = _address;
    map["city"] = _city;
    map["email_verified_at"] = _emailVerifiedAt;
    map["password"] = _password;
    map["remember_token"] = _rememberToken;
    map["created_at"] = _createdAt;
    map["updated_at"] = _updatedAt;
    map["is_phone_verified"] = _isPhoneVerified;
    map["is_default_pass_changed"] = _isDefaultPassChanged;
    map["avatar"] = _avatar;
    map["role_id"] = _roleId;
    map["state_id"] = _stateId;
    map["zone_id"] = _zoneId;
    map["city_id"] = _cityId;
    map["zone_name"] = _zoneName;
    map["city_name"] = _cityName;
    return map;
  }
}