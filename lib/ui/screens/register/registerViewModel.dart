import 'package:rwandashop/core/model/authentication/final_reg.dart';
import 'package:rwandashop/core/model/authentication/register_phone_no.dart';
import 'package:rwandashop/core/model/authentication/resend_o_t_p.dart';
import 'package:rwandashop/core/model/authentication/reset_password_otp_response.dart';
import 'package:rwandashop/core/model/authentication/reset_password_request.dart';
import 'package:rwandashop/core/model/authentication/reset_password_update.dart';
import 'package:rwandashop/core/model/authentication/reset_password_verify_o_t_p.dart';
import 'package:rwandashop/core/model/authentication/verify_o_t_p.dart';
import 'package:rwandashop/core/model/authentication/verify_otp_response.dart';
import 'package:rwandashop/core/model/error_model.dart';
import 'package:rwandashop/core/model/success_model.dart';
import 'package:rwandashop/core/services/auth_service.dart';
import 'package:rwandashop/utils/baseModel.dart';
import 'package:rwandashop/utils/helpers.dart';
import 'package:rwandashop/utils/locator.dart';
import 'package:rwandashop/utils/router/navigationService.dart';
import 'package:rwandashop/utils/router/routeNames.dart';
import 'package:shared_preferences/shared_preferences.dart';

class RegisterViewModel extends BaseModel {
  final Authentication _authentication = locator<Authentication>();
  final NavigationService _navigationService = locator<NavigationService>();
  SharedPreferences preferences;
  int zoneId,cityId,sectorId,districtId;



  saveZoneAndCity(int zoneId,int districtId) async{
    preferences = await SharedPreferences.getInstance();
    preferences.setInt('zoneId', zoneId);
    preferences.setInt('cityId', districtId);
  }

  Future<int> getZoneId() async{
    preferences = await SharedPreferences.getInstance();
    return preferences.getInt('zoneId');
  }

  Future<int> getCityId() async{
    preferences = await SharedPreferences.getInstance();
    return preferences.getInt('cityId');
  }

  signUp(RegisterPhoneNo registerPhoneNo) async {
    setBusy(true);
    var result = await _authentication.registerPhone(registerPhoneNo.toJson());
    if (result is ErrorModel) {
      //setBusy(false);
      setBusy(false);
      showErrorToast(result.error);
      print(result.error);
      notifyListeners();
      return ErrorModel(result.error);
    }
    if (result is SuccessModel) {
      setBusy(false);

      //showToast(result.data['message']);
      _navigationService.navigateClearRouteTo(OTPRoute);
      //_user = result.data;

      notifyListeners();
      return SuccessModel(result.data);
    }
  }

  getPhone(String phone) async {
    _authentication.saveRegPhone(phone);
    notifyListeners();
  }

  verifyOtp(VerifyOTP verifyOTP) async {
    setBusy(true);
    var result = await _authentication.verifyOtp(verifyOTP.toJson());

    if (result is ErrorModel) {
      //setBusy(false);
      setBusy(false);
      showErrorToast(result.error);
      print(result.error);
      notifyListeners();
      return ErrorModel(result.error);
    }
    if (result is SuccessModel) {
      setBusy(false);
      //showToast(result.data['message']);
      VerifyOtpResponse verifyOtpResponse =
          VerifyOtpResponse.fromJson(result.data);
      getPhone(verifyOtpResponse.data.user.phoneNumber);
      print(verifyOtpResponse.message);
      _navigationService.navigateTo(EnterPasswordRoute);
      //_user = result.data;
      notifyListeners();
      return SuccessModel(verifyOtpResponse);
    }
  }

  resendOtp(ResendOTP resendOTP) async {
    setBusy(true);
    var result = await _authentication.resendOtp(resendOTP.toJson());

    if (result is ErrorModel) {
      //setBusy(false);
      setBusy(false);
      showErrorToast(result.error);
      print(result.error);
      notifyListeners();
      return ErrorModel(result.error);
    }
    if (result is SuccessModel) {
      setBusy(false);
      //showToast(result.data['message']);
      // _navigationService.navigateTo(EnterPasswordRoute);
      //_user = result.data;

      notifyListeners();
      return SuccessModel(result.data);
    }
  }

  finalReg(FinalReg finalReg) async {
    setBusy(true);
    var result = await _authentication.registrationFinal(finalReg.toJson());
    if (result is ErrorModel) {
      //setBusy(false);
      setBusy(false);
      showErrorToast(result.error);
      print(result.error);
      notifyListeners();
      return ErrorModel(result.error);
    }
    if (result is SuccessModel) {
      setBusy(false);
      // showToast(result.data['message']);
      showToast("Successful, Login with your credentials.");
      _navigationService.navigateTo(LoginRoute);
      //_user = result.data;
      notifyListeners();
      return SuccessModel(result.data);
    }
  }

  forgotPassword(ResetPasswordRequest resetPasswordRequest) async {
    setBusy(true);
    var result =
        await _authentication.resetRequest(resetPasswordRequest.toJson());

    if (result is ErrorModel) {
      //setBusy(false);
      setBusy(false);
      showErrorToast(result.error);
      print(result.error);
      notifyListeners();
      return ErrorModel(result.error);
    }
    if (result is SuccessModel) {
      setBusy(false);
      //showToast(result.data['message']);
      _navigationService.navigateTo(ForgotOTPRoute);
      //_user = result.data;
      notifyListeners();
      return SuccessModel(result.data);
    }
  }

  otpForgotPassword(ResetPasswordVerifyOTP resetPasswordVerifyOTP) async {
    setBusy(true);
    var result =
        await _authentication.resetOTP(resetPasswordVerifyOTP.toJson());

    if (result is ErrorModel) {
      //setBusy(false);
      setBusy(false);
      showErrorToast(result.error);
      print(result.error);
      notifyListeners();
      return ErrorModel(result.error);
    }
    if (result is SuccessModel) {
      setBusy(false);
      //showToast(result.data['message']);
      ResetPasswordOtpResponse resetPasswordOtpResponse =
          ResetPasswordOtpResponse.fromJson(result.data);
      print(resetPasswordOtpResponse.data.phoneNumber);
      getPhone(resetPasswordOtpResponse.data.phoneNumber);
      _navigationService.navigateTo(EnterPasswordFGRoute);
      //_user = result.data;
      notifyListeners();
      return SuccessModel(resetPasswordOtpResponse);
    }
  }

  changePassword(ResetPasswordUpdate resetPasswordUpdate) async {
    setBusy(true);
    var result =
        await _authentication.changePassword(resetPasswordUpdate.toJson());

    if (result is ErrorModel) {
      //setBusy(false);
      setBusy(false);
      showErrorToast(result.error);
      print(result.error);
      notifyListeners();
      return ErrorModel(result.error);
    }
    if (result is SuccessModel) {
      setBusy(false);
      //showToast(result.data['message']);
      _navigationService.navigateTo(LoginRoute);
      //_user = result.data;
      notifyListeners();
      return SuccessModel(result.data);
    }
  }

  getZone() async {
    var result = await _authentication.getZone();

    if (result is ErrorModel) {
      //setBusy(false);
      showErrorToast(result.error);
      print(result.error);
      notifyListeners();
      return ErrorModel(result.error);
    }
    if (result is SuccessModel) {
      //showToast(result.data['message']);

      //_user = result.data;

      //notifyListeners();
      return result.data['data'];
    }
  }

  getCity(String id) async {
    var result = await _authentication.getCity(id);

    if (result is ErrorModel) {
      //setBusy(false);
      showErrorToast(result.error);
      print(result.error);
      notifyListeners();
      return ErrorModel(result.error);
    }
    if (result is SuccessModel) {
      //showToast(result.data['message']);

      //_user = result.data;

      //notifyListeners();
      return result.data['data'];
    }
  }

  getDistrict(String id) async {
    var result = await _authentication.getDistrict(id);

    if (result is ErrorModel) {
      //setBusy(false);
      showErrorToast(result.error);
      print(result.error);
      notifyListeners();
      return ErrorModel(result.error);
    }
    if (result is SuccessModel) {
      //showToast(result.data['message']);

      //_user = result.data;

      //notifyListeners();
      return result.data['data'];
    }
  }
}
