import 'package:rwandashop/core/model/error_model.dart';
import 'package:rwandashop/core/model/product/order_details_model.dart' as details;
import 'package:rwandashop/core/model/product/orders.dart';
import 'package:rwandashop/core/model/success_model.dart';
import 'package:rwandashop/core/services/products_service.dart';
import 'package:rwandashop/utils/baseModel.dart';
import 'package:rwandashop/utils/helpers.dart';
import 'package:rwandashop/utils/locator.dart';
import 'package:rwandashop/utils/router/navigationService.dart';

class OrderViewModel extends BaseModel {
  final Product _product = locator<Product>();
  final NavigationService _navigationService = locator<NavigationService>();
  Data orders;
  bool loading;

  Future<List<Data>> getOrderOnGoing() async {
    var result = await _product.getOrderOngoing();
    if (result is ErrorModel) {
      showErrorToast(result.error);
      print(result.error);
      notifyListeners();
      throw Exception('Failed to load internet');
      //return ErrorModel(result.error);
    }
    print('HDHDHL:::::::::::::::::::::::');
    print(result);
    return result;
     // notifyListeners();
      // return categories;
    }

  Future<List<Data>> getOrderHistory() async {

    var result = await _product.getOrderHistory();
    if (result is ErrorModel) {

      showErrorToast(result.error);
      print(result.error);
      notifyListeners();
      throw Exception('Failed to load internet');
      //return ErrorModel(result.error);
    }
    print('HDHDHL:::::::::::::::::::::::');
    print(result);
    return result;
    // notifyListeners();
    // return categories;
  }
  Future<List<details.OrdersDetails>> getOrderDetails(String id) async {
    var result = await _product.getDetails(id);
    if (result is ErrorModel) {

      showErrorToast(result.error);
      print(result.error);
      notifyListeners();
      throw Exception('Failed to load internet');
      //return ErrorModel(result.error);
    }
    print('HDHDHL:::::::::::::::::::::::');
    print(result);
    return result;
    // notifyListeners();
    // return categories;
  }


}
