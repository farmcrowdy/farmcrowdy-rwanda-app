/// status : true
/// message : "Zones retrieved successfully."
/// data : [{"id":1,"name":"Kigali","created_at":null,"updated_at":null,"active":null},{"id":2,"name":"The Northern Province","created_at":null,"updated_at":null,"active":null},{"id":3,"name":"The Eastern Province","created_at":null,"updated_at":null,"active":null},{"id":4,"name":"The Southern Province","created_at":null,"updated_at":null,"active":null},{"id":5,"name":"The Western Province","created_at":null,"updated_at":null,"active":null}]

class Zones {
  bool _status;
  String _message;
  List<Data> _data;

  bool get status => _status;
  String get message => _message;
  List<Data> get data => _data;

  Zones({
      bool status, 
      String message, 
      List<Data> data}){
    _status = status;
    _message = message;
    _data = data;
}

  Zones.fromJson(dynamic json) {
    _status = json["status"];
    _message = json["message"];
    if (json["data"] != null) {
      _data = [];
      json["data"].forEach((v) {
        _data.add(Data.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map["status"] = _status;
    map["message"] = _message;
    if (_data != null) {
      map["data"] = _data.map((v) => v.toJson()).toList();
    }
    return map;
  }

}

/// id : 1
/// name : "Kigali"
/// created_at : null
/// updated_at : null
/// active : null

class Data {
  int _id;
  String _name;
  dynamic _createdAt;
  dynamic _updatedAt;
  dynamic _active;

  int get id => _id;
  String get name => _name;
  dynamic get createdAt => _createdAt;
  dynamic get updatedAt => _updatedAt;
  dynamic get active => _active;

  Data({
      int id, 
      String name, 
      dynamic createdAt, 
      dynamic updatedAt, 
      dynamic active}){
    _id = id;
    _name = name;
    _createdAt = createdAt;
    _updatedAt = updatedAt;
    _active = active;
}

  Data.fromJson(dynamic json) {
    _id = json["id"];
    _name = json["name"];
    _createdAt = json["created_at"];
    _updatedAt = json["updated_at"];
    _active = json["active"];
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map["id"] = _id;
    map["name"] = _name;
    map["created_at"] = _createdAt;
    map["updated_at"] = _updatedAt;
    map["active"] = _active;
    return map;
  }

}