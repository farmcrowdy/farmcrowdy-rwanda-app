import 'package:badges/badges.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:provider_architecture/provider_architecture.dart';
import 'package:rwandashop/core/model/product/products.dart';
import 'package:rwandashop/core/model/product/products.dart' as product;
import 'package:rwandashop/core/services/analytics_service.dart';
import 'package:rwandashop/core/services/auth_service.dart';
import 'package:rwandashop/core/services/products_service.dart';
import 'package:rwandashop/ui/screens/package_details/package_details_view_model.dart';
import 'package:rwandashop/ui/widget/package_widget.dart';
import 'package:rwandashop/utils/app_localizations.dart';
import 'package:rwandashop/utils/colors.dart';
import 'package:rwandashop/utils/locator.dart';
import 'package:rwandashop/utils/router/navigationService.dart';
import 'package:rwandashop/utils/router/routeNames.dart';
import 'package:rwandashop/utils/screensize.dart';
import 'package:shimmer/shimmer.dart';

class PackageDetailsPage extends StatefulWidget {
  final Data packageDetailModel;
  final String categoryId;
  final String screenName;

  const PackageDetailsPage({Key key, this.packageDetailModel,this.screenName, this.categoryId});

  @override
  ProductDetailsPageState createState() => new ProductDetailsPageState();
}

final Authentication _authentication = locator<Authentication>();

class ProductDetailsPageState extends State<PackageDetailsPage> {
  TextEditingController numberController = TextEditingController();
  final oCcy = new NumberFormat("#,##0.00", "en_US");
  final NavigationService _navigationService = locator<NavigationService>();
  bool passwordVisible;
  String gender = 'Select Zone';
  final Product _product = locator<Product>();
  final Analytics _analytics = locator<Analytics>();

  @override
  void initState() {
    super.initState();
    passwordVisible = true;
  }

  @override
  Widget build(BuildContext context) {
    AppLocalizations tr = AppLocalizations.of(context);
    return ViewModelProvider<PackageDetailsViewModel>.withConsumer(
        onModelReady: (model) {
          _analytics.sendAnalyticsItemSelected(
              widget.packageDetailModel.id.toString());
          _analytics.setCurrentScreen(
              'Item details', 'This gives full details of an item');
          model.getSinglePackage(widget.packageDetailModel.id.toString());
        },
        viewModelBuilder: () => PackageDetailsViewModel(),
        builder: (context, model, child) {
          return Scaffold(
              body: SingleChildScrollView(
            child: Column(children: <Widget>[
              SizedBox(
                height: 50,
              ),
              Container(
                padding: EdgeInsets.only(left: 20.w, right: 20.w),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    InkWell(
                      onTap: () {
                        _navigationService.pop();
                      },
                      child: Icon(Icons.arrow_back_ios),
                    ),
                    _product.cartSummary.isNull
                        ? Container()
                        : InkWell(
                            onTap: () {
                              _navigationService.navigateTo(CartScreenRoute);
                            },
                      child: Container(
                        height: 20.h,
                        width: 20.w,
                        child: Image.asset('assets/images/saveIconGrey.png'),
                      ),
                    ),
                            // child: Badge(
                            //   borderRadius: BorderRadius.circular(5.r),
                            //   badgeColor: AppColors.themeGreen,
                            //   badgeContent: Text(
                            //     _product.cartSummary.data.basketSize.toString(),
                            //     style: TextStyle(
                            //         color: Colors.white,
                            //         fontWeight: FontWeight.bold),
                            //   ),
                            //   child: Container(
                            //     height: 20.h,
                            //     width: 20.w,
                            //     child: Image.asset('assets/images/cart.png'),
                            //   ),
                            // ),
                          // )
                  ],
                ),
              ),
              SizedBox(
                height: 10,
              ),
              Container(
                padding: EdgeInsets.all(20),
                child: Container(
                  height: 250.w,
                  decoration: BoxDecoration(
                    image: DecorationImage(
                        image: CachedNetworkImageProvider(
                            widget.packageDetailModel.image),
                        fit: BoxFit.contain),
                  ),
                ),
              ),
              // Container(
              //   padding: EdgeInsets.only(left: 20.w, right: 20.w),
              //   child: Row(
              //       mainAxisAlignment: MainAxisAlignment.spaceBetween,
              //       children: [
              //         Flexible(
              //           child: Text( widget.packageDetailModel.createdAt != null ?
              //             widget.packageDetailModel.createdAt.substring(0,11): '',
              //             style: TextStyle(
              //                 fontSize: 12.ssp,
              //                 color: Colors.green,
              //                 fontWeight: FontWeight.w400),
              //           ),
              //         ),
              //         Text(''),
              //       ]),
              // ),
              SizedBox(
                height: 20.h,
              ),

              Container(
                padding: EdgeInsets.only(left: 20.w, right: 20.w),
                width: Responsive.width(context),
                child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Container(
                        width: Responsive.width(context) * 0.5,
                        child: Text(
                          widget.packageDetailModel.name,
                          style: TextStyle(
                              fontSize: 15.ssp,
                              color: Colors.black,
                              fontWeight: FontWeight.w800),
                        ),
                      ),
                      Container(
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            widget.packageDetailModel.discount != 0 &&
                                    widget.packageDetailModel.discount != null
                                ? Text(
                                    widget.packageDetailModel.price != null
                                        ? 'RF ${oCcy.format(((widget.packageDetailModel.price) - ((widget.packageDetailModel.price * widget.packageDetailModel.discount) / 100)))}'
                                        : '',
                                    style: TextStyle(
                                      fontWeight: FontWeight.bold,
                                      fontSize: 11.0.ssp,
                                      color: Colors.black,
                                    ),
                                    overflow: TextOverflow.ellipsis,
                                    softWrap: false,
                                    maxLines: 1,
                                  )
                                : Text(
                                    widget.packageDetailModel.price != null
                                        ? 'RF ${oCcy.format((widget.packageDetailModel.price))}'
                                        : '',
                                    style: TextStyle(
                                        fontSize: 11.ssp,
                                        color: AppColors.themeGreen,
                                        fontWeight: FontWeight.w900),
                                  ),
                          ],
                        ),
                      ),
                    ]),
              ),

              SizedBox(
                height: 20.h,
              ),

              Container(
                padding: EdgeInsets.only(left: 20.w, right: 20.w),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Icon(
                          Icons.assignment_return,
                          size: 18,
                          color: Colors.grey.withOpacity(0.6),
                        ),
                        Text(
                          widget.packageDetailModel.company.name != null ?
                          widget.packageDetailModel.company.name : '',
                          style: TextStyle(
                              fontSize: 10.ssp,
                              color: Colors.grey.withOpacity(0.9),
                              fontWeight: FontWeight.w300),
                        ),
                        SizedBox(width: 20),
                        Icon(
                          Icons.location_on_rounded,
                          size: 18,
                          color: Colors.grey.withOpacity(0.6),
                        ),
                        Text(
                          widget.packageDetailModel.company.user.address != null ?
                          widget.packageDetailModel.company.user.address: '',
                          style: TextStyle(
                              fontSize: 10.ssp,
                              color: Colors.grey.withOpacity(0.9),
                              fontWeight: FontWeight.w300),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
              Divider(
                thickness: 1,
                endIndent: 20,
                indent: 20,
              ),
              SizedBox(
                height: 20.h,
              ),
              Container(
                padding: EdgeInsets.only(left: 20.w, right: 20.w),
                child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(
                        tr.text('packageDetails.productDetail'),
                        style: TextStyle(
                            fontSize: 12.ssp, fontWeight: FontWeight.bold),
                      ),
                      Text(
                        '',
                        style: TextStyle(
                            fontSize: 16.ssp, fontWeight: FontWeight.w500),
                      ),
                    ]),
              ),
              Container(
                width: double.infinity,
                padding: EdgeInsets.only(left: 20.w, right: 20.w),
                child: (widget.packageDetailModel.description != '' &&
                        widget.packageDetailModel.description != null)
                    ? Text(
                        widget.packageDetailModel.description,
                        style: TextStyle(
                            fontSize: 13.ssp, color: Color(0xff7C7C7C)),
                      )
                    : Container(),
              ),
              SizedBox(
                height: 30.h,
              ),
              Container(
                  padding:
                      EdgeInsets.only(left: 10.w, right: 10.w, bottom: 20.w),
                  child: Container(
                      height: 50.h,
                      width: Responsive.width(context),
                      child: RaisedButton(
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.all(Radius.circular(15)),
                            side: BorderSide(color: AppColors.themeGreen),
                          ),
                          color: AppColors.themeGreen,
                          onPressed: () {
                            model.callStore();
                            //  onPressed();
                          },
                          // },
                          child: Center(
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Icon(
                                  Icons.call,
                                  color: Colors.white,
                                ),
                                SizedBox(
                                  width: 5,
                                ),
                                Text('Call',
                                    style: TextStyle(
                                        fontSize: 15.ssp,
                                        color: Colors.white,
                                        fontWeight: FontWeight.bold))
                              ],
                            ),
                          )))),
              SizedBox(height: 10,),
              Divider(height: 1,),
              SizedBox(
                height: 20.h,
              ),
              widget.screenName != 'savedScreen' ?
              Column(
                children: [
                  Align(
                    alignment: Alignment.centerLeft,
                    child: Container(
                      margin: EdgeInsets.only(left: 20),
                      child: Text(
                        tr.text('shop.similarProducts'),
                        style: TextStyle(
                            fontSize: 16.ssp,
                            color: Colors.black,
                            fontWeight: FontWeight.w800),
                      ),
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.all(10),
                    child: FutureBuilder<List<product.Data>>(
                        future: model.getItem(widget.categoryId),
                        builder: (context, snapshot) {
                          if(snapshot.hasData)
                            if (snapshot.data.isNotEmpty) {
                              return ListView.builder(
                                  shrinkWrap: true,
                                  physics: NeverScrollableScrollPhysics(),
                                  itemCount: 5, //snapshot.data.length,
                                  itemBuilder: (context, index) {
                                    return Card(
                                      elevation: 2,
                                      child: Container(
                                        child: PackageWidget(
                                          packageModel: snapshot.data[index],
                                          model: model,
                                          categoryId: widget.categoryId,
                                        ),
                                      ),
                                    );
                                  });
                            }
                          return Container(
                            margin: EdgeInsets.only(top: 20),
                            child: Center(
                              child: CircularProgressIndicator(backgroundColor: AppColors.green,),
                            ),
                          );
                          // if (!snapshot.hasData) {
                          //   return Container(
                          //       padding: EdgeInsets.all(20.0),
                          //       child: Center(
                          //         child: Shimmer.fromColors(
                          //             direction: ShimmerDirection.ltr,
                          //             period: Duration(seconds: 2),
                          //             child: ListView.builder(
                          //                 itemCount: 6,
                          //                 shrinkWrap: true,
                          //                 itemBuilder: (context, index) {
                          //                   return Padding(
                          //                       padding: const EdgeInsets.only(
                          //                           bottom: 8.0),
                          //                       // child: PackageWidget(
                          //                       //   model: model,
                          //                       // )
                          //                   );
                          //                 }),
                          //             baseColor: AppColors.grey,
                          //             highlightColor: Colors.white),
                          //       ));
                          // } else if (snapshot.data.isNotEmpty) {
                          //   return ListView.builder(
                          //       shrinkWrap: true,
                          //       itemCount: snapshot.data.length,
                          //       itemBuilder: (context, index) {
                          //         return Card(
                          //           elevation: 1,
                          //           child: Container(
                          //             child: PackageWidget(
                          //               packageModel: snapshot.data[index],
                          //               model: model,
                          //             ),
                          //           ),
                          //         );
                          //       });
                          // } else if (snapshot.hasError) {
                          //   return Column(
                          //     children: <Widget>[
                          //       SizedBox(
                          //         height: 10,
                          //       ),
                          //       Text(
                          //         'Error fetching data',
                          //         style: TextStyle(
                          //           fontWeight: FontWeight.bold,
                          //           fontSize: 20,
                          //         ),
                          //       ),
                          //       SizedBox(
                          //         height: 10,
                          //       ),
                          //       Text("${'Make sure you are connected'}"),
                          //       SizedBox(
                          //         height: 100,
                          //       ),
                          //     ],
                          //   );
                          // } else {
                          //   return Text(
                          //     'Database Empty',
                          //     style: TextStyle(
                          //       fontWeight: FontWeight.bold,
                          //       fontSize: 20,
                          //     ),
                          //   );
                          // }
                        }),
                  )
                ],
              )
             : Container(),
            ]),
          ));
        });
  }
}
