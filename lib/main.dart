import 'dart:convert';
import 'dart:io';
import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:firebase_analytics/observer.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:provider/provider.dart';
import 'package:rwandashop/core/services/language_services.dart';
import 'package:rwandashop/ui/screens/splashscreen.dart';
import 'package:rwandashop/utils/app_localizations.dart';
import 'package:rwandashop/utils/colors.dart';
import 'package:rwandashop/utils/locator.dart';
import 'package:rwandashop/utils/progressBarManager/dialogManager.dart';
import 'package:rwandashop/utils/progressBarManager/dialogService.dart';
import 'package:rwandashop/utils/router/navigationService.dart';
import 'package:rwandashop/utils/router/router.dart';
import 'package:shared_preferences/shared_preferences.dart';

// This call the language service so the users preferred language is used
final LanguageService languageService = locator<LanguageService>();
void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  // To initialise the locator
  setupLocator();
  await languageService.setLocale();
  runApp(MyApp());

}

class MyApp extends StatefulWidget {
  static void setLocale(BuildContext context, Locale newLocale) {
    _MyAppState state = context.findAncestorStateOfType<_MyAppState>();
    state.setLocale(newLocale);
  }
  @override
  _MyAppState createState() => _MyAppState();
}
// ignore: must_be_immutable
class _MyAppState extends State<MyApp> {

  Locale _locale;
  setLocale(Locale locale) {
    setState(() {
      _locale = locale;
    });
  }

  getLocale() async {
    var prefs = await SharedPreferences.getInstance();
    String languageCode =  prefs.getString('language_code') ?? 'ky';
    return Locale(languageCode);
  }

  // @override
  // void didChangeDependencies() {
  //   getLocale().then((locale) {
  //     setState(() {
  //       this._locale = locale;
  //     });
  //   });
  //   super.didChangeDependencies();
  // }

  final LanguageService languageProvider = locator<LanguageService>();
  // This is to setup FCM notifications
  final FirebaseMessaging firebaseMessaging = FirebaseMessaging();
  FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin = new FlutterLocalNotificationsPlugin();

  void registerNotification() async {
    var initializationSettings = InitializationSettings();
    flutterLocalNotificationsPlugin.initialize(initializationSettings);

    firebaseMessaging.requestNotificationPermissions();

    firebaseMessaging.configure(onMessage: (Map<String, dynamic> message) {
      print('onMessage: $message');
      Get.snackbar(
        '${message['notification']['body']}',
        '${message['notification']['title']}',
        backgroundColor: AppColors.green,
      );
      showNotification(message['notification']);
      return;
    }, onResume: (Map<String, dynamic> message) {
      print('onResume: $message');
      showNotification(message['notification']);
      return;
    }, onLaunch: (Map<String, dynamic> message) {
      showNotification(message['notification']);
      print('onLaunch: $message');
      return;
    });
  }

  void showNotification(message) async {
    var androidPlatformChannelSpecifics = new AndroidNotificationDetails(
      Platform.isAndroid
          ? 'FLUTTER_NOTIFICATION_CLICK'
          : 'FLUTTER_NOTIFICATION_CLICK',
      'Flutter chat demo',
      'your channel description',
      playSound: true,
      enableVibration: true,
      importance: Importance.max,
      priority: Priority.high,
    );
    // new part
    if (Platform.isIOS) {
      this.firebaseMessaging.configure();
      this.firebaseMessaging.requestNotificationPermissions(
            IosNotificationSettings(sound: true, badge: true, alert: true),
          );
    }

    var iOSPlatformChannelSpecifics = new IOSNotificationDetails(
      presentSound: true,
      presentBadge: true,
    );
    var platformChannelSpecifics = new NotificationDetails(
        android: androidPlatformChannelSpecifics,
        iOS: iOSPlatformChannelSpecifics);
    await flutterLocalNotificationsPlugin.show(0, message['title'].toString(),
        message['body'].toString(), platformChannelSpecifics,
        payload: json.encode(message));
  }

  static FirebaseAnalytics analytics = FirebaseAnalytics();
  static FirebaseAnalyticsObserver observer =
      FirebaseAnalyticsObserver(analytics: analytics);

  void initState() {
    // TODO: implement initState
    registerNotification();
    // super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return ScreenUtilInit(
        // designSize: Size(360, 690),
        allowFontScaling: true,
        builder: () => GetMaterialApp(
              debugShowCheckedModeBanner: false,
              title: 'Rwanda MarketPlace',
              builder: (context, child) => Navigator(
                key: locator<ProgressService>().progressNavigationKey,
                onGenerateRoute: (settings) =>
                    MaterialPageRoute(builder: (context) {
                  return ProgressManager(child: child);
                  //DialogManager(child: child);
                }),
              ),
              theme: ThemeData(
                textTheme: GoogleFonts.robotoTextTheme(
                  Theme.of(context).textTheme,
                ),
              ),
          locale: _locale,
          supportedLocales: AppLocalizations.supportedLocales(),
          localizationsDelegates: [
            AppLocalizations.delegate,
            GlobalMaterialLocalizations.delegate,
            GlobalWidgetsLocalizations.delegate
          ],
              navigatorObservers: <NavigatorObserver>[observer],
              navigatorKey: locator<NavigationService>().navigationKey,
              home: AnimatedSplashScreen(),
              onGenerateRoute: generateRoute,
            ));
  }
}
