import 'dart:math';
import 'package:flutter/material.dart';
import 'package:rwandashop/core/model/error_model.dart';
import 'package:rwandashop/core/model/product/catergories.dart';
import 'package:rwandashop/core/model/product/search.dart';
import 'package:rwandashop/core/model/success_model.dart';
import 'package:rwandashop/core/services/auth_service.dart';
import 'package:rwandashop/core/services/products_service.dart';
import 'package:rwandashop/utils/baseModel.dart';
import 'package:rwandashop/utils/helpers.dart';
import 'package:rwandashop/utils/locator.dart';
import 'package:rwandashop/utils/router/navigationService.dart';

class ProductViewModel extends BaseModel {
  final Product _product = locator<Product>();
  final NavigationService _navigationService = locator<NavigationService>();
  final Authentication _authentication = locator<Authentication>();
  String _searchName;
  String _districtName;
  String get districtName => _districtName;
  String get searchName => _searchName;



  List colors = [
    Color(0xffA3D2CA),
    Color(0xffD5DBDB),
    Color(0xffE8DCCE),
  ];
  Random random = new Random();

  int randomColor = 0;

  changeIndex(){
    randomColor = random.nextInt(3);
    return colors[randomColor];
  }
  Future<List<Categories>> getMerchant() async {
    //setBusy(true);
    var result = await _product.getCategories();
    if (result is ErrorModel) {
      // showToast('Login failed');
      print(result.error);
      notifyListeners();
      throw Exception('Failed to load internet');
      //return ErrorModel(result.error);
    }
    print('Merchant:::::::::::::::::::::::  $result');
    print(result);
    return result;
  }

  Future<List<Categories>> getMerchantByDistrict(String districtName) async {
    //setBusy(true);
    var result = await _product.getCategoriesByDistrict(districtName);
    if (result is ErrorModel) {
      // showToast('Login failed');
      print(result.error);
      notifyListeners();
      throw Exception('Failed to load internet');
      //return ErrorModel(result.error);
    }

    print('MerchantSearchedByDistrict:::::::::::::::::::::::  $districtName');
    print('result: ${result.toString()}');
    return result;
  }



  Future<List<Categories>> getSearchedMerchant(String val) async {
    //setBusy(true);
    var result = await _product.getSearchedMerchant(val);
    if (result is ErrorModel) {
      // showToast('Login failed');
      print(result.error);
      notifyListeners();
      throw Exception('Failed to load internet');
      //return ErrorModel(result.error);
    }
    print('HDHDHL:::::::::::::::::::::::');
    print(result);
    return result;
  }

  Future<List<Categories>> getSearchMerchantByDistrict(String districtNameString) async {
    //setBusy(true);
    var result = await _product.getSearchedMerchantByDistrict(districtNameString);
    if (result is ErrorModel) {
      // showToast('Login failed');
      print(result.error);
      notifyListeners();
      throw Exception('Failed to load internet');
      //return ErrorModel(result.error);
    }
    // _districtName = districtNameString;
    // notifyListeners();
    print('HDHDHL:::::::::::::::::::::::');
    print(result);
    return result;
  }

  initiateSearch(String val,ProductViewModel model) {
    if (val.length > 1 && val.isNotEmpty) {

      model.getSearchedMerchant(val);
      notifyListeners();
      _searchName = val;

    } else {
      notifyListeners();
      _searchName = null;
    }
  }

  initialFilter(String val,ProductViewModel model) {
    if (val.length > 1 && val.isNotEmpty) {
      model.getSearchedMerchant(val);
      notifyListeners();
      _districtName = val;
    } else {
      notifyListeners();
      _districtName = null;
    }
  }

  getZone() async {
    var result = await _authentication.getZone();

    if (result is ErrorModel) {
      //setBusy(false);
      showErrorToast(result.error);
      print(result.error);
      notifyListeners();
      return ErrorModel(result.error);
    }
    if (result is SuccessModel) {
      //showToast(result.data['message']);

      //_user = result.data;

      //notifyListeners();
      print('''''''''''''');
      print(result.data);
      return result.data['data'];
    }
  }

  getCity(String id) async {
    var result = await _authentication.getCity(id);

    if (result is ErrorModel) {
      //setBusy(false);
      showErrorToast(result.error);
      print(result.error);
      notifyListeners();
      return ErrorModel(result.error);
    }
    if (result is SuccessModel) {
      //showToast(result.data['message']);

      //_user = result.data;

      //notifyListeners();
      return result.data['data'];
    }
  }

  getDistrict(String id) async {
    // setBusy(true);
    var result = await _authentication.getDistrict(id);

    if (result is ErrorModel) {
      // setBusy(false);
      showErrorToast(result.error);
      print(result.error);
      notifyListeners();
      return ErrorModel(result.error);
    }
    if (result is SuccessModel) {
      // setBusy(false);
      //showToast(result.data['message']);

      //_user = result.data;

      //notifyListeners();
      return result.data['data'];
    }
  }

}
