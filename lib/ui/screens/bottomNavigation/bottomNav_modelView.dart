import 'package:rwandashop/utils/baseModel.dart';


class AppStateProvider extends BaseModel {

  int _currentTabIndex = 0; // Defaults to chat tab

  int get currentTabIndex => _currentTabIndex;

  void setCurrentTabTo({int newTabIndex}) {
    _currentTabIndex = newTabIndex;
    notifyListeners();
  }

}
