import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_app_dialog/flutter_app_dialog.dart';
import 'package:provider_architecture/provider_architecture.dart';
import 'package:rwandashop/core/model/product/catergories.dart';
import 'package:rwandashop/core/services/analytics_service.dart';
import 'package:rwandashop/ui/screens/product/product_view_model.dart';
import 'package:rwandashop/ui/widget/merchant_widget.dart';
import 'package:rwandashop/utils/app_localizations.dart';
import 'package:rwandashop/utils/colors.dart';
import 'package:rwandashop/utils/locator.dart';
import 'package:rwandashop/utils/router/navigationService.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:shimmer/shimmer.dart';

class Product extends StatefulWidget {
  @override
  ProductState createState() => new ProductState();
}

class ProductState extends State<Product> {
  TextEditingController numberController = TextEditingController();
  final Analytics _analytics = locator<Analytics>();
  final NavigationService _navigationService = locator<NavigationService>();
  StateSetter _setState;
  String searchItem;
  List zone = List();
  List city = List();
  List district = List();
  int zoneId;
  int districtId;
  int cityId;
  String districtName;

  getZone(ProductViewModel model) async {
    print('ddd');
    var zones = await model.getZone();
    print(zones.runtimeType);
    setState(() {
      zone.clear();
      zone = zones;
      print('ddd:::::::::::::::::::::');
      print(zone);
    });
  }

  getCity(ProductViewModel model, String id) async {
    print('city: $id');
    var cities = await model.getCity(id);
    _setState(() {
      city = cities;
      print('ddd:::::::::::::::::::::');
      print(city);
    });
  }

  getDistrict(ProductViewModel model, String id) async {
    var districts = await model.getDistrict(id); //'7'
    _setState(() {
      district = districts;
      print('dddf:::::::::::::::::::::');
      print(district);
    });
  }

  setDistrictName(String districtNameString){
    _setState((){
      districtName = districtNameString ;
    }
    );
    print('districtName $districtName') ;
  }

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    AppLocalizations tr = AppLocalizations.of(context);

    return ViewModelProvider<ProductViewModel>.withConsumer(
        onModelReady: (model) {
          _analytics.setCurrentScreen(
              'Categories screen', 'List all the categories available');
        },
        viewModelBuilder: () => ProductViewModel(),
        builder: (context, model, child) {
          if(zone.isEmpty){
            getZone(model);
          }

          return Scaffold(
            body: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  SizedBox(
                    height: 60.h,
                  ),
                  Container(
                    padding: EdgeInsets.only(left: 20.w, right: 20.w),
                    child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          InkWell(
                            onTap: () {
                              _navigationService.pop();
                            },
                            child: Icon(
                              Icons.arrow_back_ios,
                              color: Colors.black,
                            ),
                          ),
                          Container(
                            child: Text(
                              tr.text('shop.merchants'),
                              style: TextStyle(
                                  fontSize: 18.ssp,
                                  fontWeight: FontWeight.bold),
                            ),
                          ),
                          Container(
                            child: Text(
                              '',
                              style: TextStyle(
                                  fontSize: 18.ssp,
                                  fontWeight: FontWeight.bold),
                            ),
                          ),
                        ]),
                  ),
                  SizedBox(height: 20.h),
                  Row(
                    children: [
                      Expanded(
                        child: Container(
                          padding: EdgeInsets.only(left: 20, right: 5),
                          child: TextField(
                            decoration: InputDecoration(
                              focusedBorder: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(15.0.r),
                                  borderSide: BorderSide(
                                      color: Color(0xffF2F3F2),
                                      width: 0.0,
                                      style: BorderStyle.none)),
                              enabledBorder: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(15.0),
                                  borderSide: BorderSide(
                                      color: Color(0xffF2F3F2),
                                      width: 0.0,
                                      style: BorderStyle.none)),
                              errorBorder: InputBorder.none,
                              disabledBorder: InputBorder.none,
                              fillColor: Color(0xffF2F3F2),
                              filled: true,
                              border: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(15.0),
                                  borderSide: BorderSide(
                                      color: Color(0xffF2F3F2),
                                      width: 0.0,
                                      style: BorderStyle.none)),
                              hintText: tr.text('shop.search'),
                              hintStyle: TextStyle(
                                  color: Color(0xff7C7C7C),
                                  fontSize: 14,
                                  fontWeight: FontWeight.w500),
                              contentPadding: EdgeInsets.fromLTRB(10.0, 10.0, 10.0, 10.0),
                              prefixIcon: Icon(
                                Icons.search,
                                color: Colors.black,
                              ),
                            ),
                            textInputAction: TextInputAction.search,
                            onSubmitted: (value) {
                              // model.searchUser(context, value);
                              // model.searchGroup(context, value);
                            },
                            onChanged: (value) {
                              model.initiateSearch(value, model);
                              // model.searchGroup(context, value);
                            },
                          ),
                        ),
                      ),
                      IconButton(icon: Icon(Icons.filter_list_outlined),
                      onPressed: (){
                        showDialog(
                            context: context,
                            builder: (BuildContext context) {
                              return StatefulBuilder(
                                  builder: (BuildContext context, StateSetter setState){
                                    _setState = setState;
                                    return BaseDialogWidget(
                                        child: Container(
                                          decoration: BoxDecoration(
                                            borderRadius: BorderRadius.circular(25),
                                          ),
                                          height:MediaQuery.of(context).size.height * .35,
                                          child: SingleChildScrollView(
                                            child: Stack(
                                              children: [
                                                Column(
                                                  children: [
                                                    Container(
                                                      padding: EdgeInsets.fromLTRB(10, 15, 10, 20),
                                                      child: Text('Filter Merchant',style: TextStyle(
                                                          fontSize: 21,
                                                          fontWeight: FontWeight.w600
                                                      ),),
                                                    ),
                                                    Container(
                                                        padding:
                                                        EdgeInsets.only(left: 20.w, right: 20.w),
                                                        child: Center(
                                                          child:Container(
                                                            padding: EdgeInsets.all(15),
                                                            decoration: BoxDecoration(
                                                                color: Color(0xffEEEEEE),
                                                                border:
                                                                Border.all(width: 0, color: Colors.white),
                                                                borderRadius: BorderRadius.circular(25)),
                                                            child: new DropdownButtonFormField(
                                                              decoration: InputDecoration.collapsed(
                                                                  hintText: 'Select Zone'),
                                                              isExpanded: true,

                                                              validator: (value) =>
                                                              value == null ? '' : null,
                                                              items: zone.map((item) {
                                                                return new DropdownMenuItem(
                                                                  child: new Text(item['name']),
                                                                  value: item,
                                                                );
                                                              }).toList(),
                                                              onChanged: (newVal) {
                                                                _setState(() {
                                                                  zoneId = newVal['id'];
                                                                  print('zoneId: $zoneId');
                                                                  city.clear();
                                                                  district.clear();
                                                                  getCity(model, '$zoneId');
                                                                });
                                                              },
                                                              //value: unitPrice,
                                                            ),
                                                          ),
                                                        )
                                                    ),
                                                    SizedBox(height: 20,),
                                                    Container(
                                                        padding:
                                                        EdgeInsets.only(left: 20.w, right: 20.w),
                                                        child: Center(
                                                          child: Container(
                                                            padding: EdgeInsets.all(15),
                                                            decoration: BoxDecoration(
                                                                color: Color(0xffEEEEEE),
                                                                border:
                                                                Border.all(width: 0, color: Colors.white),
                                                                borderRadius: BorderRadius.circular(25)),
                                                            child: new DropdownButtonFormField(
                                                              decoration: InputDecoration.collapsed(
                                                                  hintText: 'Select city'),
                                                              isExpanded: true,

                                                              validator: (value) =>
                                                              value == null ? '' : null,
                                                              items: city.map((item) {
                                                                return new DropdownMenuItem(
                                                                  child: new Text(item['name']),
                                                                  value: item,
                                                                );
                                                              }).toList(),
                                                              onChanged: (newVal) {
                                                                _setState(() {
                                                                  cityId = newVal['id'];
                                                                  print('cityId $cityId');
                                                                  district.clear();
                                                                  getDistrict(model, '$cityId');
                                                                });
                                                              },
                                                              //value: unitPrice,
                                                            ),
                                                          ),
                                                        )
                                                    ),
                                                    SizedBox(height: 20,),
                                                    Container(
                                                        padding:
                                                        EdgeInsets.only(left: 20.w, right: 20.w),
                                                        child: Center(
                                                          child: Container(
                                                            padding: EdgeInsets.all(15),
                                                            decoration: BoxDecoration(
                                                                color: Color(0xffEEEEEE),
                                                                border:
                                                                Border.all(width: 0, color: Colors.white),
                                                                borderRadius: BorderRadius.circular(25)),
                                                            child: new DropdownButtonFormField(
                                                              decoration: InputDecoration.collapsed(
                                                                  hintText: 'Select District'),
                                                              isExpanded: true,
                                                              validator: (value) =>
                                                              value == null ? '' : null,
                                                              items: district.map((item) {
                                                                return new DropdownMenuItem(
                                                                  child: new Text(item['name']),
                                                                  value: item,
                                                                );
                                                              }).toList(),
                                                              onChanged: (newVal) {
                                                                _setState(() {
                                                                  districtName = newVal['name'];
                                                                  print('districtName1: $districtName');
                                                                  model.initialFilter(districtName, model);
                                                                  Navigator.pop(context);
                                                                });
                                                              },
                                                              //value: unitPrice,
                                                            ),
                                                          ),
                                                        )
                                                    ),

                                                  ],
                                                ),

                                                // Container(
                                                //   color: Colors.white10.withOpacity(0.2),
                                                //   child: Center(
                                                //     child: CircularProgressIndicator(),
                                                //   ),
                                                // )

                                              ],
                                            ),
                                          ),
                                        )
                                    );
                                  }

                              );
                            });
                      },)
                    ],
                  ),
                  SizedBox(
                    height: 30,
                  ),
                  model.searchName == null
                      ?
                  Expanded(
                    child: FutureBuilder<List<Categories>>(
                        future: model.districtName == null ? model.getMerchant() : model.getMerchantByDistrict('$districtName'),
                        builder: (context, snapshot) {
                          if (!snapshot.hasData) {
                            // print('districtName: $districtName');
                            // print('geter district1 ${model.districtName} $districtName');
                            return Container(
                                padding: EdgeInsets.all(20.0),
                                child: Center(
                                  child: Shimmer.fromColors(
                                      direction: ShimmerDirection.ltr,
                                      period: Duration(seconds: 2),
                                      child: GridView.count(
                                        padding: EdgeInsets.all(5.w),
                                        crossAxisCount: 2,
                                        mainAxisSpacing: 5.0,
                                        crossAxisSpacing: 5.0,
                                        children: [0, 1, 2, 3, 4, 5, 6]
                                            .map((_) => Padding(
                                                padding: const EdgeInsets.only(
                                                    bottom: 8.0),
                                                child: MerchantWidget()))
                                            .toList(),
                                      ),
                                      baseColor: AppColors.grey,
                                      highlightColor: Colors.white),
                                ));
                          } else if (snapshot.data.isNotEmpty) {
                            print('geter district2 ${model.districtName} $districtName');
                            return GridView.count(
                              children: snapshot.data
                                  .map((feed) => MerchantWidget(
                                        onTap: () {
                                          _analytics
                                              .sendAnalyticsCategorySelected(
                                                  feed.name);
                                        },
                                        categoriesModel: feed,
                                        color: model.changeIndex(),
                                      ))
                                  .toList(),
                              padding: EdgeInsets.all(5.w),
                              crossAxisCount: 2,
                              mainAxisSpacing: 5.0,
                              crossAxisSpacing: 5.0,
                            );
                          } else if (snapshot.hasError) {
                            return Column(
                              children: <Widget>[
                                SizedBox(
                                  height: 10,
                                ),
                                Text(
                                  tr.text('product.error'),
                                  style: TextStyle(
                                    fontWeight: FontWeight.bold,
                                    fontSize: 20,
                                  ),
                                ),
                                SizedBox(
                                  height: 10,
                                ),
                                Text(
                                  tr.text('product.notConnected'),
                                ),
                                SizedBox(
                                  height: 100,
                                ),
                              ],
                            );
                          } else {
                            return Text(
                              tr.text('product.notConnected'),
                              style: TextStyle(
                                fontWeight: FontWeight.bold,
                                fontSize: 20,
                              ),
                            );
                          }
                        }),
                  ):Expanded(
                    child: FutureBuilder<List<Categories>>(
                        future: model.getSearchedMerchant(model.searchName),
                        builder: (context, snapshot) {
                          if (!snapshot.hasData) {
                            return Container(
                                padding: EdgeInsets.all(20.0),
                                child: Center(
                                  child: Shimmer.fromColors(
                                      direction: ShimmerDirection.ltr,
                                      period: Duration(seconds: 2),
                                      child: GridView.count(
                                        padding: EdgeInsets.all(5.w),
                                        crossAxisCount: 2,
                                        mainAxisSpacing: 5.0,
                                        crossAxisSpacing: 5.0,
                                        children: [0, 1, 2, 3, 4, 5, 6]
                                            .map((_) => Padding(
                                            padding: const EdgeInsets.only(
                                                bottom: 8.0),
                                            child: MerchantWidget()))
                                            .toList(),
                                      ),
                                      baseColor: AppColors.grey,
                                      highlightColor: Colors.white),
                                ));
                          } else if (snapshot.data.isNotEmpty) {
                            return GridView.count(
                              children: snapshot.data
                                  .map((feed) => MerchantWidget(
                                onTap: () {
                                  _analytics
                                      .sendAnalyticsCategorySelected(
                                      feed.name);
                                },
                                categoriesModel: feed,
                                color: model.changeIndex(),
                              ))
                                  .toList(),
                              padding: EdgeInsets.all(5.w),
                              crossAxisCount: 2,
                              mainAxisSpacing: 5.0,
                              crossAxisSpacing: 5.0,
                            );
                          } else if (snapshot.hasError) {
                            return Column(
                              children: <Widget>[
                                SizedBox(
                                  height: 10,
                                ),
                                Text(
                                  tr.text('product.error'),
                                  style: TextStyle(
                                    fontWeight: FontWeight.bold,
                                    fontSize: 20,
                                  ),
                                ),
                                SizedBox(
                                  height: 10,
                                ),
                                Text(
                                  tr.text('product.notConnected'),
                                ),
                                SizedBox(
                                  height: 100,
                                ),
                              ],
                            );
                          } else {
                            return Text(
                              tr.text('product.notConnected'),
                              style: TextStyle(
                                fontWeight: FontWeight.bold,
                                fontSize: 20,
                              ),
                            );
                          }
                        }),
                  )
                ]),
          );
        });
  }
}
