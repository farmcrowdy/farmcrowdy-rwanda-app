/// status : true
/// message : "Password Reset OTP verified!."
/// data : {"status":true,"phone_number":"+2348169545792","email":null}

class ResetPasswordOtpResponse {
  bool _status;
  String _message;
  Data _data;

  bool get status => _status;
  String get message => _message;
  Data get data => _data;

  ResetPasswordOtpResponse({
      bool status, 
      String message, 
      Data data}){
    _status = status;
    _message = message;
    _data = data;
}

  ResetPasswordOtpResponse.fromJson(dynamic json) {
    _status = json["status"];
    _message = json["message"];
    _data = json["data"] != null ? Data.fromJson(json["data"]) : null;
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map["status"] = _status;
    map["message"] = _message;
    if (_data != null) {
      map["data"] = _data.toJson();
    }
    return map;
  }

}

/// status : true
/// phone_number : "+2348169545792"
/// email : null

class Data {
  bool _status;
  String _phoneNumber;
  dynamic _email;

  bool get status => _status;
  String get phoneNumber => _phoneNumber;
  dynamic get email => _email;

  Data({
      bool status, 
      String phoneNumber, 
      dynamic email}){
    _status = status;
    _phoneNumber = phoneNumber;
    _email = email;
}

  Data.fromJson(dynamic json) {
    _status = json["status"];
    _phoneNumber = json["phone_number"];
    _email = json["email"];
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map["status"] = _status;
    map["phone_number"] = _phoneNumber;
    map["email"] = _email;
    return map;
  }

}