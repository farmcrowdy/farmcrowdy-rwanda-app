import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'package:provider_architecture/provider_architecture.dart';
import 'package:rwandashop/core/model/authentication/register_phone_no.dart';
import 'package:rwandashop/core/model/authentication/reset_password_request.dart';
import 'package:rwandashop/core/services/auth_service.dart';
import 'package:rwandashop/ui/screens/register/registerViewModel.dart';
import 'package:rwandashop/utils/app_localizations.dart';
import 'package:rwandashop/utils/locator.dart';
import 'package:rwandashop/utils/router/navigationService.dart';
import 'package:rwandashop/utils/router/routeNames.dart';
import 'package:rwandashop/utils/screensize.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class ForgotPassword extends StatefulWidget {
  @override
  RegisterState createState() => new RegisterState();
}

class RegisterState extends State<ForgotPassword> {
  TextEditingController numberController = TextEditingController();

  final NavigationService _navigationService = locator<NavigationService>();
  bool passwordVisible;
  @override
  void initState() {
    super.initState();
    passwordVisible = true;
  }

  //
  @override
  Widget build(BuildContext context) {
    AppLocalizations tr = AppLocalizations.of(context);
    return ViewModelProvider<RegisterViewModel>.withConsumer(
        viewModelBuilder: () => RegisterViewModel(),
        builder: (context, model, child) {
          return Scaffold(
            body: ListView(children: <Widget>[
              Stack(
                children: [
                  Container(
                    height: Responsive.height(context) / 2,
                    width: Responsive.width(context),
                    child: Image.asset(
                      'assets/images/beans.png',
                      fit: BoxFit.cover,
                    ),
                  ),
                  InkWell(
                      onTap: () {
                        _navigationService.pop();
                      },
                      child: Container(
                        padding: EdgeInsets.all(20.w),
                        child: Icon(Icons.arrow_back_ios),
                      )),
                ],
              ),
              Container(
                padding: EdgeInsets.all(20),
                child: Text(
                  tr.text('forgetPassword.changePassword'),
                  style:
                      TextStyle(fontSize: 20.ssp, fontWeight: FontWeight.bold),
                ),
              ),
              SizedBox(
                height: 10.h,
              ),
              Container(
                padding: EdgeInsets.only(left: 20.w, right: 20.w),
                child: Text(
                  tr.text('forgetPassword.mobileNumber'),
                  style:
                      TextStyle(fontSize: 15.ssp, fontWeight: FontWeight.w500),
                ),
              ),
              Container(
                padding: EdgeInsets.all(20),
                child: TextFormField(
                  controller: numberController,
                  // maxLength: 11,
                  decoration: InputDecoration(
//                    errorText: residentialValid
//                        ? null
//                        : 'Kindly update this field',
                    hintText: '08100000000',
                    hintStyle: TextStyle(
                      color: Colors.black45,
                    ),
                    labelStyle: TextStyle(color: Colors.blue),
                    // border: new OutlineInputBorder(
                    //   borderRadius: new BorderRadius.circular(5.0),
                    //   borderSide: new BorderSide(),
                    // ),
                  ),
                  keyboardType: TextInputType.phone,
                  style: TextStyle(color: Colors.black),
                  cursorColor: Colors.black,
                ),
              ),
              Container(
                  padding: EdgeInsets.all(20.w),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(''),
                      InkWell(
                        onTap: () {
                          model.getPhone(numberController.text);
                          model.forgotPassword(ResetPasswordRequest(
                              phoneNumber: numberController.text));
                          // _navigationService.navigateTo(OTPRoute);
                        },
                        child: Container(
                          height: 50.w,
                          width: 50.w,
                          child: Image.asset(
                            'assets/images/next.png',
                            fit: BoxFit.cover,
                          ),
                        ),
                      ),
                    ],
                  )),
            ]),
          );
        });
  }
}
