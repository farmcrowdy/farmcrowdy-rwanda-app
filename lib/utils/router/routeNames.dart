



//Declare the routing constant here.

// Views constant
const String OnBoardingRoute = "OnBoardingScreen";
const String WelcomeRoute = "Welcome";
const String LoginRoute = "LoginPage";
const String SecondLoginRoute = "SecondLoginPage";
const String RegisterRoute = "RegisterPage";
const String BottomNavigationRoute = "BottomNavigation";
const String OTPRoute = "OTPPage";
const String ForgotPasswordRoute = "ForgotPassword";
const String EnterPasswordRoute = "EnterPasswordPage";
const String ForgotOTPRoute = "ForgotOTP";
const String EnterPasswordFGRoute = "EnterPasswordFG";
const String ShopRoute = "ShopPage";
const String ProductDetailsPageRoute = "ProductDetailsPage";
const String ProductRoute = "Product";
const String ProductMoreRoute = "ProductMore";
const String FiltersRoute = "Filters";
const String AccountRoute = "Account";
const String AcceptedRoute = "AcceptedPage";
const String CheckOutRoute = "CheckOutPage";
const String ChangePasswordRoute = "ChangePasswordPage";
const String YourInfoRoute = "YourInfoPage";
const String ChangeNameRoute = "ChangeNamePage";
const String ChangeContactRoute = "ChangeContactPage";
const String ChangeAddressRoute = "ChangeAddressPage";
const String LanguageScreenRoute = 'LanguageScreenPage';
const String CartScreenRoute = 'CartScreenRoutePage';
const String PaymentRoute = 'PaymentPage';








