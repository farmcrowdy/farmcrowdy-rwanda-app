import 'package:flutter/material.dart';
import 'package:rwandashop/utils/colors.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
class AppCheckBox extends StatefulWidget {
  const AppCheckBox(
      {Key key, this.value = false, this.onChanged, @required this.label})
      : super(key: key);

  final bool value;
  final Function(bool) onChanged;
  final String label;

  @override
  _AppCheckBoxState createState() => _AppCheckBoxState();
}

class _AppCheckBoxState extends State<AppCheckBox> {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Row(
        children: [
          Container(
            height: 20.w,
            width: 20.w,
            color: widget.value ? AppColors.themeGreen : Colors.white,
            child: Theme(
              data: ThemeData(
                unselectedWidgetColor: AppColors.themeGreen ,
              ),
              child: Checkbox(
                value: widget.value,
                activeColor: Colors.white,
                checkColor: AppColors.themeGreen ,
                onChanged: (val) => widget.onChanged(val),
              ),
            ),
          ),
          SizedBox(width: 15.w),
          InkWell(
            onTap: () => widget.onChanged(!widget.value),
            child: Text(widget.label,
                style: TextStyle(fontSize: 13.0.ssp, color: Color(0xFF8B959A))),
          ),
        ],
      ),
    );
  }
}
