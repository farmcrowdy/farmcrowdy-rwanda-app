import 'dart:io';

import 'package:get/get.dart';
import 'package:rwandashop/core/model/authentication/change_password.dart';
import 'package:rwandashop/core/model/authentication/update_address_model.dart';
import 'package:rwandashop/core/model/authentication/update_name_model.dart';
import 'package:rwandashop/core/model/authentication/update_number_email.dart';
import 'package:rwandashop/core/model/error_model.dart';
import 'package:rwandashop/core/model/success_model.dart';
import 'package:rwandashop/core/services/auth_service.dart';
import 'package:rwandashop/core/services/products_service.dart';
import 'package:rwandashop/utils/baseModel.dart';
import 'package:rwandashop/utils/colors.dart';
import 'package:rwandashop/utils/helpers.dart';
import 'package:rwandashop/utils/locator.dart';

class AccountViewModel extends BaseModel {
  final Product _product = locator<Product>();
  final Authentication _authentication = locator<Authentication>();
  changePassword(ChangePassword changePassword) async {
    setBusy(true);
    var result = await _authentication.updatePassword(changePassword.toJson());

    if (result is ErrorModel) {
      setBusy(false);
      showErrorToast(result.error);
      print(result.error);
      notifyListeners();
      return ErrorModel(result.error);
    }
    if (result is SuccessModel) {
      setBusy(false);
      //setBusy(false);
      // _navigationService.navigateTo(BottomNavigationRoute);
      showToast('Password change successfully');
      notifyListeners();
      // return SuccessModel(result.data);
    }
  }

  updateEmail(UpdateNumberEmail updateNumberEmail) async {
    setBusy(true);
    var result = await _authentication.updateEmail(updateNumberEmail.toJson());

    if (result is ErrorModel) {
      setBusy(false);
      showErrorToast(result.error);
      print(result.error);
      notifyListeners();
      return ErrorModel(result.error);
    }
    if (result is SuccessModel) {
      setBusy(false);
      //setBusy(false);

      Get.snackbar(
        'Updated',
        'Phone and Email updated successfully!',
        backgroundColor: AppColors.green,
      );
      // showToast('Password change successfully');
      notifyListeners();
      // return SuccessModel(result.data);
    }
  }
  getCartSummary() async {
    //  loading = true;
    var result = await _product.getCartSummary();
    if (result is ErrorModel) {
      //  loading = false;
      showErrorToast(result.error);
      print(result.error);
      notifyListeners();
      return ErrorModel(result.error);
    }
    if (result is SuccessModel) {
      print('SUMMARY:::::::::::');
      print(result.data);
      //loading = false;
      //cart = result.data;
      notifyListeners();
      // return categories;
    }
  }
  updateAddress(UpdateAddressModel updateAddressModel) async {
    setBusy(true);
    var result =
        await _authentication.updateAddress(updateAddressModel.toJson());

    if (result is ErrorModel) {
      setBusy(false);
      showErrorToast(result.error);
      print(result.error);
      notifyListeners();
      return ErrorModel(result.error);
    }
    if (result is SuccessModel) {
      getCartSummary();
      setBusy(false);
      //setBusy(false);

      Get.snackbar(
        'Updated',
        'Address updated successfully!',
        backgroundColor: AppColors.green,
      );
      // showToast('Password change successfully');
      notifyListeners();
      // return SuccessModel(result.data);
    }
  }

  updateFullName(UpdateNameModel updateNameModel) async {
    setBusy(true);
    var result = await _authentication.updateName(updateNameModel.toJson());
    if (result is ErrorModel) {
      setBusy(false);
      showErrorToast(result.error);
      print(result.error);
      notifyListeners();
      return ErrorModel(result.error);
    }
    if (result is SuccessModel) {
      setBusy(false);
      //setBusy(false);

      Get.snackbar(
        'Updated',
        'Full name updated successfully!',
        backgroundColor: AppColors.green,
      );
      // showToast('Password change successfully');
      notifyListeners();
      // return SuccessModel(result.data);
    }
  }

  getZone() async {
    var result = await _authentication.getZone();

    if (result is ErrorModel) {
      //setBusy(false);
      showErrorToast(result.error);
      print(result.error);
      notifyListeners();
      return ErrorModel(result.error);
    }
    if (result is SuccessModel) {
      //showToast(result.data['message']);

      //_user = result.data;

      //notifyListeners();
      return result.data['data'];
    }
  }

  getCity(String id) async {
    var result = await _authentication.getCity(id);

    if (result is ErrorModel) {
      //setBusy(false);
      showErrorToast(result.error);
      print(result.error);
      notifyListeners();
      return ErrorModel(result.error);
    }
    if (result is SuccessModel) {
      //showToast(result.data['message']);

      //_user = result.data;

      //notifyListeners();
      return result.data['data'];
    }
  }
  getDistrict(String id) async {
    var result = await _authentication.getDistrict(id);

    if (result is ErrorModel) {
      //setBusy(false);
      showErrorToast(result.error);
      print(result.error);
      notifyListeners();
      return ErrorModel(result.error);
    }
    if (result is SuccessModel) {
      //showToast(result.data['message']);

      //_user = result.data;

      //notifyListeners();
      return result.data['data'];
    }
  }
  uploadDp(File image) async {
    setBusy(true);
    var result =
        await _authentication.uploadImage(image, _authentication.token.token);

    if (result == null) {
      setBusy(false);
      //showErrorToast(result.error);
      notifyListeners();
      return result;
    }

    setBusy(false);
    Get.snackbar(
      'Updated',
      'Profile picture updated successfully!',
      backgroundColor: AppColors.green,
    );
    //_user = result.data;
    // _navigationService.navigateReplacementTo(BottomNavRoute);
    notifyListeners();
    return result;
  }
}
