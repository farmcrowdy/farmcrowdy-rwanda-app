import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider_architecture/provider_architecture.dart';
import 'package:rwandashop/ui/screens/register/registerViewModel.dart';
import 'package:rwandashop/ui/widget/generalButton.dart';
import 'package:rwandashop/utils/colors.dart';
import 'package:rwandashop/utils/locator.dart';
import 'package:rwandashop/utils/router/navigationService.dart';
import 'package:rwandashop/utils/screensize.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class Filters extends StatefulWidget {
  @override
  FiltersState createState() => new FiltersState();
}

class FiltersState extends State<Filters> {
  TextEditingController numberController = TextEditingController();

  final NavigationService _navigationService = locator<NavigationService>();
  bool passwordVisible;
  String gender = 'Select Zone';

  @override
  void initState() {
    super.initState();
  }
  String radioItem = '';
  //
  @override
  Widget build(BuildContext context) {
    return ViewModelProvider<RegisterViewModel>.withConsumer(
        viewModelBuilder: () => RegisterViewModel(),
        builder: (context, model, child) {
          return Scaffold(
            body: SingleChildScrollView(
              child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    SizedBox(
                      height: 80.h,
                    ),
                    Container(
                      padding: EdgeInsets.only(left: 20.w, right: 20.w),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          InkWell(
                            onTap: () {
                              _navigationService.pop();
                            },
                            child: Icon(
                              Icons.cancel_outlined,
                              color: Colors.black,
                            ),
                          ),
                          Container(
                            child: Text(
                              'Filters',
                              style: TextStyle(
                                  fontSize: 18.ssp, fontWeight: FontWeight.bold),
                            ),
                          ),
                          Text('')
                        ],
                      ),
                    ),
                    SizedBox(height: 20.h),

                    Container(
                      padding: EdgeInsets.all(20.w),
                      width: Responsive.width(context),
                        height: Responsive.height(context)/1.2,
                        decoration: BoxDecoration(
                            color: Color(0xffF2F3F2),
                            borderRadius: BorderRadius.circular(15.0),
                            border: Border.all(
                              color: Color(0xffF2F3F2),
                            )),
                        child:
Column(
  crossAxisAlignment: CrossAxisAlignment.start,
  mainAxisAlignment: MainAxisAlignment.start,
  children: [
  Text('Filter by Brand',style: TextStyle(fontSize: 18.ssp, fontWeight: FontWeight.bold),),

        Theme(
          child: RadioListTile(

            activeColor:AppColors.themeGreen,
            groupValue: radioItem,
            title:Text('Dow DuPont') ,
            value: 'Dow DuPont',
            onChanged: (val) {
              setState(() {
                radioItem = val;
              });
            },
          ),
          data: ThemeData(
            unselectedWidgetColor: AppColors.themeGreen,
          ),
        ),
    Theme(
      child: RadioListTile(
        activeColor:AppColors.themeGreen,
        groupValue: radioItem,
        title:Text('Adama India Limited') ,
        value: 'Adama India Limited',
        onChanged: (val) {
          setState(() {
            radioItem = val;
          });
        },
      ),
      data: ThemeData(
        unselectedWidgetColor: AppColors.themeGreen,
      ),
    ),
    Theme(
      child: RadioListTile(
        activeColor:AppColors.themeGreen,
        groupValue: radioItem,
        title:Text('BASF India Limited') ,
        value: 'BASF India Limited',
        onChanged: (val) {
          setState(() {
            radioItem = val;
          });
        },
      ),
      data: ThemeData(
        unselectedWidgetColor: AppColors.themeGreen,
      ),
    ),

    Theme(
      child: RadioListTile(
        activeColor:AppColors.themeGreen,
        groupValue: radioItem,
        title:Text('Excel Crop Care') ,
        value: 'Excel Crop Care',
        onChanged: (val) {
          setState(() {
            radioItem = val;
          });
        },
      ),
      data: ThemeData(
        unselectedWidgetColor: AppColors.themeGreen,
      ),
    ), Theme(
      child: RadioListTile(
        activeColor:AppColors.themeGreen,
        groupValue: radioItem,
        title:Text('PI Industries Limited') ,
        value: 'PI Industries Limited',
        onChanged: (val) {
          setState(() {
            radioItem = val;
          });
        },
      ),
      data: ThemeData(
        unselectedWidgetColor: AppColors.themeGreen,
      ),
    ), Theme(
      child: RadioListTile(
        activeColor:AppColors.themeGreen,
        groupValue: radioItem,
        title:Text('Aries Agro Limited') ,
        value: 'Aries Agro Limited',
        onChanged: (val) {
          setState(() {
            radioItem = val;
          });
        },
      ),
      data: ThemeData(
        unselectedWidgetColor: AppColors.themeGreen,
      ),
    ), Theme(
      child: RadioListTile(
        activeColor:AppColors.themeGreen,
        groupValue: radioItem,
        title:Text('Seminis') ,
        value: 'Seminis',
        onChanged: (val) {
          setState(() {
            radioItem = val;
          });
        },
      ),
      data: ThemeData(
        unselectedWidgetColor: AppColors.themeGreen,
      ),
    ), Theme(
      child: RadioListTile(
        activeColor:AppColors.themeGreen,
        groupValue: radioItem,
        title:Text('Supreme') ,
        value: 'Supreme',
        onChanged: (val) {
          setState(() {
            radioItem = val;
          });
        },
      ),
      data: ThemeData(
        unselectedWidgetColor: AppColors.themeGreen,
      ),
    ), Theme(
      child: RadioListTile(
        activeColor:AppColors.themeGreen,
        groupValue: radioItem,
        title:Text('VNR Seeds') ,
        value: 'VNR Seeds',
        onChanged: (val) {
          setState(() {
            radioItem = val;
          });
        },
      ),
      data: ThemeData(
        unselectedWidgetColor: AppColors.themeGreen,
      ),
    ),

    Container(
        padding: EdgeInsets.only(left: 20.w, right: 20.w),
        height: 50.h,
        child: GeneralButton(
          buttonText: 'Apply Filter',
          onPressed: () {
            // _navigationService.navigateTo(LoginRoute);
          },
        )),
                  ],
))


                        ])
            ),
          );
        });
  }
}
