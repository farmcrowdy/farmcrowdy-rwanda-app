import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:rwandashop/ui/screens/bottomNavigation/bottomNav_modelView.dart';
import 'package:rwandashop/ui/widget/generalButton.dart';
import 'package:rwandashop/utils/app_localizations.dart';
import 'package:rwandashop/utils/colors.dart';
import 'package:rwandashop/utils/locator.dart';
import 'package:rwandashop/utils/router/navigationService.dart';
import 'package:rwandashop/utils/router/routeNames.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class AcceptedPage extends StatefulWidget {
  @override
  AcceptedPageState createState() => new AcceptedPageState();
}

class AcceptedPageState extends State<AcceptedPage> {
  final NavigationService _navigationService = locator<NavigationService>();
  final AppStateProvider appStateProvider = locator<AppStateProvider>();
  @override
  void initState() {
    super.initState();
  }

  //
  @override
  Widget build(BuildContext context) {
    AppLocalizations tr = AppLocalizations.of(context);
    final deviceHeight = MediaQuery.of(context).size.height;
    final deviceWidth = MediaQuery.of(context).size.width;
    return Stack(// <-- STACK AS THE SCAFFOLD PARENT
        children: [
      Container(
        height: deviceHeight,
        width: deviceWidth,
        child: Image.asset(
          'assets/images/acceptedBackground.png',
          fit: BoxFit.fill,
        ),
      ),
      Scaffold(
        backgroundColor: Colors.transparent,
        body: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Container(
              height: 160.h,
              width: 160.w,
              child: Image.asset(
                'assets/images/orderAccepted.png',
                fit: BoxFit.cover,
              ),
            ),
            SizedBox(
              height: 40.h,
            ),
            Text(
              tr.text('accepted.order'),
              style: TextStyle(
                  fontSize: 22.ssp,
                  color: AppColors.black,
                  fontWeight: FontWeight.bold),
            ),
            SizedBox(
              height: 5.h,
            ),
            Text(
              tr.text('accepted.orderComplete'),
              style: TextStyle(
                  fontSize: 22.ssp,
                  color: AppColors.black,
                  fontWeight: FontWeight.bold),
            ),
            SizedBox(
              height: 10.h,
            ),
            Text(
              tr.text('accepted.yourItem'),
              style: TextStyle(
                  fontSize: 16.ssp,
                  color: AppColors.lowGrey,
                  fontWeight: FontWeight.w400),
            ),
            SizedBox(
              height: 5.h,
            ),
            Text(
              tr.text('accepted.yourItemComplete'),
              style: TextStyle(
                  fontSize: 16.ssp,
                  color: AppColors.lowGrey,
                  fontWeight: FontWeight.w400),
            ),
            SizedBox(
              height: 60.h,
            ),
            Container(
                padding: EdgeInsets.all(20.w),
                child: GeneralButton(
                  buttonText: tr.text('accepted.trackOrder'),
                  onPressed: () {
                    appStateProvider.setCurrentTabTo(newTabIndex: 3);
                    _navigationService.navigateTo(BottomNavigationRoute);
                  },
                )),
            InkWell(
              onTap: () {
                _navigationService.navigateTo(BottomNavigationRoute);
              },
              child: Text(
                tr.text('accepted.backHome'),
                style: TextStyle(
                    fontSize: 18.ssp,
                    color: AppColors.black,
                    fontWeight: FontWeight.bold),
              ),
            ),
          ],
        ),
      )
    ]);
  }
}
