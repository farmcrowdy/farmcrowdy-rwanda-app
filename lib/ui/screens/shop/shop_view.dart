import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_app_dialog/flutter_app_dialog.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:provider_architecture/provider_architecture.dart';
import 'package:rwandashop/core/model/product/catergories.dart';
import 'package:rwandashop/core/model/product/products.dart' as product;
import 'package:rwandashop/core/model/product/search.dart';
import 'package:rwandashop/core/services/auth_service.dart';
import 'package:rwandashop/core/services/products_service.dart';
import 'package:rwandashop/ui/screens/shop/shopViewModel.dart';
import 'package:rwandashop/ui/widget/category_item.dart';
import 'package:rwandashop/ui/widget/package_widget.dart';
import 'package:rwandashop/utils/app_localizations.dart';
import 'package:rwandashop/utils/colors.dart';
import 'package:rwandashop/utils/locator.dart';
import 'package:rwandashop/utils/router/navigationService.dart';
import 'package:rwandashop/utils/screensize.dart';
import 'package:shimmer/shimmer.dart';

class ShopPage extends StatefulWidget {
  @override
  ShopPageState createState() => new ShopPageState();
}

class ShopPageState extends State<ShopPage> {
  final Authentication _authentication = locator<Authentication>();
  final Product _product = locator<Product>();
  final NavigationService _navigationService = locator<NavigationService>();
  String searchItem = '';
  bool passwordVisible;

  StateSetter _setState;
  List zone = List();
  List city = List();
  List district = List();
  int zoneId;

  // int filterResult = 0;

  bool firstCategoryFetch = true;

  // int districtId;
  //  int categoryId = 6;
  int cityId;
  bool showFilterButton = false;
  String zoneName;
  String districtName;
  String categoryName;

  getZone(ShopViewModel model) async {
    var zones = await model.getZone();
    print(zones.runtimeType);
    setState(() {
      zone.clear();
      zone = zones;
      print(zone);
    });
  }

  getCity(ShopViewModel model, String id) async {
    print('city: $id');
    if (!mounted) {
      return; // Just do nothing if the widget is disposed.
    }
    var cities = await model.getCity(id);
    _setState(() {
      city = cities;
      print('ddd:::::::::::::::::::::');
      print(city);
    });
  }

  getDistrict(ShopViewModel model, String id) async {
    var districts = await model.getDistrict(id); //'7'
    _setState(() {
      district = districts;
      print('dddf:::::::::::::::::::::');
      print(district);
    });
  }

  setDistrictName(String districtNameString) {
    _setState(() {
      districtName = districtNameString;
      // filterResult = 1;
    });
    print('districtName $districtName');
  }

  _filterWidget(ShopViewModel model, AppLocalizations tr) {
    return Column(
      children: [
        Container(
          padding: EdgeInsets.fromLTRB(10, 15, 10, 20),
          child: Text(
            tr.text('shop.filter'),
            style: TextStyle(
                fontSize: 20,
                fontWeight: FontWeight.w700,
                color: AppColors.filterBlack),
          ),
        ),
        Divider(
          color: Colors.grey,
        ),
        Container(
          margin: EdgeInsets.all(10),
          child: Column(
            children: [
              Container(
                padding: EdgeInsets.only(bottom: 10),
                child: Align(
                  alignment: Alignment.centerLeft,
                  child: Text(
                    tr.text('general.province'),
                    textAlign: TextAlign.left,
                    style: TextStyle(fontSize: 14, fontWeight: FontWeight.w600),
                  ),
                ),
              ),
              GridView.builder(
                  gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
                      crossAxisCount: 3,
                      crossAxisSpacing: 5,
                      mainAxisSpacing: 5,
                      childAspectRatio: 2.0),
                  shrinkWrap: true,
                  itemCount: zone.length,
                  scrollDirection: Axis.vertical,
                  physics: NeverScrollableScrollPhysics(),
                  itemBuilder: (BuildContext context, int index) {
                    return InkWell(
                      onTap: () {
                        _setState(() {
                          zoneId = zone[index]['id'];
                          zoneName = zone[index]['name'];
                          city.clear();
                          district.clear();
                          showFilterButton = false;
                          cityId = null;
                          getCity(model, '$zoneId');
                        });
                      },
                      child: Container(
                        padding: EdgeInsets.all(5),
                        height: 36,
                        child: Center(
                          child: Text(
                            zone[index]['name'].toString(),
                            style: TextStyle(
                                fontWeight: FontWeight.w400,
                                color: AppColors.filterBlack),
                          ),
                        ),
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(10.0),
                            border: Border.all(
                              color: zoneId == zone[index]['id']
                                  ? AppColors.green
                                  : AppColors.deepGrey,
                            )),
                      ),
                    );
                  }),
              SizedBox(height: 10),
              Divider(
                color: Colors.grey,
              ),
            ],
          ),
        ),
        city.length != 0
            ? Container(
                margin: EdgeInsets.all(10),
                child: Column(
                  children: [
                    Container(
                      padding: EdgeInsets.only(bottom: 10),
                      child: Align(
                        alignment: Alignment.centerLeft,
                        child: Text(
                          tr.text('general.district'),
                          textAlign: TextAlign.left,
                          style: TextStyle(
                              fontSize: 14, fontWeight: FontWeight.w600),
                        ),
                      ),
                    ),
                    GridView.builder(
                        scrollDirection: Axis.vertical,
                        physics: NeverScrollableScrollPhysics(),
                        primary: true,
                        shrinkWrap: true,
                        addAutomaticKeepAlives: true,
                        gridDelegate:
                            const SliverGridDelegateWithFixedCrossAxisCount(
                                crossAxisCount: 3,
                                crossAxisSpacing: 5,
                                mainAxisSpacing: 5,
                                childAspectRatio: 2.0),
                        itemCount: city.length,
                        itemBuilder: (BuildContext context, int index) {
                          return InkWell(
                            onTap: () {
                              _setState(() {
                                showFilterButton = false;
                                cityId = city[index]['id'];

                                districtName = city[index]['name'];
                              });
                            },
                            child: Container(
                              padding: EdgeInsets.all(10),
                              height: 36,
                              child: Center(
                                child: Text(city[index]['name'].toString(),
                                    style: TextStyle(
                                        fontWeight: FontWeight.w400,
                                        color: AppColors.filterBlack)),
                              ),
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(10.0),
                                  border: Border.all(
                                    color: cityId == city[index]['id']
                                        ? AppColors.green
                                        : AppColors.deepGrey,
                                  )),
                            ),
                          );
                        }),
                    SizedBox(height: 10),
                    Divider(
                      color: Colors.grey,
                    ),
                  ],
                ),
              )
            : Container(),
        SizedBox(
          height: 20,
        ),
        cityId != null
            ? Container(
                margin: EdgeInsets.all(10),
                child: Column(
                  children: [
                    Container(
                      padding: EdgeInsets.only(bottom: 10),
                      child: Align(
                        alignment: Alignment.centerLeft,
                        child: Text(
                          tr.text('general.category'),
                          textAlign: TextAlign.left,
                          style: TextStyle(
                              fontSize: 14, fontWeight: FontWeight.w600),
                        ),
                      ),
                    ),
                    FutureBuilder<List<Categories>>(
                      future: model.getCategories(),
                      builder: (context, snapshot) {
                        return snapshot.hasData
                            ? GridView.builder(
                                scrollDirection: Axis.vertical,
                                physics: NeverScrollableScrollPhysics(),
                                primary: true,
                                shrinkWrap: true,
                                addAutomaticKeepAlives: true,
                                gridDelegate:
                                    const SliverGridDelegateWithFixedCrossAxisCount(
                                        crossAxisCount: 3,
                                        crossAxisSpacing: 5,
                                        mainAxisSpacing: 5,
                                        childAspectRatio: 2.0),
                                itemCount: snapshot.data.length,
                                itemBuilder: (BuildContext context, int index) {
                                  return InkWell(
                                    onTap: () {
                                      _setState(() {
                                        // categoryId = snapshot.data[index].id;
                                        model
                                            .setCategoryId(snapshot.data[0].id);
                                        categoryName =
                                            snapshot.data[index].name;
                                        showFilterButton = true;
                                      });
                                    },
                                    child: Container(
                                      padding: EdgeInsets.all(10),
                                      height: 36,
                                      child: Center(
                                        child: Text(snapshot.data[index].name,
                                            style: TextStyle(
                                                fontWeight: FontWeight.w400,
                                                color: AppColors.filterBlack)),
                                      ),
                                      decoration: BoxDecoration(
                                          borderRadius:
                                              BorderRadius.circular(10.0),
                                          border: Border.all(
                                            color: model.categoryId ==
                                                    snapshot.data[index].id
                                                ? AppColors.green
                                                : AppColors.deepGrey,
                                          )),
                                    ),
                                  );
                                })
                            : Container();
                      },
                    ),
                  ],
                ))
            : Container(),
        showFilterButton
            ? Container(
                height: Responsive.height(context) * .05,
                margin: EdgeInsets.only(top: 20.0, bottom: 20.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    Container(
                        height: 48,
                        width: Responsive.width(context) * .4,
                        child: MaterialButton(
                          onPressed: () {
                            Navigator.of(context, rootNavigator: true).pop();
                          },
                          child: Center(
                            child: Text(
                              tr.text('shop.close'),
                              style: TextStyle(
                                  fontSize: 11.ssp,
                                  color: AppColors.green,
                                  fontWeight: FontWeight.w500),
                            ),
                          ),
                        )),
                    Container(
                        width: Responsive.width(context) * .4,
                        height: 48,
                        decoration: BoxDecoration(
                            color: AppColors.green,
                            borderRadius: BorderRadius.circular(6)),
                        child: MaterialButton(
                          onPressed: () {
                            _setState(() {
                              model.setFilterAction(1);
                              Navigator.of(context, rootNavigator: true).pop();
                            });
                          },
                          child: Center(
                            child: Text(
                              tr.text('shop.applyFilter'),
                              style: TextStyle(
                                  fontSize: 11.ssp,
                                  color: AppColors.white,
                                  fontWeight: FontWeight.w500),
                            ),
                          ),
                        )),
                  ],
                ),
              )
            : Container()
      ],
    );
  }

  @override
  initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    AppLocalizations tr = AppLocalizations.of(context);
    return ViewModelProvider<ShopViewModel>.withConsumer(
        viewModelBuilder: () => ShopViewModel(),
        onModelReady: (model) {
          model.getCategories();
          getZone(model);
        },
        builder: (context, model, child) {
          searchItem = model.searchName;
          return Scaffold(
            body: Column(key: PageStorageKey('storage-key'), children: <Widget>[
              SizedBox(height: 40.h),
              if (_authentication.currentUser.isNull)
                Container()
              else
                Column(
                  children: [
                    // Container(
                    //   child: Row(
                    //     mainAxisAlignment: MainAxisAlignment.center,
                    //     children: [
                    //       Icon(
                    //         Icons.location_on,
                    //         size: 20,
                    //       ),
                    //       SizedBox(
                    //         width: 5.w,
                    //       ),
                    //       Container(
                    //         child: Text(
                    //           _authentication.currentUser.zoneName +
                    //               ' ' +
                    //               _authentication.currentUser.cityName,
                    //           style: TextStyle(
                    //               fontSize: 12, fontWeight: FontWeight.w500),
                    //         ),
                    //       ),
                    //     ],
                    //   ),
                    // ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Container(
                          width: MediaQuery.of(context).size.width * 0.88,
                          padding: EdgeInsets.only(left: 20, right: 5),
                          child: TextField(
                            decoration: InputDecoration(
                              focusedBorder: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(15.0.r),
                                  borderSide: BorderSide(
                                      color: Color(0xffF2F3F2),
                                      width: 0.0,
                                      style: BorderStyle.none)),
                              enabledBorder: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(15.0),
                                  borderSide: BorderSide(
                                      color: Color(0xffF2F3F2),
                                      width: 0.0,
                                      style: BorderStyle.none)),
                              errorBorder: InputBorder.none,
                              disabledBorder: InputBorder.none,
                              fillColor: Color(0xffF2F3F2),
                              filled: true,
                              border: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(15.0),
                                  borderSide: BorderSide(
                                      color: Color(0xffF2F3F2),
                                      width: 0.0,
                                      style: BorderStyle.none)),
                              hintText: tr.text('shop.search'),
                              hintStyle: TextStyle(
                                  color: Color(0xff7C7C7C),
                                  fontSize: 14,
                                  fontWeight: FontWeight.w500),
                              contentPadding:
                                  EdgeInsets.fromLTRB(10.0, 10.0, 10.0, 10.0),
                              prefixIcon: Icon(
                                Icons.search,
                                color: Colors.black,
                              ),
                            ),
                            textInputAction: TextInputAction.search,
                            onSubmitted: (value) {
                              // model.searchUser(context, value);
                              // model.searchGroup(context, value);
                            },
                            onChanged: (value) {
                              model.initiateSearch(value, model);
                              model.setFilterAction(0);
                              // model.searchGroup(context, value);
                            },
                          ),
                        ),
                        Container(
                          // padding: EdgeInsets.only(top: 20),
                          child: IconButton(
                            icon: Container(
                              height: 40.h,
                              width: 40.w,
                              child:
                                  Image.asset('assets/images/filterIcon.png'),
                            ),
                            onPressed: () {
                              showDialog(
                                  context: context,
                                  builder: (BuildContext context) {
                                    return StatefulBuilder(builder:
                                        (BuildContext context,
                                            StateSetter setState) {
                                      _setState = setState;
                                      return BaseDialogWidget(
                                          child: Container(
                                        decoration: BoxDecoration(
                                          borderRadius:
                                              BorderRadius.circular(25),
                                        ),
                                        child: SingleChildScrollView(
                                          child: Stack(
                                            children: [
                                              _filterWidget(model, tr)
                                            ],
                                          ),
                                        ),
                                      ));
                                    });
                                  });
                            },
                          ),
                        )
                        // Center(
                        //   child: InkWell(
                        //     onTap: () {
                        //       _navigationService.navigateTo(CartScreenRoute);
                        //     },
                        //     child: Badge(
                        //       borderRadius: BorderRadius.circular(5.r),
                        //       badgeColor: AppColors.themeGreen,
                        //       badgeContent: Text(
                        //         null != _product.cartSummary
                        //             ? _product.cartSummary.data.basketSize
                        //             .toString()
                        //             : '0',
                        //         style: TextStyle(
                        //             color: Colors.white,
                        //             fontWeight: FontWeight.bold),
                        //       ),
                        //       child: Container(
                        //         height: 20.h,
                        //         width: 20.w,
                        //         child: Image.asset('assets/images/cart.png'),
                        //       ),
                        //     ),
                        //   ),
                        // ),
                      ],
                    )
                  ],
                ),
              SizedBox(
                height: 18,
              ),
              Container(
                padding: EdgeInsets.only(left: 20.w, right: 20.w),
                child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(
                        tr.text('general.category'),
                        style: TextStyle(
                            fontSize: 24.ssp,
                            color: Colors.black,
                            fontWeight: FontWeight.w700),
                      ),
                      // InkWell(
                      //     onTap: () {
                      //       _navigationService.navigateTo(ProductRoute);
                      //     },
                      //     child: Text(
                      //       tr.text('shop.see'),
                      //       style: TextStyle(
                      //           color: AppColors.themeGreen,
                      //           fontWeight: FontWeight.w500,
                      //           fontSize: 14.ssp),
                      //     ))
                    ]),
              ),
              SizedBox(height: 10.h),
              Container(
                height: 50,
                margin: EdgeInsets.only(left: 10, right: 10),
                child: model.filterAction == 0
                    ? FutureBuilder<List<Categories>>(
                        future: model.getCategories(),
                        builder: (context, snapshot) {
                          if (!snapshot.hasData) {
                            return Container(
                                padding: EdgeInsets.all(20.0),
                                child: Center(
                                  child: Shimmer.fromColors(
                                      direction: ShimmerDirection.ltr,
                                      period: Duration(seconds: 2),
                                      child: Row(
                                        children: [0, 1, 2, 3, 4, 5]
                                            .map((_) => Expanded(
                                                  child: Padding(
                                                      padding:
                                                          const EdgeInsets.all(
                                                              8.0),
                                                      child: Container(
                                                        color:
                                                            AppColors.deepGreen,
                                                        height:
                                                            Responsive.width(
                                                                    context) *
                                                                .22,
                                                      )),
                                                ))
                                            .toList(),
                                      ),
                                      baseColor: AppColors.grey,
                                      highlightColor: Colors.white),
                                ));
                          } else if (snapshot.data.isNotEmpty) {
                             firstCategoryFetch ?  model.setCategoryId(snapshot.data[0].id) : null ; //=  : null ;
                            return Container(
                              child: ListView(
                                  scrollDirection: Axis.horizontal,
                                  shrinkWrap: true,
                                  children: snapshot.data
                                      .map((feed) => CategoriesItem(
                                            categoriesModel: feed,
                                            active: model.categoryId == feed.id
                                                    ? true
                                                    : false,
                                            onTap: () {
                                              setState(() {
                                                // categoryId = feed.id;
                                                model.setCategoryId(feed.id);
                                                firstCategoryFetch = false;
                                              });
                                            },
                                          ))
                                      .toList()),
                            );
                            // ListView(
                            //   scrollDirection: Axis.horizontal,
                            //   shrinkWrap: true,
                            //   children: snapshot.data
                            //       .map((feed) => MerchantsItem(
                            //             categoriesModel: feed,
                            //             active:
                            //                 productId == feed.id ? true : false,
                            //             onTap: () {
                            //               setState(() {
                            //                 productId = feed.id;
                            //               });
                            //             },
                            //           ))
                            //       .toList());
                          } else if (snapshot.hasError) {
                            return Column(
                              children: <Widget>[
                                SizedBox(
                                  height: 10,
                                ),
                                Text(
                                  'Error fetching data',
                                  style: TextStyle(
                                    fontWeight: FontWeight.bold,
                                    fontSize: 20,
                                  ),
                                ),
                                SizedBox(
                                  height: 10,
                                ),
                                Text("${'Make sure you are connected'}"),
                                SizedBox(
                                  height: 100,
                                ),
                              ],
                            );
                          } else {
                            return Text(
                              'Database Empty',
                              style: TextStyle(
                                fontWeight: FontWeight.bold,
                                fontSize: 20,
                              ),
                            );
                          }
                        })
                    : Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        mainAxisSize: MainAxisSize.max,
                        children: [
                          Container(
                            padding: EdgeInsets.all(10),
                            height: 40,
                            width: Responsive.width(context) * .3,
                            child: Center(
                              child: Text(
                                  zoneName.length > 10
                                      ? zoneName.substring(0, 10) + '...'
                                      : zoneName,
                                  style: TextStyle(
                                      fontWeight: FontWeight.w400,
                                      color: AppColors.filterBlack,
                                      fontSize: 14)),
                            ),
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(10.0),
                                border: Border.all(
                                  color: AppColors.green,
                                )),
                          ),
                          SizedBox(
                            width: 10,
                          ),
                          Container(
                            padding: EdgeInsets.all(10),
                            height: 40,
                            width: Responsive.width(context) * .3,
                            child: Center(
                              child: Text(
                                  districtName.length > 10
                                      ? districtName.substring(0, 10) + '...'
                                      : districtName,
                                  style: TextStyle(
                                      fontWeight: FontWeight.w400,
                                      color: AppColors.filterBlack,
                                      fontSize: 14)),
                            ),
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(10.0),
                                border: Border.all(
                                  color: AppColors.green,
                                )),
                          ),
                          SizedBox(
                            width: 10,
                          ),
                          Container(
                            padding: EdgeInsets.all(10),
                            height: 40,
                            width: Responsive.width(context) * .3,
                            child: Center(
                              child: Text(
                                  categoryName.length > 10
                                      ? categoryName.substring(0, 10) + '...'
                                      : categoryName,
                                  style: TextStyle(
                                      fontWeight: FontWeight.w400,
                                      color: AppColors.filterBlack,
                                      fontSize: 14)),
                            ),
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(10.0),
                                border: Border.all(
                                  color: AppColors.green,
                                )),
                          ),
                        ],
                      ),
              ),
              model.loadingNew
                  ? LinearProgressIndicator(
                      backgroundColor: AppColors.green,
                      valueColor: AlwaysStoppedAnimation(Colors.green),
                    )
                  : Container(),
              searchItem == null
                  ? Expanded(
                      child: FutureBuilder<List<product.Data>>(
                          future: model.filterAction == 0
                              ? model.getPackagesByCategory(
                                  model.categoryId.toString())
                              : model.getPackagesByCategoryAndDistrict(
                                  model.categoryId.toString(), districtName),
                          builder: (context, snapshot) {
                            if (!snapshot.hasData) {
                              return Container(
                                  padding: EdgeInsets.all(20.0),
                                  child: Center(
                                    child: Shimmer.fromColors(
                                        direction: ShimmerDirection.ltr,
                                        period: Duration(seconds: 3),
                                        child: ListView(
                                          children: [0, 1, 2, 3, 4, 5]
                                              .map((_) => Padding(
                                                    padding:
                                                        const EdgeInsets.all(
                                                            8.0),
                                                    child: Row(
                                                      crossAxisAlignment:
                                                          CrossAxisAlignment
                                                              .start,
                                                      children: [
                                                        Container(
                                                          width:
                                                              Responsive.width(
                                                                      context) -
                                                                  20,
                                                          height: 150,
                                                          color: Colors.white,
                                                        ),
                                                        Padding(
                                                          padding:
                                                              const EdgeInsets
                                                                      .symmetric(
                                                                  horizontal:
                                                                      8.0),
                                                        ),
                                                        Expanded(
                                                          child: Column(
                                                            crossAxisAlignment:
                                                                CrossAxisAlignment
                                                                    .start,
                                                            children: [
                                                              Container(
                                                                width: double
                                                                    .infinity,
                                                                height: 8.0,
                                                                color: Colors
                                                                    .white,
                                                              ),
                                                              Padding(
                                                                padding: const EdgeInsets
                                                                        .symmetric(
                                                                    vertical:
                                                                        2.0),
                                                              ),
                                                              Container(
                                                                width: double
                                                                    .infinity,
                                                                height: 8.0,
                                                                color: Colors
                                                                    .white,
                                                              ),
                                                              Padding(
                                                                padding: const EdgeInsets
                                                                        .symmetric(
                                                                    vertical:
                                                                        2.0),
                                                              ),
                                                              Container(
                                                                width: 40.0,
                                                                height: 8.0,
                                                                color: Colors
                                                                    .white,
                                                              ),
                                                            ],
                                                          ),
                                                        )
                                                      ],
                                                    ),
                                                  ))
                                              .toList(),
                                        ),

                                        // ListView.builder(
                                        //     itemCount: 6,
                                        //     itemBuilder: (context, index) {
                                        //       return Padding(
                                        //           padding:
                                        //               const EdgeInsets.only(
                                        //                   bottom: 8.0),
                                        //           child: PackageWidget(
                                        //               model: model,
                                        //               categoryId: categoryId
                                        //                   .toString()));
                                        //     }),
                                        // GridView.count(
                                        //   padding: EdgeInsets.all(20.w),
                                        //   childAspectRatio: (200.w / 330.h),
                                        //   crossAxisCount: 2,
                                        //   mainAxisSpacing: 12.0.w,
                                        //   crossAxisSpacing: 12.0.w,
                                        //   children: [0, 1, 2, 3, 4, 5, 6]
                                        //       .map((_) => Padding(
                                        //           padding:
                                        //               const EdgeInsets.only(
                                        //                   bottom: 8.0),
                                        //           child: PackageWidget(
                                        //             model: model,
                                        //           )))
                                        //       .toList(),
                                        // ),
                                        baseColor: AppColors.grey,
                                        highlightColor: Colors.white),
                                  ));
                            } else if (snapshot.data.isNotEmpty) {
                              return Container(
                                padding:
                                    EdgeInsets.only(right: 10.0, left: 10.0),
                                child: ListView.builder(
                                    itemCount: snapshot.data.length,
                                    itemBuilder: (context, index) {
                                      return Card(
                                        elevation: 2,
                                        child: Container(
                                          child: PackageWidget(
                                            packageModel: snapshot.data[index],
                                            model: model,
                                            screenName: 'shopScreen',
                                            // callMerchant: model.callStore('08101'),
                                            categoryId:
                                                model.categoryId.toString(),
                                          ),
                                        ),
                                      );
                                    }),
                              );
                            } else if (snapshot.hasError) {
                              return Column(
                                children: <Widget>[
                                  SizedBox(
                                    height: 10,
                                  ),
                                  Text(
                                    'Error fetching data',
                                    style: TextStyle(
                                      fontWeight: FontWeight.bold,
                                      fontSize: 20,
                                    ),
                                  ),
                                  SizedBox(
                                    height: 10,
                                  ),
                                  Text("${'Make sure you are connected'}"),
                                  SizedBox(
                                    height: 100,
                                  ),
                                ],
                              );
                            } else {
                              return Text(
                                tr.text('shop.noProduct'),
                                style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  fontSize: 20,
                                ),
                              );
                            }
                          }),
                    )
                  : Expanded(
                      child: FutureBuilder<List<product.Data>>(
                          //future: model.getItem(widget.categoriesDetails.id.toString()),
                          future: model.getSearchedPackages(
                              Search(searchInput: searchItem)),
                          builder: (context, snapshot) {
                            if (!snapshot.hasData) {
                              return Container(
                                  padding: EdgeInsets.all(20.0),
                                  child: Center(
                                    child: Shimmer.fromColors(
                                        direction: ShimmerDirection.ltr,
                                        period: Duration(seconds: 2),
                                        child: GridView.count(
                                          padding: EdgeInsets.all(20.w),
                                          childAspectRatio: (200.w / 330.h),
                                          crossAxisCount: 2,
                                          mainAxisSpacing: 12.0.w,
                                          crossAxisSpacing: 12.0.w,
                                          children: [0, 1, 2, 3, 4, 5, 6]
                                              .map((_) => Padding(
                                                  padding:
                                                      const EdgeInsets.only(
                                                          bottom: 8.0),
                                                  child: PackageWidget(
                                                    model: model,
                                                  )))
                                              .toList(),
                                        ),
                                        baseColor: AppColors.grey,
                                        highlightColor: Colors.white),
                                  ));
                            } else if (snapshot.data.isNotEmpty) {
                              return Container(
                                padding:
                                    EdgeInsets.only(right: 10.0, left: 10.0),
                                child: ListView.builder(
                                    itemCount: snapshot.data.length,
                                    itemBuilder: (context, index) {
                                      return PackageWidget(
                                        packageModel: snapshot.data[index],
                                        model: model,
                                      );
                                    }),
                              );
                            } else if (snapshot.hasError) {
                              return Column(
                                children: <Widget>[
                                  SizedBox(
                                    height: 10,
                                  ),
                                  Text(
                                    'Error fetching data',
                                    style: TextStyle(
                                      fontWeight: FontWeight.bold,
                                      fontSize: 20,
                                    ),
                                  ),
                                  SizedBox(
                                    height: 10,
                                  ),
                                  Text("${'Make sure you are connected'}"),
                                  SizedBox(
                                    height: 100,
                                  ),
                                ],
                              );
                            } else {
                              return Text(
                                'Not found',
                                style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  fontSize: 20,
                                ),
                              );
                            }
                          }),
                    ),
            ]),
          );
        });
  }
}
