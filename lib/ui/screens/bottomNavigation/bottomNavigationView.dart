import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider_architecture/provider_architecture.dart';
import 'package:rwandashop/ui/screens/account/account.dart';
import 'package:rwandashop/ui/screens/bottomNavigation/bottomNav_modelView.dart';
import 'package:rwandashop/ui/screens/cart/cart.dart';
import 'package:rwandashop/ui/screens/order/order_view.dart';
import 'package:rwandashop/ui/screens/shop/shop_view.dart';
import 'package:rwandashop/utils/app_localizations.dart';
import 'package:rwandashop/utils/colors.dart';

class BottomNavigation extends StatefulWidget {
  BottomNavigation() : super();
  @override
  _MyStatefulWidgetState createState() => _MyStatefulWidgetState();
}

class _MyStatefulWidgetState extends State<BottomNavigation> {
  Widget getViewForIndex(int index) {
    print('jdjdsjds:::::::::::::::::');
    print(index);
    switch (index) {
      case 0:
        return ShopPage();
      case 1:
        return CartPage();
      // case 2:
      //   return Order();
      case 2:
        return Account();
      default:
        return ShopPage();
    }
  }

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    AppLocalizations tr = AppLocalizations.of(context);

    return ViewModelProvider<AppStateProvider>.withConsumer(
        viewModelBuilder: () => AppStateProvider(),
        builder: (context, model, child) {
          return Scaffold(
            backgroundColor: AppColors.white,
            body: getViewForIndex(model.currentTabIndex),
            // pageList[model.currentTabIndex],
            // pageList[_selectedIndex],

            bottomNavigationBar: BottomNavigationBar(
              type: BottomNavigationBarType.fixed,
              // isSelected: appNotifier.currentTabIndex == 1,
              // appNotifier.setCurrentTabTo(newTabIndex: 1);
              onTap: (newTab) {
                print(newTab);
                model.setCurrentTabTo(newTabIndex: newTab);
              },
              // onTap: (newTab) => setState(() =>
              // _selectedIndex =  appNotifier.setCurrentTabTo(newTabIndex: 1),),
              currentIndex: model.currentTabIndex,
              items: [
                BottomNavigationBarItem(
                  icon: ImageIcon(
                    AssetImage(
                      'assets/images/shopIcon.png',
                    ),
                  ),
                  label: tr.text('shop.shop'),
                ),
                BottomNavigationBarItem(
                  icon: ImageIcon(
                    AssetImage('assets/images/saveIcon.png'),
                  ),
                  label: tr.text('general.saved'), //tr.text('shop.cart'),
                ),
                // BottomNavigationBarItem(
                //   icon: ImageIcon(
                //     AssetImage('assets/images/order.png'),
                //   ),
                //   label: tr.text('shop.order'),
                // ),
                BottomNavigationBarItem(
                  icon: ImageIcon(
                    AssetImage('assets/images/accountIcon.png'),
                  ),
                  label: tr.text('shop.account'),
                ),
              ],
              selectedItemColor: AppColors.green,
            ),
          );
        });
  }
}
