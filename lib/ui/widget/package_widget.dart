import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:rwandashop/core/model/product/products.dart';
import 'package:rwandashop/core/services/auth_service.dart';
import 'package:rwandashop/ui/screens/login/second_login_view.dart';
import 'package:rwandashop/ui/screens/package_details/package_details_view.dart';
import 'package:rwandashop/utils/app_localizations.dart';
import 'package:rwandashop/utils/colors.dart';
import 'package:rwandashop/utils/locator.dart';
import 'package:rwandashop/utils/screensize.dart';

// This is the widget for products(items)
class PackageWidget extends StatelessWidget {
  final Data packageModel;
  final Function onTap;
  final String categoryId;
  final oCcy = new NumberFormat("#,##0.00", "en_US");
  final dynamic model;
  final String screenName;

  // final Function callMerchant;
  final Authentication _authentication = locator<Authentication>();

  PackageWidget(
      {this.packageModel,
      this.categoryId,
      this.onTap,
      this.model,
      this.screenName}); //

  @override
  Widget build(BuildContext context) {
    AppLocalizations tr = AppLocalizations.of(context);
    return Container(
        child:
        InkWell(
            onTap: () {
              Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (BuildContext context) => PackageDetailsPage(
                          packageDetailModel: packageModel,
                          screenName: screenName,
                          categoryId: categoryId)));
              // if (onTap != null) {
              //   onTap();
              // }
            },
            child: Column(
              children: [
                Container(
                  padding: EdgeInsets.all(10.0),
                  child:
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      null != packageModel.image
                          ? Container(
                              height: Responsive.width(context) * .25,
                              width: Responsive.width(context) * .25,
                              margin: EdgeInsets.only(right: 10.0, top: 10),
                              decoration: BoxDecoration(
                                image: DecorationImage(
                                    image: CachedNetworkImageProvider(
                                        packageModel.image),
                                    fit: BoxFit.cover),
                              ),
                            )
                          : Container(
                              margin: EdgeInsets.only(right: 10.0),
                              height: Responsive.width(context) * .25,
                              width: Responsive.width(context) * .25,
                              color: Colors.grey.withOpacity(0.6),
                            ),
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Container(
                            width: Responsive.width(context) * .6,
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [

                                Container(
                                  width: Responsive.width(context) * .5,
                                  child: Text(
                                    packageModel.name != null
                                        ? packageModel.name
                                        : '',
                                    style: TextStyle(
                                        fontSize: 12.ssp,
                                        fontWeight: FontWeight.w600),
                                    maxLines: 2,
                                    overflow: TextOverflow.ellipsis,
                                    // textDirection: TextDirection.LTR,
                                    textAlign: TextAlign.justify,
                                  ),
                                ),
                                InkWell(
                                  onTap: () {
                                    if (_authentication.currentUser.isNull) {
                                      Navigator.push(
                                        context,
                                        new MaterialPageRoute(
                                            builder: (context) =>
                                                new SecondLoginPage(
                                                    pageIndex: 0)),
                                      );
                                    }
                                    model.addAndRemoveFromSavedPackages(
                                        _authentication.currentUser.id
                                            .toString(),
                                        packageModel.id.toString(),
                                        context);
                                    // model.addToCart(
                                    //     AddCart(
                                    //         userId: _authentication.currentUser.id
                                    //             .toString(),
                                    //         packageId: packageModel.id.toString(),
                                    //         quantity: '1',
                                    //         weight: '1',
                                    //         statusId: '3'),
                                    //     context);
                                  },
                                  child: Container(
                                    height: 30.h,
                                    width: 30.w,
                                    child: Image.asset(
                                        screenName == 'savedScreen'
                                            ? 'assets/images/saveIconFilled.png'
                                            : 'assets/images/saveIcon.png'),
                                  ),
                                ),
                              ],
                            ),
                          ),
                          SizedBox(
                            height: 10,
                          ),
                          Container(
                            width: Responsive.width(context) * .5,
                            child: Text(
                              packageModel.shortDescription != null
                                  ? packageModel.shortDescription
                                  : '',
                              maxLines: 2,
                              overflow: TextOverflow.ellipsis,
                              style: TextStyle(
                                  fontSize: 10.ssp, color: Color(0xff7C7C7C)),
                            ),
                          ),
                          SizedBox(
                            height: 10,
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Icon(
                                Icons.location_on_rounded,
                                size: 18,
                                color: AppColors.deepGrey,
                              ),
                              Text(
                                (null != packageModel.company.user.address)
                                    ? (packageModel.company.user.address.length >
                                    12)
                                        ? '${packageModel.company.user.address.substring(0, 12)} ...'
                                        : packageModel.company.user.address
                                    : '',
                                style: TextStyle(
                                    fontSize: 10.ssp,
                                    color: AppColors.deepGrey,
                                    fontWeight: FontWeight.w300),
                              ),
                            ],
                          ),
                          SizedBox(
                            height: 10,
                          ),
                          Container(
                            width: Responsive.width(context) * .6,
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    Icon(
                                      Icons.assignment_return,
                                      size: 18,
                                      color: Colors.grey.withOpacity(0.6),
                                    ),
                                    Text(
                                      (null != packageModel.company.name)
                                          ? (packageModel.company.name.length >
                                          12)
                                          ? '${packageModel.company.name.substring(0, 12)} ...'
                                          : packageModel.company.name
                                          : '',
                                      style: TextStyle(
                                          fontSize: 10.ssp,
                                          color: Colors.grey.withOpacity(0.9),
                                          fontWeight: FontWeight.w300),
                                    ),
                                  ],
                                ),
                                Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    packageModel.discount != 0 &&
                                            packageModel.discount != null
                                        ? Text(
                                            packageModel.price != null
                                                ? 'RF ${oCcy.format(((packageModel.price) - ((packageModel.price * packageModel.discount) / 100)))}'
                                                : '',
                                            style: TextStyle(
                                              fontWeight: FontWeight.bold,
                                              fontSize: 12.0.ssp,
                                              color: Colors.black,
                                            ),
                                            overflow: TextOverflow.ellipsis,
                                            softWrap: false,
                                            maxLines: 1,
                                          )
                                        : Text(
                                            packageModel.price != null
                                                ? 'RF ${oCcy.format((packageModel.price))}'
                                                : '',
                                            style: TextStyle(
                                                fontSize: 11.ssp,
                                                color: AppColors.themeGreen,
                                                fontWeight: FontWeight.w900),
                                          ),
                                  ],
                                ),
                              ],
                            ),
                          ),
                          SizedBox(
                            height: 15,
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
                Container(
                  decoration: BoxDecoration(
                      border: Border.all(
                    color: Colors.grey.withOpacity(0.6),
                  )),
                  child: Container(
                    height: Responsive.height(context) * .05,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        Container(
                            width: Responsive.width(context) * .4,
                            child: MaterialButton(
                              onPressed: () {
                                model.callStore(packageModel.contact);
                              },
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Container(
                                    height: 30.h,
                                    width: 30.w,
                                    child: Image.asset(
                                        'assets/images/callIconGrey.png'),
                                  ),
                                  SizedBox(
                                    width: 10,
                                  ),
                                  Text(
                                    tr.text('shop.call'),
                                    style: TextStyle(
                                        fontSize: 11.ssp,
                                        color: AppColors.deepGrey,
                                        fontWeight: FontWeight.w300),
                                  ),
                                ],
                              ),
                            )),
                        Container(
                          width: 1.0,
                          color: Colors.grey.withOpacity(0.6),
                          height: 60,
                        ),
                        Container(
                          width: Responsive.width(context) * .4,
                          child: MaterialButton(
                            child: Center(
                                child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Container(
                                  height: 30.h,
                                  width: 30.w,
                                  child:
                                      Image.asset('assets/images/viewIcon.png'),
                                ),
                                Text(
                                  tr.text('shop.view'),
                                  style: TextStyle(
                                      fontSize: 11.ssp,
                                      color: AppColors.deepGrey,
                                      fontWeight: FontWeight.w300),
                                ),
                              ],
                            )),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
                Container(
                  width: double.infinity,
                  child: Row(
                    children: [
                      Container(
                        color: AppColors.blue,
                        height: 2.0,
                        width: (Responsive.width(context) * 0.5) - 16,
                      ),
                      Container(
                        color: AppColors.green,
                        height: 2.0,
                        width: (Responsive.width(context) * 0.5) - 16,
                      ),
                    ],
                  ),
                )
              ],
            )
            // Column(
            //   crossAxisAlignment: CrossAxisAlignment.start,
            //   mainAxisAlignment: MainAxisAlignment.start,
            //   children: [
            //     // SizedBox(
            //     //   height: 15.h,
            //     // ),
            //
            //     // Center(
            //     //   child: Container(
            //     //     height: 85.w,
            //     //     width: 130.w,
            //     //     child: Image.asset('assets/images/cide.png'),
            //     //   ),
            //     // ),
            //     Center(
            //       child: null != packageModel.image ? Container(
            //         height: 85.w,
            //         width: 130.w,
            //         decoration: BoxDecoration(
            //           image: DecorationImage(
            //               image: CachedNetworkImageProvider(packageModel.image),
            //               fit: BoxFit.contain),
            //         ),
            //       ) : Container(
            //         height: 85.w,
            //         width: 130.w,
            //       ),
            //     ),
            //     // Center(
            //     //   child: Container(
            //     //     height: 100.h,
            //     //     width: 100.w,
            //     //     child: Image.network(packageModel.image),
            //     //   ),
            //     // ),
            //     SizedBox(
            //       height: 10,
            //     ),
            //     FittedBox(
            //       child: Text(
            //         packageModel.name != null ? packageModel.name : '',
            //         style:
            //             TextStyle(fontSize: 16.ssp, fontWeight: FontWeight.w600),
            //       ),
            //     ),
            //     SizedBox(
            //       height: 11,
            //     ),
            //     Text(
            //       packageModel.shortDescription != null
            //           ? packageModel.shortDescription
            //           : '',
            //       maxLines: 2, overflow: TextOverflow.ellipsis,
            //       style: TextStyle(fontSize: 10.ssp, color: Color(0xff7C7C7C)),
            //     ),
            //
            //     SizedBox(
            //       height: 16,
            //     ),
            //     Row(
            //       mainAxisAlignment: MainAxisAlignment.spaceBetween,
            //       children: [
            //         packageModel.discount != 0 && packageModel.discount != null
            //             ? Text(
            //                 packageModel.price != null
            //                     ? 'RF ${oCcy.format(((packageModel.price) - ((packageModel.price * packageModel.discount) / 100)))}'
            //                     : '',
            //                 style: TextStyle(
            //                   fontWeight: FontWeight.bold,
            //                   fontSize: 12.0.ssp,
            //                   color: Colors.black,
            //                 ),
            //                 overflow: TextOverflow.ellipsis,
            //                 softWrap: false,
            //                 maxLines: 1,
            //               )
            //             : Text(
            //                 packageModel.price != null
            //                     ? 'RF ${oCcy.format((packageModel.price))}'
            //                     : '',
            //                 style: TextStyle(
            //                     fontSize: 11.ssp, fontWeight: FontWeight.w900),
            //               ),
            //
            //         Container(
            //             padding: EdgeInsets.only(right: 2),
            //             child: InkWell(
            //               onTap: () {
            //                 if (_authentication.currentUser.isNull) {
            //                   Navigator.push(
            //                     context,
            //                     new MaterialPageRoute(
            //                         builder: (context) =>
            //                         new SecondLoginPage(pageIndex: 0)),
            //                   );
            //                 }
            //                 model.addToCart(
            //                     AddCart(
            //                         userId:
            //                         _authentication.currentUser.id.toString(),
            //                         packageId: packageModel.id.toString(),
            //                         quantity: '1',
            //                         weight: '1',
            //                         statusId: '3'),
            //                     context);
            //               },
            //               child: Container(
            //                 height: 40.h,
            //                 width: 40.w,
            //                 child: Image.asset('assets/images/add.png'),
            //               ),
            //             )),
            //
            //         // packageModel.discount != 0 && packageModel.discount != null
            //         //     ? Text(
            //         //         packageModel.price != null
            //         //             ? 'RF ${oCcy.format(packageModel.price)}'
            //         //             : '',
            //         //         style: TextStyle(
            //         //           fontWeight: FontWeight.bold,
            //         //           fontSize: 10.0.ssp,
            //         //           decoration: TextDecoration.lineThrough,
            //         //           color: Colors.grey,
            //         //         ),
            //         //       )
            //         //     : Container()
            //
            //         // Container(
            //         //   height: 30,
            //         //   width: 80,
            //         //   decoration: new BoxDecoration(
            //         //       color: discountPink2,
            //         //       borderRadius: new BorderRadius.only(
            //         //           topLeft:  const  Radius.circular(40.0),
            //         //           bottomLeft: const  Radius.circular(40.0))
            //         //   ),
            //         //   child:
            //         //   Center(
            //         //       child:
            //         //       Text(feed.discount != null ? '-${feed.discount.toString()} % OFF': '', style: TextStyle(color: discountPink, fontWeight: FontWeight.bold,),)
            //         //   ),
            //         // )
            //
            //         //     ]
            //         //
            //         // ): Container(),
            //       ],
            //     ),
            //     SizedBox(
            //       height: 16,
            //     ),
            //     // Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
            //     //   // Text(''),
            //     //   Container(
            //     //       padding: EdgeInsets.only(right: 2),
            //     //       child: InkWell(
            //     //         onTap: () {
            //     //           if (_authentication.currentUser.isNull) {
            //     //             Navigator.push(
            //     //               context,
            //     //               new MaterialPageRoute(
            //     //                   builder: (context) =>
            //     //                       new SecondLoginPage(pageIndex: 0)),
            //     //             );
            //     //           }
            //               model.addToCart(
            //                   AddCart(
            //                       userId:
            //                           _authentication.currentUser.id.toString(),
            //                       packageId: packageModel.id.toString(),
            //                       quantity: '1',
            //                       weight: '1',
            //                       statusId: '3'),
            //                   context);
            //             },
            //     //         child: Container(
            //     //           height: 40.h,
            //     //           width: 40.w,
            //     //           child: Image.asset('assets/images/add.png'),
            //     //         ),
            //     //       )),
            //     // ])
            //   ],
            // )),ß
            ));
  }
}
