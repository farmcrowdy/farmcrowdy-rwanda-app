import 'package:flutter/material.dart';
import 'package:provider_architecture/provider_architecture.dart';
import 'package:rwandashop/ui/screens/register/registerViewModel.dart';
import 'package:rwandashop/ui/widget/app_checkBox.dart';
import 'package:rwandashop/ui/widget/generalButton.dart';
import 'package:rwandashop/utils/colors.dart';
import 'package:rwandashop/utils/locator.dart';
import 'package:rwandashop/utils/router/navigationService.dart';
import 'package:rwandashop/utils/screensize.dart';

class FeaturesWidget extends StatefulWidget {
  const FeaturesWidget({Key key, this.onChanged, this.featureValue})
      : super(key: key);

  final VoidCallback onChanged;
  final List featureValue;
  @override
  _FeaturesWidgetState createState() => _FeaturesWidgetState();
}

class _FeaturesWidgetState extends State<FeaturesWidget> {
  final NavigationService _navigationService = locator<NavigationService>();
  bool event = false;
  bool live = false;
  bool products = false;
  List<String> featureValue = [];
  @override
  Widget build(BuildContext context) {
    return ViewModelProvider<RegisterViewModel>.withConsumer(
    viewModelBuilder: () => RegisterViewModel(),
    builder: (context, model, child) {
      return Scaffold(
          body: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
            SizedBox(
            height: 80,
          ),
          Container(
            padding: EdgeInsets.only(left: 20, right: 20),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                InkWell(
                  onTap: () {
                    _navigationService.pop();
                  },
                  child: Icon(
                    Icons.cancel_outlined,
                    color: Colors.black,
                  ),
                ),
                Container(
                  child: Text(
                    'Filters',
                    style: TextStyle(
                        fontSize: 18, fontWeight: FontWeight.bold),
                  ),
                ),
                Text('')
              ],
            ),
          ),
          SizedBox(height: 20),
        Container(
          padding: EdgeInsets.all(20),
          width: Responsive.width(context),
          height: Responsive.height(context)/1.2,
          decoration: BoxDecoration(
              color: Color(0xffF2F3F2),
              borderRadius: BorderRadius.circular(20.0),
              border: Border.all(
                color: Color(0xffF2F3F2),
              )),
          child:
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Text('Filter by Brand',style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),),

              SizedBox(height: 20.0),
                  AppCheckBox(
                    label: 'Dow DuPont',
                    value: widget.featureValue.contains('Dow DuPont')
                        ? event = true
                        : false,
                    onChanged: (val) {
                      setState(() {
                        print(val);
                        event = val;
                        if (val) {
                          if (!widget.featureValue.contains('Dow DuPont')) {
                            widget.featureValue.add('Dow DuPont');
                            print(widget.featureValue);
                          }
                        } else {
                          widget.featureValue.remove('Dow DuPont');
                        }
                      });
                    },
                  ),
                  SizedBox(height: 15.0),
                  AppCheckBox(
                    label: 'Adama India Limited',
                    value: widget.featureValue.contains('Adama India Limited')
                        ? live = true
                        : false,
                    onChanged: (val) {
                      setState(() {
                        print(val);
                        live = val;
                        if (val) {
                          if (!widget.featureValue.contains('Adama India Limited')) {
                            widget.featureValue.add('Adama India Limited');
                            print(widget.featureValue);
                          }
                        } else {
                          widget.featureValue.remove('Adama India Limited');
                        }
                      });
                    },
                  ),
                  SizedBox(height: 15.0),
                  AppCheckBox(
                    label: 'BASF India Limited',
                    value: widget.featureValue.contains('BASF India Limited')
                        ? products = true
                        : false,
                    onChanged: (val) {
                      setState(() {
                        print(val);
                        products = val;
                        if (val) {
                          if (!widget.featureValue.contains('BASF India Limited')) {
                            widget.featureValue.add('BASF India Limited');
                            print(widget.featureValue);
                          }
                        } else {
                          widget.featureValue.remove('BASF India Limited');
                        }
                      });
                    },
                  ),
              SizedBox(height: 15.0),
              AppCheckBox(
                label: 'BASF India Limited',
                value: widget.featureValue.contains('BASF India Limited')
                    ? products = true
                    : false,
                onChanged: (val) {
                  setState(() {
                    print(val);
                    products = val;
                    if (val) {
                      if (!widget.featureValue.contains('BASF India Limited')) {
                        widget.featureValue.add('BASF India Limited');
                        print(widget.featureValue);
                      }
                    } else {
                      widget.featureValue.remove('BASF India Limited');
                    }
                  });
                },
              ),
              SizedBox(height: 15.0),
              AppCheckBox(
                label: 'BASF India Limited',
                value: widget.featureValue.contains('BASF India Limited')
                    ? products = true
                    : false,
                onChanged: (val) {
                  setState(() {
                    print(val);
                    products = val;
                    if (val) {
                      if (!widget.featureValue.contains('BASF India Limited')) {
                        widget.featureValue.add('BASF India Limited');
                        print(widget.featureValue);
                      }
                    } else {
                      widget.featureValue.remove('BASF India Limited');
                    }
                  });
                },
              ),
              SizedBox(height: 15.0),
              AppCheckBox(
                label: 'BASF India Limited',
                value: widget.featureValue.contains('BASF India Limited')
                    ? products = true
                    : false,
                onChanged: (val) {
                  setState(() {
                    print(val);
                    products = val;
                    if (val) {
                      if (!widget.featureValue.contains('BASF India Limited')) {
                        widget.featureValue.add('BASF India Limited');
                        print(widget.featureValue);
                      }
                    } else {
                      widget.featureValue.remove('BASF India Limited');
                    }
                  });
                },
              ),
              SizedBox(height: 15.0),
              AppCheckBox(
                label: 'BASF India Limited',
                value: widget.featureValue.contains('BASF India Limited')
                    ? products = true
                    : false,
                onChanged: (val) {
                  setState(() {
                    print(val);
                    products = val;
                    if (val) {
                      if (!widget.featureValue.contains('BASF India Limited')) {
                        widget.featureValue.add('BASF India Limited');
                        print(widget.featureValue);
                      }
                    } else {
                      widget.featureValue.remove('BASF India Limited');
                    }
                  });
                },
              ),
              SizedBox(height: 15.0),
              AppCheckBox(
                label: 'BASF India Limited',
                value: widget.featureValue.contains('BASF India Limited')
                    ? products = true
                    : false,
                onChanged: (val) {
                  setState(() {
                    print(val);
                    products = val;
                    if (val) {
                      if (!widget.featureValue.contains('BASF India Limited')) {
                        widget.featureValue.add('BASF India Limited');
                        print(widget.featureValue);
                      }
                    } else {
                      widget.featureValue.remove('BASF India Limited');
                    }
                  });
                },
              ),
              SizedBox(height: 15.0),
              AppCheckBox(
                label: 'BASF India Limited',
                value: widget.featureValue.contains('BASF India Limited')
                    ? products = true
                    : false,
                onChanged: (val) {
                  setState(() {
                    print(val);
                    products = val;
                    if (val) {
                      if (!widget.featureValue.contains('BASF India Limited')) {
                        widget.featureValue.add('BASF India Limited');
                        print(widget.featureValue);
                      }
                    } else {
                      widget.featureValue.remove('BASF India Limited');
                    }
                  });
                },
              ),
              SizedBox(height: 15.0),
              AppCheckBox(
                label: 'BASF India Limited',
                value: widget.featureValue.contains('BASF India Limited')
                    ? products = true
                    : false,
                onChanged: (val) {
                  setState(() {
                    print(val);
                    products = val;
                    if (val) {
                      if (!widget.featureValue.contains('BASF India Limited')) {
                        widget.featureValue.add('BASF India Limited');
                        print(widget.featureValue);
                      }
                    } else {
                      widget.featureValue.remove('BASF India Limited');
                    }
                  });
                },
              ),
              SizedBox(height: 15.0),
              AppCheckBox(
                label: 'BASF India Limited',
                value: widget.featureValue.contains('BASF India Limited')
                    ? products = true
                    : false,
                onChanged: (val) {
                  setState(() {
                    print(val);
                    products = val;
                    if (val) {
                      if (!widget.featureValue.contains('BASF India Limited')) {
                        widget.featureValue.add('BASF India Limited');
                        print(widget.featureValue);
                      }
                    } else {
                      widget.featureValue.remove('BASF India Limited');
                    }
                  });
                },
              ),
                  SizedBox(height: 30.0),
                  Container(
                      padding: EdgeInsets.only(left: 20, right: 20),
                      height: 50,
                      child: GeneralButton(
                        buttonText: 'Apply Filter',
                        onPressed: () {
                          // _navigationService.navigateTo(LoginRoute);
                        },
                      )),
                ],
              ),
            ),

      ]));
    });


  }
}
