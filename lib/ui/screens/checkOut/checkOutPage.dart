import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider_architecture/provider_architecture.dart';
import 'package:rwandashop/ui/screens/bottomNavigation/bottomNav_modelView.dart';
import 'package:rwandashop/ui/screens/register/registerViewModel.dart';
import 'package:rwandashop/ui/widget/generalButton.dart';
import 'package:rwandashop/utils/colors.dart';
import 'package:rwandashop/utils/locator.dart';
import 'package:rwandashop/utils/router/navigationService.dart';
import 'package:rwandashop/utils/screensize.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
class CheckOutPage extends StatefulWidget {
  @override
  CheckOutPageState createState() => new CheckOutPageState();
}

class CheckOutPageState extends State<CheckOutPage> {
  TextEditingController numberController = TextEditingController();

  final NavigationService _navigationService = locator<NavigationService>();
  bool passwordVisible;
  String gender = 'Select Zone';
  final AppStateProvider appStateProvider = locator<AppStateProvider>();
  @override
  void initState() {
    super.initState();
  }

  String radioItem = '';
  //
  @override
  Widget build(BuildContext context) {
    return ViewModelProvider<RegisterViewModel>.withConsumer(
        viewModelBuilder: () => RegisterViewModel(),
        builder: (context, model, child) {

          return Scaffold(
              body:ListView(children: <Widget>[
                Container(
                  padding: EdgeInsets.all(20.w),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(
                        'Checkout',
                        style: TextStyle(
                            fontWeight: FontWeight.bold, fontSize: 18),
                      ),
                      InkWell(
                          onTap: () {
                            _navigationService.pop();
                          },
                          child: Container(
                            height: 15.h,
                            width: 15.h,
                            child: Image.asset('assets/images/cancel.png'),
                          )),
                    ],
                  ),
                ),
                Container(
                  //padding: EdgeInsets.all(20),
                  child: ExpansionTile(
                    title: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(
                          'Delivery',
                          style: TextStyle(
                              fontWeight: FontWeight.bold,
                              color: AppColors.lowGrey,
                              fontSize: 14.ssp),
                        ),
                        Text('Select Method',
                            style: TextStyle(
                                fontWeight: FontWeight.bold,
                                color: AppColors.black,
                                fontSize: 14)),
                      ],
                    ),
                    children: <Widget>[
                      Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          Theme(
                            child: Radio(
                              activeColor: AppColors.themeGreen,
                              groupValue: radioItem,
                              // title: Text('12 hours',style: TextStyle(color: Colors.black54,fontSize:15)),
                              value: 'Door Delivery',
                              onChanged: (val) {
                                setState(() {
                                  radioItem = val;
                                });
                              },
                            ),
                            data: ThemeData(
                              unselectedWidgetColor: AppColors.themeGreen,
                            ),
                          ),
                          Expanded(
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: [
                                Text('Door Delivery',
                                    style: TextStyle(
                                        color: Color(0xff3E4A59),
                                        fontSize: 14.ssp,
                                        fontWeight: FontWeight.bold)),
                                SizedBox(
                                  height: 10.h,
                                ),
                                Container(
                                  padding: EdgeInsets.only(right: 10.w),
                                  child: Text(
                                    'Delivered between Friday 25 Dec and Tuesday 29 Dec for RF4.99',
                                    style: TextStyle(
                                      fontSize: 12.ssp,
                                    ),
                                  ),
                                ),
                                SizedBox(
                                  height: 10.h,
                                ),
                                Container(
                                    padding: EdgeInsets.all(10.w),
                                    width: Responsive.width(context) / 1.3,
                                    height: 60.h,
                                    decoration: BoxDecoration(
                                        color: Color(0xffF2F3F2),
                                        borderRadius:
                                            BorderRadius.circular(7.0.r),
                                        border: Border.all(
                                          color: Color(0xffF2F3F2),
                                        )),
                                    child: Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      mainAxisAlignment:
                                          MainAxisAlignment.start,
                                      children: [
                                        Text(
                                          'Door Delivery',
                                          style: TextStyle(
                                              fontSize: 12.ssp,
                                              fontWeight: FontWeight.bold,
                                              color: Color(0xff7C7C7C)),
                                        ),
                                        SizedBox(
                                          height: 5.h,
                                        ),
                                        Text(
                                          '8 KN 4 AV, Centenary House, Kigali',
                                          style: TextStyle(
                                              fontSize: 12.ssp,
                                              fontWeight: FontWeight.bold),
                                        ),
                                      ],
                                    )),
                              ],
                            ),
                          ),
                        ],
                      ),
                      SizedBox(
                        height: 20.h,
                      ),
                      Row(
                        children: [
                          Theme(
                            child: Radio(
                              activeColor: AppColors.themeGreen,
                              groupValue: radioItem,
                              // title: Text('12 hours',style: TextStyle(color: Colors.black54,fontSize:15)),
                              value: 'Pickup Station',
                              onChanged: (val) {
                                setState(() {
                                  radioItem = val;
                                });
                              },
                            ),
                            data: ThemeData(
                              unselectedWidgetColor: AppColors.themeGreen,
                            ),
                          ),
                          Expanded(
                              child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: [
                              Text('Pickup Station (Cheaper Shipping Fees)',
                                  style: TextStyle(
                                      color: Color(0xff3E4A59),
                                      fontSize: 14.ssp,
                                      fontWeight: FontWeight.bold)),
                              SizedBox(
                                height: 10.h,
                              ),
                              Container(
                                padding: EdgeInsets.only(right: 10.w),
                                child: Text(
                                  'Ready for pickup between Friday 25 Dec to Tuesday 29 Dec with cheaper shipping fees',
                                  style: TextStyle(
                                    fontSize: 12.ssp,
                                  ),
                                ),
                              ),
                            ],
                          )),
                        ],
                      ),
                      SizedBox(
                        height: 10.h,
                      )
                    ],
                  ),
                ),
                Container(
                  //padding: EdgeInsets.all(20),
                  child: ExpansionTile(
                    title: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(
                          'Payment Method',
                          style: TextStyle(
                              fontWeight: FontWeight.bold,
                              color: AppColors.lowGrey,
                              fontSize: 14.ssp),
                        ),
                        Container(
                          height: 20.w,
                          width: 20.w,
                          child: Image.asset('assets/images/card.png'),
                        ),
                      ],
                    ),
                    children: <Widget>[
                      Row(
                        children: [
                          Theme(
                            child: Radio(
                              activeColor: AppColors.themeGreen,
                              groupValue: radioItem,
                              // title: Text('12 hours',style: TextStyle(color: Colors.black54,fontSize:15)),
                              value: 'Pay with Mobile Money',
                              onChanged: (val) {
                                setState(() {
                                  radioItem = val;
                                });
                              },
                            ),
                            data: ThemeData(
                              unselectedWidgetColor: AppColors.themeGreen,
                            ),
                          ),
                          Text('Pay with Mobile Money',
                              style: TextStyle(
                                  color: Color(0xff3E4A59),
                                  fontSize: 14,
                                  fontWeight: FontWeight.w400)),
                        ],
                      ),
                      Container(
                          padding: EdgeInsets.only(left: 50.w, bottom: 20.w),
                          child: Row(
                            children: [
                              Container(
                                height: 50.w,
                                width: 50.w,
                                child: Image.asset('assets/images/mtn.png'),
                              ),
                              Container(
                                height: 50.w,
                                width: 50.w,
                                child: Image.asset('assets/images/airtel.png'),
                              ),
                            ],
                          )),
                    ],
                  ),
                ),
                Container(
                  //padding: EdgeInsets.all(20),
                  child: ExpansionTile(
                    title: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(
                          'Promo Code',
                          style: TextStyle(
                              fontWeight: FontWeight.bold,
                              color: AppColors.lowGrey,
                              fontSize: 14.ssp),
                        ),
                        Text('Pick discount',
                            style: TextStyle(
                                fontWeight: FontWeight.bold,
                                color: AppColors.black,
                                fontSize: 14.ssp)),
                      ],
                    ),
                    children: <Widget>[
                      ListTile(
                        title: Text('data'),
                      )
                    ],
                  ),
                ),
                Container(
                  padding: EdgeInsets.all(20.w),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(
                        'Total Cost',
                        style: TextStyle(
                          fontSize: 14.ssp,
                          fontWeight: FontWeight.bold,
                          color: AppColors.lowGrey,
                        ),
                      ),
                      Row(
                        children: [
                          Text(
                            '13.97',
                            style: TextStyle(
                                fontSize: 14.ssp, fontWeight: FontWeight.bold),
                          ),
                          Icon(
                            Icons.arrow_forward_ios,
                            color: Colors.black,
                            size: 18.w,
                          )
                        ],
                      )
                    ],
                  ),
                ),
                Divider(
                  thickness: 1,
                ),
                Container(
                  padding: EdgeInsets.all(20.w),
                  child: Text(
                    'By placing an order you agree to our Terms And Conditions',
                    style: TextStyle(
                      fontSize: 12.ssp,
                    ),
                  ),
                ),
                SizedBox(
                  height: 40.h,
                ),
                Container(
                    padding: EdgeInsets.only(left: 20.w, right: 20.w),
                    height: 67.h,
                    child: GeneralButton(
                      buttonText: 'Place Order',
                      onPressed: () {
                        // _navigationService.navigateTo(LoginRoute);
                      },
                    )),
              ]),
          );
        });
  }
}
