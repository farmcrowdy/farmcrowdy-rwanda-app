import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:rwandashop/core/model/product/add_cart.dart';
import 'package:rwandashop/core/model/error_model.dart';
import 'package:rwandashop/core/model/product/products.dart';
import 'package:rwandashop/core/model/product/search.dart';
import 'package:rwandashop/core/model/success_model.dart';
import 'package:rwandashop/core/services/products_service.dart';
import 'package:rwandashop/utils/app_localizations.dart';
import 'package:rwandashop/utils/baseModel.dart';
import 'package:rwandashop/utils/colors.dart';
import 'package:rwandashop/utils/helpers.dart';
import 'package:rwandashop/utils/locator.dart';
import 'package:rwandashop/utils/router/navigationService.dart';
import 'package:url_launcher/url_launcher.dart';

class PackageViewModel extends BaseModel {
  final Product _product = locator<Product>();
  final NavigationService _navigationService = locator<NavigationService>();
  String _searchName;
  String get searchName => _searchName;


  callStore(String contact) async {
    var  url = "tel:$contact";
    if (await canLaunch(url)) {
      print(url);
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }

  Future<List<Data>> getItem(String id) async {
    //setBusy(true);
    var result = await _product.getPackages( id);
    if (result is ErrorModel) {
      // showToast('Login failed');
      print(result.error);
      notifyListeners();
      throw Exception('Failed to load internet');
      //return ErrorModel(result.error);
    }
    print('HDHDHL:::::::::::::::::::::::');
    print(result);
    return result;
  }
  Future<List<Data>> getSearchedPackages(Search search) async {
    //setBusy(true);
    var result = await _product.getSearchedPackages(search.toJson());
    if (result is ErrorModel) {
      // showToast('Login failed');
      print(result.error);
      notifyListeners();
      throw Exception('Failed to load internet');
      //return ErrorModel(result.error);
    }
    print('HDHDHL:::::::::::::::::::::::');
    print(result);
    return result;
  }
  initiateSearch(String val,PackageViewModel model ) {
    if (val.length > 1 && val.isNotEmpty) {

      model.getSearchedPackages(Search(searchInput: val));
      notifyListeners();
      _searchName = val;

    } else {
      notifyListeners();
      _searchName = null;

    }
  }
  getCartSummary() async {
    //  loading = true;
    var result = await _product.getCartSummary();
    if (result is ErrorModel) {
      //  loading = false;
      showErrorToast(result.error);
      print(result.error);
      notifyListeners();
      return ErrorModel(result.error);
    }
    if (result is SuccessModel) {
      print('SUMMARY:::::::::::');
      print(result.data);
      //loading = false;
      //cart = result.data;
      notifyListeners();
      // return categories;
    }
  }
  addToCart(AddCart addCart, BuildContext context) async {
    AppLocalizations tr = AppLocalizations.of(context);
    setBusy(true);
    var result = await _product.addToCart(addCart.toJson());

    if (result is ErrorModel) {
      setBusy(false);
      showErrorToast(result.error);
      print(result.error);
      notifyListeners();
      return ErrorModel(result.error);
    }
    if (result is SuccessModel) {
      setBusy(false);
      Get.snackbar(tr.text('shop.added'),result.data['message'],backgroundColor: AppColors.green,
      );
      getCartSummary();
      //_product.getCartSummary();
     // Get.back();
      //setBusy(false);
    // _navigationService.navigateTo(BottomNavigationRoute);

      notifyListeners();
      //_navigationService.pop();
      // return SuccessModel(result.data);
    }
  }
}
