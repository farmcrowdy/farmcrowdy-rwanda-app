import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:rwandashop/core/model/product/catergories.dart';
import 'package:rwandashop/utils/colors.dart';


// This is the widget for Merchant circle avatar on the home page
class MerchantsItem extends StatelessWidget {
  final Categories categoriesModel;
  final bool active;
  final Function onTap;

  MerchantsItem({this.categoriesModel, this.active = false, this.onTap});

  @override
  Widget build(BuildContext context) {
    return InkWell(
        onTap: () {
          print(categoriesModel.id);
          if (onTap != null) {
            onTap();
          }
        },
        child: Container(
            padding: EdgeInsets.all(10),
            child: Column(children: [
              CircleAvatar(
                  radius: 30,
                  backgroundColor: active ? AppColors.green : AppColors.grey,
                  child: active
                      ? CircleAvatar(
                    radius: 30,
                    backgroundColor: active? Colors.teal: AppColors.lowGrey,
                    child: CircleAvatar(
                      backgroundImage:CachedNetworkImageProvider(
                          categoriesModel.image),
                      radius: 28,
                    ),
                  )
                      : ColorFiltered(
                          colorFilter: ColorFilter.mode(
                            Colors.grey[100],
                            BlendMode.saturation,
                          ),
                          child:    CircleAvatar(
                            radius: 30,
                            backgroundColor: active? Colors.teal: AppColors.lowGrey,
                            child: CircleAvatar(
                              backgroundImage:CachedNetworkImageProvider(
                                  categoriesModel.image),
                              radius: 28,
                            ),
                          ),
                        )),

              SizedBox(
                height: 10,
              ),
              Flexible(
                  child: Text(
                categoriesModel.name.length > 9
                    ? categoriesModel.name.substring(0, 9) + '...'
                    : categoriesModel.name,
                style: TextStyle(
                    color: active? Colors.black : Colors.grey,
                    fontSize: 15,
                    fontWeight: FontWeight.w500),
              ))
            ])));
  }
}
