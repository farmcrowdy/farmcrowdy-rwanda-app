/// phone_number : ""
/// zone_id : ""
/// password : ""
/// city_id : ""

class FinalReg {
  String _phoneNumber;
  int _zoneId;
  String _password;
  int _cityId;
  int _sectorId;

  String get phoneNumber => _phoneNumber;
  int get zoneId => _zoneId;
  String get password => _password;
  int get cityId => _cityId;
  int get sectorId => _sectorId;

  FinalReg({
      String phoneNumber,
    int zoneId,
      String password,
    int cityId,
    int sectorId,

  }){
    _phoneNumber = phoneNumber;
    _zoneId = zoneId;
    _password = password;
    _cityId = cityId;
    _sectorId = sectorId;
}

  FinalReg.fromJson(dynamic json) {
    _phoneNumber = json["phone_number"];
    _zoneId = json["zone_id"];
    _password = json["password"];
    _cityId = json["city_id"];
    _sectorId = json["sector_id"];
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map["phone_number"] = _phoneNumber;
    map["zone_id"] = _zoneId;
    map["password"] = _password;
    map["city_id"] = _cityId;
    map["sector_id"] = _sectorId;
    return map;
  }

}