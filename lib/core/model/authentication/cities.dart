/// status : true
/// message : "Cities retrieved successfully."
/// data : [{"id":1,"zone_id":1,"active":1,"name":"Gasabo","created_at":null,"updated_at":null},{"id":2,"zone_id":1,"active":1,"name":"Kicukiro","created_at":null,"updated_at":null},{"id":3,"zone_id":1,"active":1,"name":"Nyarugenge","created_at":null,"updated_at":null},{"id":4,"zone_id":2,"active":1,"name":"Burera","created_at":null,"updated_at":null},{"id":5,"zone_id":2,"active":1,"name":"Gakenke","created_at":null,"updated_at":null},{"id":6,"zone_id":2,"active":1,"name":"Gicumbi","created_at":null,"updated_at":null},{"id":7,"zone_id":2,"active":1,"name":"Musanze","created_at":null,"updated_at":null},{"id":8,"zone_id":2,"active":1,"name":"Rulindo","created_at":null,"updated_at":null},{"id":9,"zone_id":3,"active":1,"name":"Bugesera","created_at":null,"updated_at":null},{"id":10,"zone_id":3,"active":1,"name":"Gatsibo","created_at":null,"updated_at":null},{"id":11,"zone_id":3,"active":1,"name":"Kayonza","created_at":null,"updated_at":null},{"id":12,"zone_id":3,"active":1,"name":"Kirehe","created_at":null,"updated_at":null},{"id":13,"zone_id":3,"active":1,"name":"Ngoma","created_at":null,"updated_at":null},{"id":14,"zone_id":3,"active":1,"name":"Nyagatare","created_at":null,"updated_at":null},{"id":15,"zone_id":3,"active":1,"name":"Rwamagana","created_at":null,"updated_at":null},{"id":16,"zone_id":4,"active":1,"name":"Karongi","created_at":null,"updated_at":null},{"id":17,"zone_id":4,"active":1,"name":"Ngororero","created_at":null,"updated_at":null},{"id":18,"zone_id":4,"active":1,"name":"Nyabihu","created_at":null,"updated_at":null},{"id":19,"zone_id":4,"active":1,"name":"Nyamasheke","created_at":null,"updated_at":null},{"id":20,"zone_id":4,"active":1,"name":"Rubavu","created_at":null,"updated_at":null},{"id":21,"zone_id":4,"active":1,"name":"Rusizi","created_at":null,"updated_at":null},{"id":22,"zone_id":4,"active":1,"name":"Rutsiro","created_at":null,"updated_at":null},{"id":23,"zone_id":5,"active":1,"name":"Gisagara","created_at":null,"updated_at":null},{"id":24,"zone_id":5,"active":1,"name":"Huye","created_at":null,"updated_at":null},{"id":25,"zone_id":5,"active":1,"name":"Kamonyi","created_at":null,"updated_at":null},{"id":26,"zone_id":5,"active":1,"name":"Muhanga","created_at":null,"updated_at":null},{"id":27,"zone_id":5,"active":1,"name":"Nyamagabe","created_at":null,"updated_at":null},{"id":28,"zone_id":5,"active":1,"name":"Nyanza","created_at":null,"updated_at":null},{"id":29,"zone_id":5,"active":1,"name":"Nyaruguru","created_at":null,"updated_at":null},{"id":30,"zone_id":5,"active":1,"name":"Ruhango","created_at":null,"updated_at":null}]

class Cities {
  bool _status;
  String _message;
  List<Data> _data;

  bool get status => _status;
  String get message => _message;
  List<Data> get data => _data;

  Cities({
      bool status, 
      String message, 
      List<Data> data}){
    _status = status;
    _message = message;
    _data = data;
}

  Cities.fromJson(dynamic json) {
    _status = json["status"];
    _message = json["message"];
    if (json["data"] != null) {
      _data = [];
      json["data"].forEach((v) {
        _data.add(Data.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map["status"] = _status;
    map["message"] = _message;
    if (_data != null) {
      map["data"] = _data.map((v) => v.toJson()).toList();
    }
    return map;
  }

}

/// id : 1
/// zone_id : 1
/// active : 1
/// name : "Gasabo"
/// created_at : null
/// updated_at : null

class Data {
  int _id;
  int _zoneId;
  int _active;
  String _name;
  dynamic _createdAt;
  dynamic _updatedAt;

  int get id => _id;
  int get zoneId => _zoneId;
  int get active => _active;
  String get name => _name;
  dynamic get createdAt => _createdAt;
  dynamic get updatedAt => _updatedAt;

  Data({
      int id, 
      int zoneId, 
      int active, 
      String name, 
      dynamic createdAt, 
      dynamic updatedAt}){
    _id = id;
    _zoneId = zoneId;
    _active = active;
    _name = name;
    _createdAt = createdAt;
    _updatedAt = updatedAt;
}

  Data.fromJson(dynamic json) {
    _id = json["id"];
    _zoneId = json["zone_id"];
    _active = json["active"];
    _name = json["name"];
    _createdAt = json["created_at"];
    _updatedAt = json["updated_at"];
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map["id"] = _id;
    map["zone_id"] = _zoneId;
    map["active"] = _active;
    map["name"] = _name;
    map["created_at"] = _createdAt;
    map["updated_at"] = _updatedAt;
    return map;
  }

}