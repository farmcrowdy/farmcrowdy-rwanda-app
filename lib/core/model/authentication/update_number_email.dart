/// phone_number : "08169545793"
/// email : "mma@gmail.com"

class UpdateNumberEmail {
  String _phoneNumber;
  String _email;

  String get phoneNumber => _phoneNumber;
  String get email => _email;

  UpdateNumberEmail({
      String phoneNumber, 
      String email}){
    _phoneNumber = phoneNumber;
    _email = email;
}

  UpdateNumberEmail.fromJson(dynamic json) {
    _phoneNumber = json["phone_number"];
    _email = json["email"];
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map["phone_number"] = _phoneNumber;
    map["email"] = _email;
    return map;
  }

}