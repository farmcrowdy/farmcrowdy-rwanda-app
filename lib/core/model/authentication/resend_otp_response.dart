/// status : true
/// message : "OTP sent!"
/// data : {"otp":90774,"phone_number":"08169545792"}

class ResendOtpResponse {
  bool _status;
  String _message;
  Data _data;

  bool get status => _status;
  String get message => _message;
  Data get data => _data;

  ResendOtpResponse({
      bool status, 
      String message, 
      Data data}){
    _status = status;
    _message = message;
    _data = data;
}

  ResendOtpResponse.fromJson(dynamic json) {
    _status = json["status"];
    _message = json["message"];
    _data = json["data"] != null ? Data.fromJson(json["data"]) : null;
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map["status"] = _status;
    map["message"] = _message;
    if (_data != null) {
      map["data"] = _data.toJson();
    }
    return map;
  }

}

/// otp : 90774
/// phone_number : "08169545792"

class Data {
  int _otp;
  String _phoneNumber;

  int get otp => _otp;
  String get phoneNumber => _phoneNumber;

  Data({
      int otp, 
      String phoneNumber}){
    _otp = otp;
    _phoneNumber = phoneNumber;
}

  Data.fromJson(dynamic json) {
    _otp = json["otp"];
    _phoneNumber = json["phone_number"];
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map["otp"] = _otp;
    map["phone_number"] = _phoneNumber;
    return map;
  }

}