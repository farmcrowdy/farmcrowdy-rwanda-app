import 'package:get_it/get_it.dart';
import 'package:rwandashop/core/services/analytics_service.dart';
import 'package:rwandashop/core/services/auth_service.dart';
import 'package:rwandashop/core/services/language_services.dart';
import 'package:rwandashop/core/services/products_service.dart';
import 'package:rwandashop/ui/screens/bottomNavigation/bottomNav_modelView.dart';
import 'package:rwandashop/ui/screens/language_screen/language_view_model.dart';
import 'package:rwandashop/utils/progressBarManager/dialogService.dart';
import 'package:rwandashop/utils/router/navigationService.dart';
// This injects service to app and make it available through the app
GetIt locator = GetIt.instance;

Future<void> setupLocator() async {
  locator.registerLazySingleton(() => NavigationService());
  locator.registerLazySingleton(() => AppStateProvider());
  locator.registerLazySingleton(() => ProgressService());
  locator.registerLazySingleton(() => Authentication());
  locator.registerLazySingleton(() => Product());
  locator.registerLazySingleton(() => LanguageService());
  locator.registerLazySingleton(() => Analytics());
  locator.registerLazySingleton(() => LanguageScreenViewModel());

  // locator.registerLazySingleton(() => ApiService());
}
