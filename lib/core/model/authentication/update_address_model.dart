/// zone_id : 2
/// address : "ssss"
/// city_id : 2

class UpdateAddressModel {
  int _zoneId;
  String _address;
  int _cityId;
  int _sectorId;

  int get zoneId => _zoneId;
  String get address => _address;
  int get cityId => _cityId;
  int get sectorId => _sectorId;

  UpdateAddressModel({int zoneId, String address, int cityId, int sectorId}) {
    _zoneId = zoneId;
    _address = address;
    _cityId = cityId;
    _sectorId = sectorId;
  }

  UpdateAddressModel.fromJson(dynamic json) {
    _zoneId = json["zone_id"];
    _address = json["address"];
    _cityId = json["city_id"];
    _sectorId = json["sector_id"];
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map["zone_id"] = _zoneId;
    map["address"] = _address;
    map["city_id"] = _cityId;
    map["sector_id"] = _sectorId;
    return map;
  }
}
