/// first_name : "ade"
/// last_name : "ade"

class UpdateNameModel {
  String _firstName;
  String _lastName;

  String get firstName => _firstName;
  String get lastName => _lastName;

  UpdateNameModel({
      String firstName, 
      String lastName}){
    _firstName = firstName;
    _lastName = lastName;
}

  UpdateNameModel.fromJson(dynamic json) {
    _firstName = json["first_name"];
    _lastName = json["last_name"];
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map["first_name"] = _firstName;
    map["last_name"] = _lastName;
    return map;
  }

}