/// phone_number : ""
/// password : ""
/// request_type : ""

class Login {
  String _phoneNumber;
  String _password;
  String _requestType;

  String get phoneNumber => _phoneNumber;
  String get password => _password;
  String get requestType => _requestType;

  Login({
      String phoneNumber, 
      String password, 
      String requestType}){
    _phoneNumber = phoneNumber;
    _password = password;
    _requestType = requestType;
}

  Login.fromJson(dynamic json) {
    _phoneNumber = json["phone_number"];
    _password = json["password"];
    _requestType = json["request_type"];
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map["phone_number"] = _phoneNumber;
    map["password"] = _password;
    map["request_type"] = _requestType;
    return map;
  }

}