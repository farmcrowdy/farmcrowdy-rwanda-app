import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider_architecture/provider_architecture.dart';
import 'package:rwandashop/core/model/authentication/update_name_model.dart';
import 'package:rwandashop/core/services/analytics_service.dart';
import 'package:rwandashop/core/services/auth_service.dart';
import 'package:rwandashop/ui/screens/account/account_view_model.dart';
import 'package:rwandashop/ui/widget/generalButton.dart';
import 'package:rwandashop/utils/app_localizations.dart';
import 'package:rwandashop/utils/helpers.dart';
import 'package:rwandashop/utils/locator.dart';

class ChangeNamePage extends StatefulWidget {
  @override
  _Pickup createState() => _Pickup();
}

class _Pickup extends State<ChangeNamePage> {
  TextEditingController firstController = TextEditingController();
  TextEditingController lastNameController = TextEditingController();
  final Authentication _authentication = locator<Authentication>();
  final Analytics _analytics = locator<Analytics>();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    firstController.text = _authentication.currentUser.name;
    lastNameController.text = _authentication.currentUser.name;
  }

  @override
  Widget build(BuildContext context) {
    AppLocalizations tr = AppLocalizations.of(context);
    return ViewModelProvider<AccountViewModel>.withConsumer(
        onModelReady: (model) {
          _analytics.setCurrentScreen('Change name', 'Change name details');
        },
        viewModelBuilder: () => AccountViewModel(),
        builder: (context, model, child) {
          return Scaffold(
            body: SingleChildScrollView(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Container(
                    padding: EdgeInsets.only(top: 60),
                    child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Row(children: <Widget>[
                            GestureDetector(
                              onTap: () {
                                Navigator.pop(context);
                              },
                              child: Container(
                                height: 60,
                                width: 60,
                                //margin: EdgeInsets.only(top: 20),
                                // padding: EdgeInsets.only(top: 20,),
                                child: Icon(Icons.arrow_back_ios),
                              ),
                            ),
                            Text(
                              tr.text('changeContact.fullName'),
                              style: TextStyle(
                                  color: Colors.black,
                                  fontWeight: FontWeight.bold,
                                  fontSize: 20),
                            ),
                          ]),
                          Divider(),
                          Column(
                            children: <Widget>[
                              Container(
                                padding: EdgeInsets.all(20),
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: <Widget>[
                                    Text(
                                      tr.text('changeContact.firstName'),
                                      style: TextStyle(
                                          color: Colors.black45,
                                          fontWeight: FontWeight.bold,
                                          fontSize: 12),
                                    ),
                                    TextFormField(
                                      //controller: emailController,
                                      decoration: InputDecoration(
//                    errorText: residentialValid
//                        ? null
//                        : 'Kindly update this field',
                                        hintText: 'John',

                                        hintStyle: TextStyle(
                                          color: Colors.black45,
                                        ),
                                        labelStyle:
                                            TextStyle(color: Colors.blue),
                                        // border: new OutlineInputBorder(
                                        //   borderRadius: new BorderRadius.circular(5.0),
                                        //   borderSide: new BorderSide(),
                                        // ),
                                      ),
                                      keyboardType:
                                          TextInputType.visiblePassword,
                                      style: TextStyle(color: Colors.black),
                                      cursorColor: Colors.black,
                                      controller: firstController,
                                    ),
                                  ],
                                ),
                              ),
                              Container(
                                padding: EdgeInsets.all(20),
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: <Widget>[
                                    Text(
                                      tr.text('changeContact.lastName'),
                                      style: TextStyle(
                                          color: Colors.black45,
                                          fontWeight: FontWeight.bold,
                                          fontSize: 12),
                                    ),
                                    TextFormField(
                                      //controller: emailController,
                                      decoration: InputDecoration(
//                    errorText: residentialValid
//                        ? null
//                        : 'Kindly update this field',
                                        hintText: 'Doe',
                                        hintStyle: TextStyle(
                                          color: Colors.black45,
                                        ),
                                        labelStyle:
                                            TextStyle(color: Colors.blue),
                                        // border: new OutlineInputBorder(
                                        //   borderRadius: new BorderRadius.circular(5.0),
                                        //   borderSide: new BorderSide(),
                                        //),
                                      ),
                                      keyboardType:
                                          TextInputType.visiblePassword,
                                      style: TextStyle(color: Colors.black),
                                      cursorColor: Colors.black,
                                      controller: lastNameController,
                                    ),
                                  ],
                                ),
                              ),
                              Container(
                                  margin: EdgeInsets.all(20),
                                  child: GeneralButton(
                                    onPressed: () {
                                      if (firstController.text.isNotEmpty &&
                                          lastNameController.text.isNotEmpty) {
                                        model.updateFullName(UpdateNameModel(
                                          firstName: firstController.text,
                                          lastName: lastNameController.text,
                                        ));
                                      } else {
                                        showToast(tr.text(
                                            'changeAddress.fieldCompulsory'));
                                      }
                                    },
                                    buttonText:
                                        tr.text('changeAddress.saveChanges'),
                                  )
                              )
                            ],
                          )
                        ]),
                  ),
                ],
              ),
            ),
          );
        });
  }
}
