import 'package:rwandashop/core/model/authentication/login.dart';
import 'package:rwandashop/core/model/error_model.dart';
import 'package:rwandashop/core/model/success_model.dart';
import 'package:rwandashop/core/services/auth_service.dart';
import 'package:rwandashop/ui/screens/bottomNavigation/bottomNav_modelView.dart';
import 'package:rwandashop/utils/baseModel.dart';
import 'package:rwandashop/utils/helpers.dart';
import 'package:rwandashop/utils/locator.dart';
import 'package:rwandashop/utils/router/navigationService.dart';
import 'package:rwandashop/utils/router/routeNames.dart';

class LoginViewModel extends BaseModel {
  final Authentication _authentication = locator<Authentication>();
  final AppStateProvider appStateProvider = locator<AppStateProvider>();
  final NavigationService _navigationService = locator<NavigationService>();
  signIn(Login login) async {
    setBusy(true);
    var result = await _authentication.login(login.toJson());

    if (result is ErrorModel) {
      setBusy(false);
      showErrorToast(result.error);
      print(result.error);
      notifyListeners();
      return ErrorModel(result.error);
    }
    if (result is SuccessModel) {
      setBusy(false);
      //setBusy(false);
      _navigationService.navigateClearRouteTo(BottomNavigationRoute);

      notifyListeners();
      // return SuccessModel(result.data);
    }
  }

  secondSignIn(Login login, int pageIndex) async {
    setBusy(true);
    var result = await _authentication.login(login.toJson());

    if (result is ErrorModel) {
      setBusy(false);
      showErrorToast(result.error);
      print(result.error);
      notifyListeners();
      return ErrorModel(result.error);
    }
    if (result is SuccessModel) {
      setBusy(false);
      //setBusy(false);
      if (pageIndex == 100) {
        _navigationService.pop();
        // _navigationService.navigateReplacementTo(ProductDetailsPageRoute);
      } else {
        appStateProvider.setCurrentTabTo(newTabIndex: pageIndex);
        _navigationService.navigateClearRouteTo(BottomNavigationRoute);
      }
      notifyListeners();
      return 'Success';
      // return SuccessModel(result.data);
    }
  }
}
