import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider_architecture/provider_architecture.dart';
import 'package:rwandashop/core/services/analytics_service.dart';
import 'package:rwandashop/core/services/auth_service.dart';
import 'package:rwandashop/ui/screens/account/account_view_model.dart';
import 'package:rwandashop/utils/app_localizations.dart';
import 'package:rwandashop/utils/colors.dart';
import 'package:rwandashop/utils/locator.dart';
import 'package:rwandashop/utils/router/navigationService.dart';
import 'package:rwandashop/utils/router/routeNames.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class YourInfo extends StatefulWidget {
  @override
  AccountState createState() => new AccountState();
}

class AccountState extends State<YourInfo> {
  final Authentication _authentication = locator<Authentication>();
  final NavigationService _navigationService = locator<NavigationService>();
  final Analytics _analytics = locator<Analytics>();
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    AppLocalizations tr = AppLocalizations.of(context);
    return ViewModelProvider<AccountViewModel>.withConsumer(
        onModelReady: (model) {
          _analytics.setCurrentScreen('Edit Info Page', 'Edit Info Page');
        },
        viewModelBuilder: () => AccountViewModel(),
        builder: (context, model, child) {
          return Scaffold(
            body: SingleChildScrollView(
              child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    SizedBox(
                      height: 50.h,
                    ),
                    Container(
                        padding: EdgeInsets.only(left: 20),
                        child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              InkWell(
                                onTap: () {
                                  Navigator.pop(context);
                                },
                                child: Icon(Icons.arrow_back_ios),
                              ),
                              Text(
                                tr.text('yourInfo.yourInfo'),
                                style: TextStyle(
                                    fontSize: 14.ssp,
                                    fontWeight: FontWeight.bold),
                              ),
                              Text(
                                '',
                                style: TextStyle(
                                    fontSize: 14.ssp,
                                    fontWeight: FontWeight.bold),
                              ),
                            ])),
                    SizedBox(
                      height: 20.h,
                    ),
                    Divider(
                      thickness: 1,
                    ),
                    InkWell(
                      onTap: () {
                        _navigationService.navigateTo(ChangeNameRoute);
                      },
                      child: Container(
                          padding: EdgeInsets.only(left: 15.w, right: 15.w),
                          child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    Text(
                                      tr.text('yourInfo.fullName'),
                                      style: TextStyle(
                                          fontSize: 14.ssp,
                                          fontWeight: FontWeight.bold),
                                    ),
                                    Icon(
                                      Icons.arrow_forward_ios,
                                      color: Colors.black,
                                      size: 18.w,
                                    )
                                  ],
                                ),
                                SizedBox(
                                  height: 10.h,
                                ),
                                Text(
                                  _authentication.currentUser.name != null
                                      ? _authentication.currentUser.name
                                      : tr.text('yourInfo.notAddedYet'),
                                  style: TextStyle(
                                      fontSize: 12.ssp,
                                      color: AppColors.lowGrey),
                                ),
                                SizedBox(
                                  height: 5.h,
                                ),
                              ])),
                    ),
                    Divider(
                      thickness: 1,
                    ),
                    SizedBox(
                      height: 5.h,
                    ),
                    InkWell(
                      onTap: () {
                        _navigationService.navigateTo(ChangeContactRoute);
                      },
                      child: Container(
                          padding: EdgeInsets.only(left: 15.w, right: 15.w),
                          child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    Text(
                                      tr.text('yourInfo.contactDetails'),
                                      style: TextStyle(
                                          fontSize: 14.ssp,
                                          fontWeight: FontWeight.bold),
                                    ),
                                    Icon(
                                      Icons.arrow_forward_ios,
                                      color: Colors.black,
                                      size: 18.w,
                                    )
                                  ],
                                ),
                                SizedBox(
                                  height: 10.h,
                                ),
                                FittedBox(
                                    child: Row(
                                  children: [
                                    Text(
                                      _authentication.currentUser.phoneNumber !=
                                              null
                                          ? tr.text('yourInfo.phoneNo') +
                                              _authentication
                                                  .currentUser.phoneNumber
                                          : tr.text(
                                              'yourInfo.phoneNotAddedYet'),
                                      style: TextStyle(
                                          fontSize: 12.ssp,
                                          color: AppColors.lowGrey),
                                    ),
                                    Text(
                                      ' . ',
                                      style: TextStyle(
                                          fontSize: 15,
                                          fontWeight: FontWeight.bold),
                                    ),
                                    Text(
                                      _authentication.currentUser.email != null
                                          ? tr.text('yourInfo.email') +
                                              _authentication.currentUser.email
                                          : tr.text(
                                              'yourInfo.emailNotAddedYet'),
                                      style: TextStyle(
                                          fontSize: 12.ssp,
                                          color: AppColors.lowGrey),
                                    ),
                                  ],
                                )),
                                SizedBox(
                                  height: 5.h,
                                ),
                              ])),
                    ),
                    Divider(
                      thickness: 1,
                    ),
                    // SizedBox(
                    //   height: 5.h,
                    // ),
                    // InkWell(
                    //   onTap: () {
                    //     _navigationService.navigateTo(ChangeAddressRoute);
                    //   },
                    //   child: Container(
                    //       padding: EdgeInsets.only(left: 15.w, right: 15.w),
                    //       child: Column(
                    //           crossAxisAlignment: CrossAxisAlignment.start,
                    //           children: [
                    //             Row(
                    //               mainAxisAlignment:
                    //                   MainAxisAlignment.spaceBetween,
                    //               children: [
                    //                 Text(
                    //                   tr.text(
                    //                       'yourInfo.address'),
                    //                   style: TextStyle(
                    //                       fontSize: 14.ssp,
                    //                       fontWeight: FontWeight.bold),
                    //                 ),
                    //                 Icon(
                    //                   Icons.arrow_forward_ios,
                    //                   color: Colors.black,
                    //                   size: 18.w,
                    //                 )
                    //               ],
                    //             ),
                    //             SizedBox(
                    //               height: 10.h,
                    //             ),
                    //             FittedBox(
                    //                 child: Row(
                    //               children: [
                    //                 Text(
                    //                   _authentication.currentUser.zoneName !=
                    //                           null
                    //                       ?      tr.text(
                    //                       'yourInfo.zone') +
                    //                           _authentication
                    //                               .currentUser.zoneName
                    //                       :
                    //                   tr.text(
                    //                       'yourInfo.zoneNotAddedYet'),
                    //                   style: TextStyle(
                    //                       fontSize: 12.ssp,
                    //                       color: AppColors.lowGrey),
                    //                 ),
                    //                 Text(
                    //                   ' . ',
                    //                   style: TextStyle(
                    //                       fontSize: 15,
                    //                       fontWeight: FontWeight.bold),
                    //                 ),
                    //                 Text(
                    //                   _authentication.currentUser.address !=
                    //                           null
                    //                       ? tr.text(
                    //                       'yourInfo.home')+
                    //                           _authentication
                    //                               .currentUser.address
                    //                       : tr.text(
                    //                       'yourInfo.homeNotAddedYet'),
                    //                   style: TextStyle(
                    //                       fontSize: 12.ssp,
                    //                       color: AppColors.lowGrey),
                    //                 ),
                    //               ],
                    //             ))
                    //           ])),
                    // ),
                    // Divider(
                    //   thickness: 1,
                    // ),
                    SizedBox(
                      height: 40.h,
                    ),
                  ]),
            ),
          );
        });
  }
}
