import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider_architecture/provider_architecture.dart';
import 'package:rwandashop/core/model/authentication/update_address_model.dart';
import 'package:rwandashop/core/services/analytics_service.dart';
import 'package:rwandashop/core/services/auth_service.dart';
import 'package:rwandashop/ui/screens/account/account_view_model.dart';
import 'package:rwandashop/ui/widget/generalButton.dart';
import 'package:rwandashop/utils/app_localizations.dart';
import 'package:rwandashop/utils/helpers.dart';
import 'package:rwandashop/utils/locator.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class ChangeAddress extends StatefulWidget {
  @override
  _Pickup createState() => _Pickup();
}

class _Pickup extends State<ChangeAddress> {
  TextEditingController addressController = TextEditingController();
  final Authentication _authentication = locator<Authentication>();
  final Analytics _analytics = locator<Analytics>();
  List zone = List();
  int zoneId;
  List city = List();
  int cityId;
  String zoneName;
  String cityName;
  List district = List();
  int districtId;

  getZone(AccountViewModel model) async {
    print('ddd');
    var zones = await model.getZone();
    setState(() {
      zone = zones;
      print('ddd:::::::::::::::::::::');
      print(zone);
    });
  }

  getCity(AccountViewModel model, String id) async {
    print('ddd');
    var cities = await model.getCity(id);
    setState(() {
      city = cities;
      print('ddd:::::::::::::::::::::');
      print(city);
    });
  }

  getDistrict(AccountViewModel model, String id) async {
    print('ddd');
    var districts = await model.getDistrict("7");
    setState(() {
      district = districts;
      print('ddd:::::::::::::::::::::');
      print(district);
    });
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    // zoneId = _authentication.currentUser.zoneId;
    // cityId = _authentication.currentUser.cityId;
    // zoneName = _authentication.currentUser.zoneName;
    // cityName = _authentication.currentUser.cityName;
    addressController.text = _authentication.currentUser.address;
  }

  @override
  Widget build(BuildContext context) {
    AppLocalizations tr = AppLocalizations.of(context);
    return ViewModelProvider<AccountViewModel>.withConsumer(
        onModelReady: (model) {
          _analytics.setCurrentScreen(
              'Change address', 'Page to change address');
        },
        viewModelBuilder: () => AccountViewModel(),
        builder: (context, model, child) {
          if(zone.isEmpty){
            getZone(model);
          }
          if (district.isEmpty) {
            getDistrict(model, "7");
            //getCity(model, zoneId.toString());
          }
          return Scaffold(
            body: SingleChildScrollView(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Container(
                    padding: EdgeInsets.only(top: 60),
                    child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: <Widget>[
                                GestureDetector(
                                  onTap: () {
                                    Navigator.pop(context);
                                  },
                                  child: Container(
                                    height: 60,
                                    width: 60,
                                    //margin: EdgeInsets.only(top: 20),
                                    // padding: EdgeInsets.only(top: 20,),
                                    child: Icon(Icons.arrow_back_ios),
                                  ),
                                ),
                                Text(
                                  tr.text('changeAddress.ContactDetails'),
                                  style: TextStyle(
                                      color: Colors.black,
                                      fontWeight: FontWeight.bold,
                                      fontSize: 20),
                                ),
                                Text('')
                              ]),
                          Divider(),
                          Column(
                            children: <Widget>[
                              Container(
                                padding: EdgeInsets.all(20),
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: <Widget>[
                                    Text(
                                      tr.text('changeAddress.address'),
                                      style: TextStyle(
                                          color: Colors.black45,
                                          fontWeight: FontWeight.bold,
                                          fontSize: 12),
                                    ),
                                    TextFormField(
                                      //controller: emailController,
                                      decoration: InputDecoration(
//                    errorText: residentialValid
//                        ? null
//                        : 'Kindly update this field',
                                        hintText: 'No 2 kigali str',

                                        hintStyle: TextStyle(
                                          color: Colors.black45,
                                        ),
                                        labelStyle:
                                            TextStyle(color: Colors.blue),
                                        // border: new OutlineInputBorder(
                                        //   borderRadius: new BorderRadius.circular(5.0),
                                        //   borderSide: new BorderSide(),
                                        // ),
                                      ),
                                      keyboardType:
                                          TextInputType.visiblePassword,
                                      style: TextStyle(color: Colors.black),
                                      cursorColor: Colors.black,
                                      controller: addressController,
                                    ),
                                  ],
                                ),
                              ),
                              Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    Container(
                                      padding:
                                          EdgeInsets.only(left: 20, right: 20),
                                      child: Text(
                                        tr.text('changeAddress.yourZone'),
                                        style: TextStyle(
                                            color: Colors.black45,
                                            fontWeight: FontWeight.bold,
                                            fontSize: 12),
                                      ),
                                    ),
                                    Text('')
                                  ]),
                              SizedBox(height: 10.h),
                              Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    Container(
                                        padding: EdgeInsets.only(
                                            left: 20, right: 20),
                                        child: Text(
                                          'The Northern Province',
                                          style: TextStyle(
                                              fontWeight: FontWeight.bold),
                                        ))
                                  ]),
                              Container(
                                  padding:
                                      EdgeInsets.only(left: 20.w, right: 20.w),
                                  child: Center(
                                    child: new DropdownButtonFormField(
                                      decoration: InputDecoration.collapsed(
                                          hintText: ''),
                                      isExpanded: true,
                                      validator: (value) =>
                                          value == null ? '' : null,
                                      items: zone.map((item) {
                                        return new DropdownMenuItem(
                                          child: new Text(item['name']),
                                          value: item,
                                        );
                                      }).toList(),
                                      onChanged: (newVal) {
                                        setState(() {
                                          zoneId = newVal['id'];
                                          print(zoneId);
                                          city.clear();
                                          getCity(model, zoneId.toString());
                                        });
                                      },
                                      //value: unitPrice,
                                    ),
                                  )),
                              SizedBox(height: 10.h),
                              Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    Container(
                                      padding:
                                          EdgeInsets.only(left: 20, right: 20),
                                      child: Text(
                                        tr.text('changeAddress.yourCity'),
                                        style: TextStyle(
                                            color: Colors.black45,
                                            fontWeight: FontWeight.bold,
                                            fontSize: 12),
                                      ),
                                    ),
                                    Text('')
                                  ]),
                              SizedBox(height: 20.h),
                              Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    Container(
                                        padding: EdgeInsets.only(
                                            left: 20, right: 20),
                                        child: Text(
                                          'Musanze',
                                          style: TextStyle(
                                              fontWeight: FontWeight.bold),
                                        )),
                                    Text('')
                                  ]),
                              //
                              // Container(
                              //     padding:
                              //         EdgeInsets.only(left: 20.w, right: 20.w),
                              //     child: Center(
                              //       child: new DropdownButtonFormField(
                              //         decoration: InputDecoration.collapsed(
                              //             hintText: ''),
                              //         isExpanded: true,
                              //
                              //         validator: (value) =>
                              //             value == null ? '' : null,
                              //         items: city.map((item) {
                              //           return new DropdownMenuItem(
                              //             child: new Text(item['name']),
                              //             value: item,
                              //           );
                              //         }).toList(),
                              //         onChanged: (newVal) {
                              //           setState(() {
                              //             cityId = newVal['id'];
                              //             print(cityId);
                              //             district.clear();
                              //             getDistrict(model, cityId.toString());
                              //           });
                              //         },
                              //         //value: unitPrice,
                              //       ),
                              //     )),
                              SizedBox(height: 20.h),
                              Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    Container(
                                      padding:
                                          EdgeInsets.only(left: 20, right: 20),
                                      child: Text(
                                        tr.text('enterPassword.district'),
                                        style: TextStyle(
                                            color: Colors.black45,
                                            fontWeight: FontWeight.bold,
                                            fontSize: 12),
                                      ),
                                    ),
                                    Text('')
                                  ]),
                              SizedBox(height: 20.h),
                              Container(
                                  padding:
                                      EdgeInsets.only(left: 20.w, right: 20.w),
                                  child: Center(
                                    child: new DropdownButtonFormField(
                                      decoration: InputDecoration.collapsed(
                                          hintText: ''),
                                      isExpanded: true,

                                      validator: (value) =>
                                          value == null ? '' : null,
                                      items: district.map((item) {
                                        return new DropdownMenuItem(
                                          child: new Text(item['name']),
                                          value: item,
                                        );
                                      }).toList(),
                                      onChanged: (newVal) {
                                        setState(() {
                                          districtId = newVal['id'];
                                          print(districtId);
                                        });
                                      },
                                      //value: unitPrice,
                                    ),
                                  )),
                              SizedBox(
                                height: 20,
                              ),
                              Container(
                                  margin: EdgeInsets.all(20),
                                  child: GeneralButton(
                                    onPressed: () {
                                      if (addressController.text.isNotEmpty &&
                                          zoneId != null &&
                                          cityId != null &&
                                          districtId != null) {
                                        model.updateAddress(UpdateAddressModel(
                                            address: addressController.text,
                                            //zoneId: zoneId,
                                            //cityId: cityId,
                                            zoneId: 2,
                                            cityId: 7,
                                            sectorId: districtId));
                                      } else {
                                        showToast(tr.text(
                                            'changeAddress.fieldCompulsory'));
                                      }
                                    },
                                    buttonText:
                                        tr.text('changeAddress.saveChanges'),
                                  ))
                            ],
                          )
                        ]),
                  ),
                ],
              ),
            ),
          );
        });
  }
}
